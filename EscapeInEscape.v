Require Export Round.

(* One decision (among many others): To what degree do we omit components of the embedding
   that aren't needed by the input puzzle? *)
Definition embedEscape (innerState: State): State. Abort.
