(* https://github.com/ProofGeneral/PG/issues/355#issuecomment-509572853 *)
(* Put the cursor after the inner closing parenthesis and press C-x C-e. *)
(* (load-library "cl-extra") *)
(* Remember to enable undo-tree-mode also.  (Why is it not enabled by default?) *)

(* XXX: Review these *)
Require Export List.
Require Export Arith.PeanoNat.
Export List.ListNotations.
Open Scope bool_scope.
Require Export Lia.
Require Export Classical.
Global Set Printing Projections.

(* Strict options for proofs. *)
Global Set Default Goal Selector "!".

(* Mangle Names is enabled in the build but is disabled in interactive sessions
   because it makes goals and the output of Check, Print, Search, etc. harder to
   read.  This means that when you reference an automatically generated name,
   you'll get an error in the build but not in the interactive session.  If the
   problem isn't obvious from the error message, you can temporarily write "Set
   Mangle Names" in the file to reproduce the problem in the interactive session.

   There's a lower risk that something works in the build and not in the
   interactive session, e.g., a tactic defines a hypothesis that collides with an
   unmangled generated name.  I think we can fix any such cases as we find them
   in interactive sessions and it isn't worth trying to build both
   configurations.

   I'm disappointed that Coq does not have a better solution to the problem,
   perhaps based on https://github.com/coq/coq/pull/268 , but I still think we're
   better off using the Mangle Names feature than not.
   *)

(* Use to document that a case should not be reached when a function is used as
   intended and the return value is not meaningful. *)
Definition ShouldNotReach {A : Type} (x: A) := x.

(* Use to document that a case is unimplemented and the return value is not
   correct but may still be used to draft proofs. *)
Definition Unimplemented {A : Type} (x: A) := x.
