open Util;;

(*
ESX format:

- Magic string "ESXL"
- Width and height: 32-bit signed integers
- Title: 32-bit unsigned length followed by byte string
  (original code has the length variable signed, but it's converted to unsigned on every use)
- Author: ditto
- Player initial x and y: 32-bit signed integers
- Tiles: RLE dump of tile types
- Alternate tiles: RLE dump of tile types
- Destinations: RLE dump of cell numbers (y * width + x)
- Cell flags: RLE dump of cell flags
- Number of bots: 32-bit signed integer
- Bot locations: RLE dump of cell numbers
- Bot types: RLE dump of bot types [0, NUM_ROBOTS)

Note:
- The original implementation limits level width and height to 100, area to 2048, and
  number of bots to 15.  I modified my copy to increase all these limits to 2^31 - 1, the
  highest I believe the original implementation has a chance of safely handling.  (Looking
  just at the ESX format, the area could be 2^31 for cell numbers.)  TBD if I want to lower
  the limits a bit to avoid problems.
- The original implementation limits bot timers to 10.  This could be changed, but I have
  no interest in doing so.
- The original implementation breaks unpredictably if the level is 2^32 bytes or more
  in length.  We should probably check for this.
- Various violations of the format will not trigger the "corrupted" flag in the original
  implementation: trailing garbage, missing bot information (as long as <3 bytes), invalid
  number of bots.  We will be stricter.
- Once the ESX meets all the conditions here, the state must still be valid and initial.
  I could modify the original implementation to remove the "initial" requirement if that's
  useful.

RLE dump has the following format.  The caller must always know how many values it expects.

- First byte:
  - 0 through 4: value /byte/ count; frame bit count is 8
  - 128 | 0 through 63: value bit count; frame bit count is 8
    - If value bit count > 32, it looks like high bits of values get discarded, which is useless.
      Maybe we'll just call that invalid.
  - 128 | 64 | 0 through 63: value bit count; frame bit count follows
- Bit buffer (big endian, padded out with arbitrary bits to a full byte)
  - If applicable: 5-bit number giving frame bit count (cannot be 0, which would be useless)
  - Sequence of items as follows (delimited when we get the right number of values):
    - Frame number
    - If frame number > 0 ("run"): a value to be replicated that many times
    - If frame number = 0 ("antirun"): another frame number (must not be 0)
      followed by that many values

Note: An RLE dump containing an item that overflows the expected number of values is invalid.
*)

(* Structure that closely mirrors an esx file, as an intermediary between the
   actual binary representation and the State representation from Coq. *)
type lowlevel = {
  width: int;
  height: int;
  title: string;
  author: string;
  playerX: int;
  playerY: int;

  tiles: int array;
  otiles: int array;
  dests: int array;
  flags: int array;
  nbots: int;
  boti: int array; (* bot location *)
  bott: int array; (* bot type *)
  (* bota will be all -1 when loading a traditional esx.  We may add it here in the
     future if we extend the esx format to handle non-initial states. *)
  (* botd is only in-memory in the original implementation and is of no interest to us. *)
};;

(* Decodes enough values to fill the array.  Returns the new byte position. *)
let rleDecode buf pos arr =
  (* Because of the 30-bit limit, the return value will always be nonnegative. *)
  let getBits bitPos n =
    if n > 30 then failwith "getBits of more than 30 bits is not supported"
    else
    let rec getRestOfBits p n x =
      if n = 0 then x
      else let bit = (int_of_char (Buffer.nth buf (p / 8)) lsr (7 - (p mod 8))) land 1 in
	getRestOfBits (p + 1) (n - 1) ((x lsl 1) + bit)
    in getRestOfBits bitPos n 0
  in
  let bitPos = 8 * pos in
  let metaByte = getBits bitPos 8 in
  let (valueBits, frameBits, bitPos) =
    if metaByte land 128 <> 0
    then
      if metaByte land 64 <> 0
      then
	let fb = getBits (bitPos + 8) 5 in
	if fb = 0 then failwith "Invalid framebits";
        (metaByte land 63, fb, bitPos + 13)
      else
        (metaByte land 63, 8, bitPos + 8)
    else (8 * metaByte, 8, bitPos + 8)
  in
  let rec finishFrames bitPos ai =
    if ai = Array.length arr then bitPos else
    let frame = getBits bitPos frameBits in
    let bitPos = bitPos + frameBits in
    if frame = 0
    then
      let frame = getBits bitPos frameBits in
      let bitPos = bitPos + frameBits in
      if frame = 0 then failwith "Antirun of length 0 is invalid";
      let rec finishAntirun bitPos j =
      	if j = frame then finishFrames bitPos (ai + frame)
	else
          let value = getBits bitPos valueBits in
          let bitPos = bitPos + valueBits in
	  arr.(ai + j) <- value;  (* throws exception if out of range *)
	  finishAntirun bitPos (j + 1)
      in finishAntirun bitPos 0
    else
      let value = getBits bitPos valueBits in
      let bitPos = bitPos + valueBits in
      for j = 0 to frame - 1 do
        arr.(ai + j) <- value  (* throws exception if out of range *)
      done;
      finishFrames bitPos (ai + frame)
  in
  let newBitPos = finishFrames bitPos 0 in
  (newBitPos + 7) / 8
;;

type rleItem = RLEFrame of int | RLEValue of int;;

let rleEncode buf arr =
  let rec makeItems ai =
    let (antirunValues, laterItems) = scanAntirun false ai in
    match antirunValues with
    | [] -> laterItems
    (* A run of length 1 is a more compact encoding than an antirun of length 1. *)
    | [v] -> RLEFrame 1 :: RLEValue v :: laterItems
    | _ ->
      RLEFrame 0 :: RLEFrame (List.length antirunValues) ::
        (List.map (fun v -> RLEValue v) antirunValues @ laterItems)
  and scanAntirun started ai =
    if ai = Array.length arr
    then ([], [])
    (* Following the original implementation, once an antirun is started, break
       it only for a run of at least 3.  If an antirun is not yet started,
       trigger on a run of 2. *)
    else if ai + 2 <= Array.length arr && arr.(ai) = arr.(ai+1)
            && (not started || ai + 3 <= Array.length arr && arr.(ai) = arr.(ai+2))
    then ([], finishRun 2 arr.(ai) (ai+2))
    else let (antirunValues, laterItems) = scanAntirun true (ai+1) in
      (arr.(ai) :: antirunValues, laterItems)
  and finishRun n v ai =
    if ai < Array.length arr && arr.(ai) = v
    then finishRun (n+1) v (ai+1)
    else RLEFrame n :: RLEValue v :: makeItems ai
  in
  let items = makeItems 0 in
  let bitsNeeded x =
    let rec bitsNeeded1 x i = if x lsr i = 0 then i else bitsNeeded1 x (i+1)
    in bitsNeeded1 x 0
  in
  let frameBits = max 1 (bitsNeeded (List.fold_left max 0 (
    list_filter_map (fun i -> match i with RLEFrame f -> Some f | RLEValue _ -> None) items))) in
  let valueBits = bitsNeeded (List.fold_left max 0 (
    list_filter_map (fun i -> match i with RLEFrame _ -> None | RLEValue v -> Some v) items)) in
  let putBits startBitPos n x =
    if n > 30 then failwith "putBits of more than 30 bits is not supported"
    else
      for i = 0 to n - 1 do
        let bitPos = startBitPos + i in
        let bit = (x lsr (n - 1 - i)) land 1 in
        let bytePos = bitPos / 8 in
        let bitPosWithinByte = bitPos mod 8 in
        (* This is ridiculous: there is no random-access write in a buffer.  Fortunately,
           we can truncate and add back the last byte. *)
        let oldByte =
          if bytePos = Buffer.length buf
          then 0
          else let b = int_of_char (Buffer.nth buf bytePos) in Buffer.truncate buf bytePos; b
	in
	let newByte = oldByte lor (bit lsl (7 - bitPosWithinByte)) in
	Buffer.add_char buf (char_of_int newByte)
      done
  in
  let rec putItems bitPos items =
    match items with
    | [] -> ()
    | RLEFrame f :: t ->
      putBits bitPos frameBits f; putItems (bitPos + frameBits) t
    | RLEValue v :: t ->
      putBits bitPos valueBits v; putItems (bitPos + valueBits) t
  in
  let bitPos = 8 * Buffer.length buf in
  let bitPos =
    if frameBits = 8
    then
      (putBits bitPos 8 (valueBits lor 128);
      bitPos + 8)
    else
      (putBits bitPos 8 (valueBits lor 128 lor 64);
      putBits (bitPos + 8) 5 frameBits;
      bitPos + 13)
  in
  putItems bitPos items
  (* We don't need to return the new byte position: it is given by the end of the buffer. *)
;;

let expectedMagic = "ESXL";;

let bufferToLowlevel buf =
  (* [Because "bytes" offers get_int32_be and Buffer doesn't.] - Not now but maybe in the future *)
  let bts = Buffer.to_bytes buf in
  (* Assert file length < 2^31 to try to avoid overflows in the original implementation.
     On a 32-bit machine, we'll hit an exception trying to fill the buffer. *)
  ignore (int_to_int32_strict (Bytes.length bts));
  let pos = 0 in

  (* XXX: Throw a more descriptive exception when running off the end of the byte string. *)
  (* XXX: Ideally we should distinguish between "definitely invalid" and "unsupported by this parser,
     may or may not be valid".  This may not be a priority for our use scenario. *)
  let actualMagic = Bytes.sub_string bts pos 4 in
  let pos = pos + 4 in
  if actualMagic <> expectedMagic then failwith "Wrong magic";

  (* Note: We're not obliged to do any more validation than is necessary to make sure we can read in the
     level without losing data.  Full validation will be done by stateValid (and some in the high level
     converter, e.g., panel flags). *)
  (* Width or height >= 2^30 but < 2^31 is valid, but this implementation doesn't support it for now. :/ *)
  let width = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let height = int32_to_int_strict (bytes_get_int32_be bts (pos + 4)) in
  let pos = pos + 8 in
  if width < 0 then failwith "Width is negative";
  if height < 0 then failwith "Height is negative";
  (* We support width and height all the way up to max_int (subject to available memory),
     so no need to check an upper bound. *)
  if height > 0 && width > max_int / height then failwith "Area is too large";

  let titleLength = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  if titleLength < 0 then failwith "Title length is negative";
  let title = Bytes.sub_string bts pos titleLength in
  let pos = pos + titleLength in
  
  let authorLength = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  if authorLength < 0 then failwith "Author length is negative";
  let author = Bytes.sub_string bts pos authorLength in
  let pos = pos + authorLength in

  let playerX = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let playerY = int32_to_int_strict (bytes_get_int32_be bts (pos + 4)) in
  let pos = pos + 8 in

  (* The value to which the array is initialized doesn't matter; it gets overwritten. *)
  let tiles = Array.make (width * height) 0 in
  let pos = rleDecode buf pos tiles in
  let otiles = Array.make (width * height) 0 in
  let pos = rleDecode buf pos otiles in
  let dests = Array.make (width * height) 0 in
  let pos = rleDecode buf pos dests in
  let flags = Array.make (width * height) 0 in
  let pos = rleDecode buf pos flags in

  let (nbots, boti, bott, pos) =
    if pos = Bytes.length bts
    then (0, Array.make 0 0, Array.make 0 0, pos)
    else
      let nbots = int32_to_int_strict (bytes_get_int32_be bts pos) in
      if nbots < 0 then failwith "Number of bots is negative";
      let pos = pos + 4 in
      let boti = Array.make nbots 0 in
      let pos = rleDecode buf pos boti in
      let bott = Array.make nbots 0 in
      let pos = rleDecode buf pos bott in
      (nbots, boti, bott, pos)
  in
  if pos <> Bytes.length bts then failwith "Trailing garbage in esx file";

  ({
    width = width;
    height = height;
    title = title;
    author = author;
    playerX = playerX;
    playerY = playerY;
    tiles = tiles;
    otiles = otiles;
    dests = dests;
    flags = flags;
    nbots = nbots;
    boti = boti;
    bott = bott
  } : lowlevel)
;;

let lowlevelToBuffer ll =
  let buf = Buffer.create 0 in
  Buffer.add_string buf expectedMagic;
  buffer_add_int32_be buf (int_to_int32_strict ll.width);
  buffer_add_int32_be buf (int_to_int32_strict ll.height);
  buffer_add_int32_be buf (int_to_int32_strict (String.length ll.title));
  Buffer.add_string buf ll.title;
  buffer_add_int32_be buf (int_to_int32_strict (String.length ll.author));
  Buffer.add_string buf ll.author;
  buffer_add_int32_be buf (int_to_int32_strict ll.playerX);
  buffer_add_int32_be buf (int_to_int32_strict ll.playerY);
  rleEncode buf ll.tiles;
  rleEncode buf ll.otiles;
  rleEncode buf ll.dests;
  rleEncode buf ll.flags;
  buffer_add_int32_be buf (int_to_int32_strict ll.nbots);  
  rleEncode buf ll.boti;
  rleEncode buf ll.bott;
  (* Assert file length < 2^31 to try to avoid overflows in the original implementation.
     On a 32-bit machine, we'll hit an exception trying to fill the buffer. *)
  ignore (int_to_int32_strict (Buffer.length buf));
  buf
;;
