Require Export Basics.
Require Export BoolProp.
Require Export NatExt.

Lemma length_map:
  forall {A B : Type} (f : A -> B) (l : list A),
    length (map f l) = length l.
Proof.
  intros A B f l. induction l.
  - reflexivity.
  - simpl. lia.
Qed.

(* Factor out fd to make it easier to match the conclusion against goals. *)
Lemma nth_map:
  forall {A B : Type} (f : A -> B) (l : list A) (n : nat) (d : A) (fd : B),
    fd = f d -> nth n (map f l) fd = f (nth n l d).
Proof.
  intros A B f l n d fd Hfd. rewrite Hfd; clear fd Hfd. revert n.
  induction l as [ | h t IHl]; intro n.
  - destruct n; reflexivity.
  - destruct n. + reflexivity. + apply IHl.
Qed.

Lemma Forall_map:
  forall {A B : Type} (f : A -> B) (l : list A) (p : A -> Prop) (q : B -> Prop),
    Forall p l -> (forall (a : A), p a -> q (f a)) -> Forall q (map f l).
Proof.
  intros A B f l p q Hl Hf.
  induction l as [ | h t IHl].
  - apply Forall_nil.
  - inversion Hl. apply Forall_cons.
    + apply Hf. assumption.
    + apply IHl. assumption.
Qed.

Lemma Forall_app:
  forall {A : Type} (f : A -> Prop) (l1 l2 : list A),
    Forall f (l1 ++ l2) <-> Forall f l1 /\ Forall f l2.
Proof.
  intros A f l1 l2.
  (* Maybe we could do this with Forall_forall instead of an induction, but I
     had trouble making that work. *)
  split.
  - intro Hfa. induction l1 as [| a l1 IHl1].
    + split.
      * apply Forall_nil.
      * assumption.
    + inversion Hfa as [| ? ? Hf0 Hfa']. destruct (IHl1 Hfa').
      split.
      * apply Forall_cons.
        -- assumption.
        -- assumption.
      * assumption.
  - intros [Hf1 Hf2]. induction l1 as [| a l1 IHl1].
    + assumption.
    + inversion Hf1 as [| ? ? Hf0 Hf1']. apply Forall_cons.
      * assumption.
      * apply IHl1. assumption.
Qed.

Fixpoint update_nth {A : Type} (l : list A) (i : nat) (x : A) :=
  match l, i with
  | nil, _ => ShouldNotReach l
  | h :: t, 0 => x :: t
  | h :: t, S i2 => h :: update_nth t i2 x
  end.

Lemma length_update_nth:
  forall {A : Type} (l : list A) (i : nat) (x : A), length (update_nth l i x) = length l.
Proof.
  intros A l.
  induction l as [ | h t IHl].
  - reflexivity.
  - intros i x. destruct i. + reflexivity. + simpl. rewrite IHl. reflexivity.
Qed.

Lemma nth_update_nth:
  forall {A : Type} (l : list A) (i : nat) (x d : A),
    i < length l -> nth i (update_nth l i x) d = x.
Proof.
  intros A l. induction l as [ | h t IHl]; intros i x d Hi.
  - simpl in Hi. lia.
  - destruct i.
    + reflexivity.
    + apply IHl. simpl in Hi. lia.
Qed.

Lemma nth_error_update_nth:
  forall {A : Type} (l : list A) (i : nat) (x : A),
    i < length l -> nth_error (update_nth l i x) i = Some x.
Proof.
  intros A l. induction l as [ | h t IHl]; intros i x Hi.
  - simpl in Hi. lia.
  - destruct i.
    + reflexivity.
    + apply IHl. simpl in Hi. lia.
Qed.

Lemma mth_update_nth:
  forall {A : Type} (l : list A) (m n : nat) (x d : A),
    m <> n -> nth m (update_nth l n x) d = nth m l d.
Proof.
  intros A l. induction l as [ | h t IHl]; intros m n x d Hmn.
  - reflexivity.
  - destruct m; destruct n.
    + lia.
    + reflexivity.
    + reflexivity.
    + apply IHl. lia.
Qed.

Lemma mth_error_update_nth:
  forall {A : Type} (l : list A) (m n : nat) (x : A),
    m <> n -> nth_error (update_nth l n x) m = nth_error l m.
Proof.
  intros A l. induction l as [ | h t IHl]; intros m n x Hmn.
  - reflexivity.
  - destruct m; destruct n.
    + lia.
    + reflexivity.
    + reflexivity.
    + apply IHl. lia.
Qed.

(* Passing a default is a little weird, but if callers want to use it, fine. *)
Lemma Forall_nth:
  forall {A : Type} (l : list A) (f : A -> Prop) (n : nat) (d : A),
    Forall f l -> n < length l -> f (nth n l d).
Proof.
  intros A l f n d HFl Hn; revert n Hn.
  induction l as [ | h t IHl]; intros n Hn.
  - simpl in Hn. lia.
  - simpl in *. inversion HFl. destruct n.
    + assumption.
    + apply IHl. * assumption. * lia.
Qed.

Lemma Forall_nth_error:
  forall {A : Type} (l : list A) (f : A -> Prop) (n : nat) (x : A),
    Forall f l -> nth_error l n = Some x -> f x.
Proof.
  intros A l f n x HFl; revert n.
  induction l as [ | h t IHl]; intros n Heq; destruct n; simpl in Heq; try discriminate.
  - injection Heq as H0. inversion HFl. rewrite <- H0. assumption.
  - eapply IHl. + inversion HFl. assumption. + apply Heq.
Qed.

Lemma Forall_update_nth:
  forall {A : Type} (l : list A) (f : A -> Prop) (n : nat) (x : A),
    Forall f l -> f x -> Forall f (update_nth l n x).
Proof.
  intros A l f n x HFl Hfx. revert n.
  induction l as [ | h t IHl]; intro n.
  - apply Forall_nil.
  - simpl. inversion HFl. destruct n; apply Forall_cons.
    + assumption. + assumption.
    + assumption. + apply IHl. assumption.
Qed.

(* We may want to replace forallb and/or Forall with something else, but at
   least this gives us something to get started. *)
Definition forallbp {A: Type} (f: A -> BoolProp) (l: list A): BoolProp.
  refine {|
      bp_bool := forallb (fun a => (f a).(bp_bool)) l;
      bp_prop := Forall (fun a => (f a).(bp_prop)) l
    |}.
  split; intro H; induction l as [ | h t IHl].
  (* forallb to Forall *)
  - constructor.
  - simpl in H. apply Bool.andb_true_iff in H. destruct H.
    constructor. + apply (f h).(bp_equiv). assumption. + apply IHl. assumption.
  (* Forall to forallb *)
  - constructor.
  - simpl. inversion H as [ | ? ? H2]. apply (f h).(bp_equiv) in H2. rewrite H2. simpl.
  apply IHl. assumption.
Defined.

Definition existsbp {A : Type} (f: A -> BoolProp) (l: list A) : BoolProp.
  refine {|
      bp_bool := existsb (fun a => (f a).(bp_bool)) l;
      bp_prop := Exists (fun a => (f a).(bp_prop)) l
    |}.
  split; intro H; induction l as [| h t IHl].
  (* existsb to Exists *)
  - discriminate H.
  - simpl in H. destruct (f h).(bp_bool) eqn:Hfh.
    + apply Exists_cons_hd. apply (f h).(bp_equiv). assumption.
    + apply Exists_cons_tl. apply IHl. apply H.
  (* Exists to existsb *)
  - inversion H.
  - simpl. inversion H as [? ? H1 |].
    + apply (f h).(bp_equiv) in H1. rewrite H1. simpl. reflexivity.
    + rewrite IHl. * rewrite Bool.orb_true_r. reflexivity. * assumption.
Defined.

Fixpoint forall_range_b (lo hi : nat) (f : nat -> bool) :=
  match hi with
  | 0 => true
  | S hi' => if hi' >=? lo then andb (f hi') (forall_range_b lo hi' f) else true
  end.

Definition forall_range_bp (lo hi : nat) (f : nat -> BoolProp) : BoolProp.
  refine {|
      bp_bool := forall_range_b lo hi (fun i => (f i).(bp_bool));
      bp_prop := forall i, lo <= i < hi -> (f i).(bp_prop)
    |}.
  split; intro H.
  (* forall_range_b to forall *)
  - induction hi as [| hi IHhi].
    + intros i Hi. lia.
    + intros i Hi. simpl in H.
      destruct (lo <=? hi) eqn:Hlohi.
      * unfold Nat.geb in H. rewrite Hlohi in H. simpl in H. apply andb_prop in H.
        destruct (Nat.eq_dec i hi) as [e |].
        -- rewrite e in *; clear i e.
           apply (f hi).(bp_equiv). apply H.
        -- apply IHhi. ++ apply H. ++ lia.
      * apply Compare_dec.leb_complete_conv in Hlohi. lia.
  (* forall to forall_range_b *)
  - induction hi as [| hi IHhi].
    + reflexivity.
    + unfold forall_range_b. unfold Nat.geb.
      destruct (lo <=? hi) eqn:Hlohi.
      * apply Compare_dec.leb_complete in Hlohi. simpl. apply andb_true_intro. split.
        -- apply (f hi).(bp_equiv). apply H. lia.
        -- apply IHhi. intros i Hi. apply H. lia.
      * reflexivity.
Defined.
