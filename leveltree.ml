let rec scanLevels root =
  let names = Array.to_list (Sys.readdir root) in
  let checkOne n =
    let fullPath = Filename.concat root n in
    if Sys.is_directory fullPath then
      if Sys.file_exists (Filename.concat fullPath "index.esi") ||
         Sys.file_exists (Filename.concat fullPath "webindex.esi")
      then scanLevels fullPath
      else []
    else if String.length n > 4 && String.sub n (String.length n - 4) 4 = ".esx"
         then [fullPath]
         else []
  in List.concat (List.map checkOne names)
;;
