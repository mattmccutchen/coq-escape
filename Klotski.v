Require Export Basics.
Require Export BoolProp.
Require Export ListExt.
Require Export ZArith_base.

Record Pos :=
  {
    p_x: nat;
    p_y: nat
  }.

Record Offset :=
  {
    (* This is a little funny, but what else to do? *)
    o_x: Z;
    o_y: nat
  }.
Section OffsetEquality.
Local Unset Mangle Names.
Scheme Equality for Offset.
End OffsetEquality.

Definition shouldNotReachOffset := {| o_x := 0; o_y := 0 |}.

(* (y, x) >= (0, 0) lexicographically: given that y >= 0, equivalent to y > 0 or x >= 0 *)
Definition offsetValid (o: Offset) :=
  (* XXX: Build up the whole BoolProp stack for Z like we did for nat? *)
  (leafBoolProp (Z.geb o.(o_x) 0))
    ||| (o.(o_y) >?? 0).

Definition PieceType := list Offset.

Definition shouldNotReachPieceType := [shouldNotReachOffset].

Definition typeValid (t: PieceType) :=
  (* First offset is (0, 0) *)
  match t with
  | {| o_x := 0; o_y := 0 |} :: t' => leafBoolProp true
  | _ => leafBoolProp false
  end
    (* All offsets valid *)
    &&& forallbp offsetValid t
    (* No duplicates *)
    &&& forall_range_bp 0 (length t)
    (fun j => forall_range_bp
                0 j (fun i =>
                       negbp (leafBoolProp (Offset_beq
                                              (nth i t shouldNotReachOffset)
                                              (nth j t shouldNotReachOffset)
    )))).
(* I don't think we need the offsets to be in lexicographic order. *)

Record PieceInfo :=
  {
    p_typeNum: nat;
    p_anchor: Pos
  }.
Section PieceInfoEquality.
Local Unset Mangle Names.
Scheme Equality for PieceInfo.
End PieceInfoEquality.

Record State :=
  {
    s_width: nat;
    s_height: nat;
    s_types: list PieceType;
    s_pieces: list PieceInfo;
    (* Not a real piece, but a format for the goal piece type and goal zone anchor
       that makes it convenient to validate the goal zone. *)
    s_goalPiece: PieceInfo
  }.

Definition pieceValid (s: State) (p: PieceInfo) :=
  (* Alternative: check against length and then use nth.  This seems much cleaner. :/ *)
  match nth_error s.(s_types) p.(p_typeNum) with
  | None => leafBoolProp false
  | Some t => forallbp (fun o =>
                          (* This is a terrible mess; anything better to do with Z and nat? *)
                          let x := Z.add (Z.of_nat p.(p_anchor).(p_x)) o.(o_x) in
                          let y := p.(p_anchor).(p_y) + o.(o_y) in
                          (leafBoolProp (Z.geb x 0) &&& leafBoolProp (Z.ltb x (Z.of_nat s.(s_width))))
                            &&& (y <?? s.(s_height))
                       ) t
  end.

Definition pieceContains (s: State) (pos: Pos) (p: PieceInfo) :=
  match nth_error s.(s_types) p.(p_typeNum) with
  | None => ShouldNotReach (leafBoolProp false)
  | Some t => existsbp (fun o =>
                          (* TODO Factor this out? *)
                          let x := Z.add (Z.of_nat p.(p_anchor).(p_x)) o.(o_x) in
                          let y := p.(p_anchor).(p_y) + o.(o_y) in
                          leafBoolProp (Z.eqb x (Z.of_nat pos.(p_x))) &&& (y =?? pos.(p_y))
                       ) t
  end.

Definition stateValid (s: State) :=
  (forallbp typeValid s.(s_types))
    &&& (forallbp (pieceValid s) s.(s_pieces))
    &&& (pieceValid s s.(s_goalPiece))
    (* There is exactly one piece of the goal type.  (Want to require that it be the first piece?) *)
    &&& (length (filter (fun p => p.(p_typeNum) =? s.(s_goalPiece).(p_typeNum)) s.(s_pieces)) =?? 1)
    (* Every space on the board is covered by at most one piece.  (Another way to
       formulate this would be that no two pieces, identified by two distinct indices,
       cover the same space. *)
    &&& (forall_range_bp 0 s.(s_height) (fun y => forall_range_bp 0 s.(s_width) (fun x => length (filter (fun p => (pieceContains s {| p_x := x; p_y := y |} p).(bp_bool)) s.(s_pieces)) <=?? 1)))
.

(* Note: It's implicit that a Klotski board must have positive width and height
   because it must contain a goal piece and that piece type must include the
   (0, 0) offset.

   Normally Klotski piece types are connected, but we have no need to require this.
*)

(* Other options:
   1. Don't use PieceType, but inline the info in every PieceInfo.
   2. Like #1, but list all contained positions (and update all of them
      when the piece moves) instead of using offsets from an anchor.
      But we need to express the goal position somehow.  Maybe match it
      against the first position in the list.

   Also, when moving a piece, we need to identify it.  We could identify
   it by its index in the list of pieces, by its anchor, or by the first
   position in its list.
 *)

Variant Direction := Up | Down | Left | Right.

Record Move :=
  {
    m_pieceNum: nat;
    m_dir: Direction
  }.

Definition attemptMove (s: State) (m: Move): option State :=
  match nth_error s.(s_pieces) m.(m_pieceNum) with
  | None => None
  | Some piece =>
    let oldAnchor := piece.(p_anchor) in
    let newAnchorOpt := match m.(m_dir) with
                        | Up => if oldAnchor.(p_y) =? 0 then None
                                else Some {| p_x := oldAnchor.(p_x); p_y := pred oldAnchor.(p_y) |}
                        | Down => Some {| p_x := oldAnchor.(p_x); p_y := S oldAnchor.(p_y) |}
                        | Left => if oldAnchor.(p_x) =? 0 then None
                                else Some {| p_x := pred oldAnchor.(p_x); p_y := oldAnchor.(p_y) |}
                        | Right => Some {| p_x := S oldAnchor.(p_x); p_y := oldAnchor.(p_y) |}
                        end in
    match newAnchorOpt with
    | None => None
    | Some newAnchor =>
      let newPiece := {| p_anchor := newAnchor; p_typeNum := piece.(p_typeNum) |} in
      let newState := {|
            s_width := s.(s_width);
            s_height := s.(s_height);
            s_types := s.(s_types);
            s_pieces := update_nth s.(s_pieces) m.(m_pieceNum) newPiece;
            s_goalPiece := s.(s_goalPiece);
          |} in
      (* Sledgehammer.  Alternatively, we could check that the new piece doesn't
         occupy any squares that are still occupied when the old piece is removed. *)
      if (stateValid newState).(bp_bool) then Some newState else None
    end
  end.

(* Presupposition: State is valid. *)
Definition stateIsWon (s: State) :=
  existsbp (fun p => leafBoolProp (PieceInfo_beq p s.(s_goalPiece))) s.(s_pieces).

(* Presupposition: initState is valid. *)
Fixpoint isSolution (initState: State) (moves: list Move) :=
  match moves with
  | nil => stateIsWon initState
  | m :: ms =>
    match attemptMove initState m with
    | None => leafBoolProp false
    | Some s' => isSolution s' ms
    end
  end.
