Require Export Basics.
Require Export BoolProp.

Module Nat.
  (* Go figure why these aren't in the original Nat. *)
  Definition geb x y := Nat.leb y x.
  Definition gtb x y := Nat.ltb y x.
  Definition neb x y := negb (Nat.eqb x y).
  (* Notations for use in the Module block *)
  Notation "x >=? y" := (Nat.geb x y) (at level 70) : nat_scope.
  Notation "x >? y" := (Nat.gtb x y) (at level 70) : nat_scope.
  Notation "x <>? y" := (Nat.neb x y) (at level 70) : nat_scope.
  Lemma geb_ge : forall x y, x >=? y = true <-> x >= y.
  Proof.
    intros x y. unfold geb. unfold ge. apply Nat.leb_le.
  Qed.
  Lemma gtb_gt : forall x y, x >? y = true <-> x > y.
  Proof.
    intros x y. unfold gtb. unfold gt. apply Nat.ltb_lt.
  Qed.
  Lemma neb_ne : forall x y, x <>? y = true <-> x <> y.
  Proof.
    intros x y. unfold neb.
    (* Is there an easier way??? *)
    destruct (x =? y) eqn:Heqb; simpl.
    - apply Nat.eqb_eq in Heqb. split. + intro H. inversion H. + intro. contradiction.
    - apply Nat.eqb_neq in Heqb. split. + intros _. apply Heqb. + intros _. reflexivity.
  Qed.

  Definition eqbp x y :=
    {|
      bp_bool := x =? y;
      bp_prop := x = y;
      (* It's odd that Nat qualification is needed. 
         I must have something yet to learn about modules. *)
      bp_equiv := Nat.eqb_eq x y
    |}.
  Definition lebp x y :=
    {|
      bp_bool := x <=? y;
      bp_prop := x <= y;
      bp_equiv := Nat.leb_le x y
    |}.
  Definition ltbp x y :=
    {|
      bp_bool := x <? y;
      bp_prop := x < y;
      bp_equiv := Nat.ltb_lt x y
    |}.
  Definition nebp x y :=
    {|
      bp_bool := x <>? y;
      bp_prop := x <> y;
      bp_equiv := Nat.neb_ne x y
    |}.
  Definition gebp x y :=
    {|
      bp_bool := x >=? y;
      bp_prop := x >= y;
      bp_equiv := Nat.geb_ge x y
    |}.
  Definition gtbp x y :=
    {|
      bp_bool := x >? y;
      bp_prop := x > y;
      bp_equiv := Nat.gtb_gt x y
    |}.    
End Nat.
Notation "x >=? y" := (Nat.geb x y) (at level 70) : nat_scope.
Notation "x >? y" := (Nat.gtb x y) (at level 70) : nat_scope.
Notation "x <>? y" := (Nat.neb x y) (at level 70) : nat_scope.
Notation "x =?? y" := (Nat.eqbp x y) (at level 70) : nat_scope.
Notation "x <=?? y" := (Nat.lebp x y) (at level 70) : nat_scope.
Notation "x <?? y" := (Nat.ltbp x y) (at level 70) : nat_scope.
Notation "x <>?? y" := (Nat.nebp x y) (at level 70) : nat_scope.
Notation "x >=?? y" := (Nat.gebp x y) (at level 70) : nat_scope.
Notation "x >?? y" := (Nat.gtbp x y) (at level 70) : nat_scope.
