Require Export Round.

Variant PlayStatus := Okay | Won | Lost.
Section PlayStatusEquality.
Local Unset Mangle Names.
Scheme Equality for PlayStatus.
End PlayStatusEquality.
Record PlayResult :=
  {
    pr_numMoves: nat;
    pr_state: State;
    pr_status: PlayStatus
  }.

Definition MoveSequence := list Direction.

(* Play as many of the given moves as possible.  Stop if the state is won or lost
   (except at the very beginning) or if the next move is illegal.  If the
   returned pr_status is Okay, check pr_numMoves to tell whether play got all the
   way through or stopped on an illegal move. *)
(* Precondition: stateValid s and not stateIsSeverelyLost s *)
Definition play (s: State) (ms: MoveSequence) : PlayResult :=
  let fix play1 s' ms' n :=
      let finishWithStatus st :=
          {| pr_numMoves := n; pr_state := s'; pr_status := st |} in
      let tryNextMove :=
          match ms' with
          | [] => finishWithStatus Okay
          | h :: ms'' =>
            match attemptRound s' h with
            | None => finishWithStatus Okay
            | Some s'' => play1 s'' ms'' (S n)
            end
          end in
      match n with
      | O => tryNextMove
      | S _ =>
        if (stateIsLost s').(bp_bool)
        then finishWithStatus Lost
        else if (stateIsWon s').(bp_bool)
             then finishWithStatus Won
             else tryNextMove
      end in
  play1 s ms 0.

Definition isSolution (s: State) (ms: MoveSequence) :=
  let pr := play s ms in
  PlayStatus_beq pr.(pr_status) Won && (pr.(pr_numMoves) =? (length ms)).
