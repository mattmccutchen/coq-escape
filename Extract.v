Require Export Play.
From Coq Require Extraction.
Require Export Ascii.
Require Export String.

(* Make conversions a little lighter. *)
Extract Inductive bool => "bool" [ "true" "false" ].
Extract Inductive option => "option" [ "Some" "None" ].
Extract Inductive list => "list" [ "Extraction.nil" "Extraction.cons" ] "Extraction.list_match".
Extract Inductive ascii => "char" [ "Extraction.charCons" ] "Extraction.char_match".
Extract Inductive string
=> "string" [ "Extraction.emptyString" "Extraction.stringCons" ] "Extraction.string_match".

(* TODO: How to ensure this is re-run at the appropriate times? *)
(* TODO: Want to use "Recursive Extraction Library" rather than list all the leaves we need?  It will
   mean exposing a lot more files to the OCaml build system, such as it is. *)
Extraction "escape.ml" TitledState play stateValid stateIsInitial.
