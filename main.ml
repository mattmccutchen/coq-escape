(*
open Util;;
open Highlevel;;
*)
(* TODO: Learn how to put uppercase letters in the middle of a file name. *)
open Verifyplayerfile;;

(*
(* Just a test of passing data through *)
match Array.to_list Sys.argv with
| [_; inputEsx; outputEsx] -> (
  let inChannel = open_in_bin inputEsx in
  let buf = Buffer.create 0 in
  fullRead inChannel buf;
  close_in inChannel;

  let ts = bufferToTitledState buf in
  print_endline ("The title is: " ^ ts.ts_title);
  match Escape.attemptRound ts.ts_state Left with
  | None -> print_endline "Oops, illegal move";
  | Some state2 ->
    let ts2 = { ts with ts_state = state2 } in
    let outBuf = titledStateToBuffer ts2 in
    let outChannel = open_out_bin outputEsx in
    Buffer.output_buffer outChannel outBuf;
    close_out outChannel;
    print_endline "Performed move and wrote output"
)
| _ -> failwith "Wrong command line arguments"
*)

(* Parse the player file and check all solutions and bookmarks. *)
match Array.to_list Sys.argv with
| [_; inputPlayerFile; inputLevelRoot] ->
  verifyPlayerFile inputPlayerFile inputLevelRoot
| _ -> failwith "Wrong command line arguments"
