Require Export State.

(* Any kind of state manipulation that occurs in Escape should produce a
   compatible state.  Various things rely on this for positions and entity
   indices to remain valid. *)
Definition statesCompatible (s1 s2 : State) :=
  s1.(s_width) = s2.(s_width) /\ s1.(s_height) = s2.(s_height) /\
  length s1.(s_bots) = length s2.(s_bots).

Lemma statesCompatible_reflexive:
  forall s : State, statesCompatible s s.
Proof.
  intro s. repeat split; reflexivity.
Qed.

Lemma statesCompatible_symmetric:
  forall s1 s2 : State, statesCompatible s1 s2 -> statesCompatible s2 s1.
Proof.
  intros s1 s2 H12.
  destruct H12 as [H12w [H12h H12e]].
  repeat split; lia.
Qed.

Lemma statesCompatible_transitive:
  forall (s1 s2 s3 : State), statesCompatible s1 s2 -> statesCompatible s2 s3 -> statesCompatible s1 s3.
Proof.
  intros s1 s2 s3 H12 H23.
  destruct H12 as [H12w [H12h H12e]]. destruct H23 as [H23w [H23h H23e]].
  repeat split.
  - rewrite H12w. rewrite H23w. reflexivity.
  - rewrite H12h. rewrite H23h. reflexivity.
  - rewrite H12e. rewrite H23e. reflexivity.
Qed.

Definition posInBounds (s: State) (p: Pos) :=
  (p.(p_x) <?? s.(s_width)) &&& (p.(p_y) <?? s.(s_height)).

Lemma posInBounds_compatible:
  forall (s s2 : State) (p : Pos),
    statesCompatible s s2 -> (posInBounds s p).(bp_prop) -> (posInBounds s2 p).(bp_prop).
Proof.
  intros s s2 p Hc Hpb.
  destruct Hpb as [Hxb Hyb]. destruct Hc as [Hwc [Hhc Hec]].
  split.
  - rewrite <- Hwc. assumption.
  - rewrite <- Hhc. assumption.
Qed.

Definition getCell (s: State) (p: Pos) :=
  (* Since we can, make up a default cell rather than force every caller to write a ShouldNotReach case. *)
  let defaultCell := {|
        c_state := {| l_tile := Floor; l_panel := None |};
        c_ostate := {| l_tile := Floor; l_panel := None |};
        c_dest := {| p_x := 0; p_y := 0 |} |} in
  nth p.(p_x) (nth p.(p_y) s.(s_cells) []) defaultCell.

(* Maybe consider https://github.com/tchajed/coq-record-update ? *)

Definition setCell (s: State) (p: Pos) (c: CellInfo) :=
  {|
    s_width := s.(s_width);
    s_height := s.(s_height);
    s_cells := match nth_error s.(s_cells) p.(p_y) with
             | None => ShouldNotReach s.(s_cells)
             | Some row => update_nth s.(s_cells) p.(p_y) (update_nth row p.(p_x) c)
               end;
    s_playerPos := s.(s_playerPos);
    s_playerBombed := s.(s_playerBombed);
    s_bots := s.(s_bots);
  |}.

(* Helper for common operation *)
Definition setTile (s: State) (p: Pos) (t: TileType) :=
  let c := getCell s p in
  setCell s p {|
            c_state := {|
                        l_tile := t;
                        l_panel := c.(c_state).(l_panel)
                      |};
            c_ostate := c.(c_ostate);
            c_dest := c.(c_dest)
          |}.

Definition shouldNotReachBot :=
  ShouldNotReach {| b_pos := {| p_x := 0; p_y := 0 |}; b_status := NoBot |}.

Definition getBot (s: State) (bi: nat) := nth bi s.(s_bots) shouldNotReachBot.

Definition setBot (s: State) (bi: nat) (b: BotInfo) :=
  {|
    s_width := s.(s_width);
    s_height := s.(s_height);
    s_cells := s.(s_cells);
    s_playerPos := s.(s_playerPos);
    s_playerBombed := s.(s_playerBombed);
    s_bots := update_nth s.(s_bots) bi b;
  |}.

Variant EntityID :=
| Player
| Bot (bi: nat).

(* This one is propositional, for now *)
Definition entityIDValid (s: State) (ei: EntityID) :=
  match ei with
  | Player => True
  | Bot bi => bi < length s.(s_bots)
  end.

Lemma entityIDValid_compatible:
  forall (s s2: State) (ei: EntityID),
    statesCompatible s s2 -> entityIDValid s ei -> entityIDValid s2 ei.
Proof.
  intros s s2 ei Hc Hei.
  unfold entityIDValid in *.
  destruct ei; [exact I |].
  destruct Hc as [hh [hw hb]].
  lia.
Qed.

Definition getEntityPos (s: State) (ei: EntityID) :=
  match ei with
  | Player => s.(s_playerPos)
  | Bot bi => (getBot s bi).(b_pos)
  end.

Definition moveEntity (s: State) (ei: EntityID) (p: Pos) :=
  match ei with
  | Player => {|
      s_width := s.(s_width);
      s_height := s.(s_height);
      s_cells := s.(s_cells);
      s_playerPos := p;
      s_playerBombed := s.(s_playerBombed);
      s_bots := s.(s_bots)
    |}
  | Bot bi =>
    let b := getBot s bi in
    setBot s bi {| b_pos := p; b_status := b.(b_status) |}
  end.

Definition destroyBot (s: State) (bi: nat) :=
  let b := getBot s bi in
  setBot s bi {| b_pos := b.(b_pos); b_status := NoBot |}.

Definition setBotStatus (s: State) (bi: nat) (st: BotStatus) :=
  let b := getBot s bi in
  setBot s bi {| b_pos := b.(b_pos); b_status := st |}.

Definition tileTypeAllowsPanel (t: TileType) :=
  match t with
  | Floor
  | GrayBlock
  | RedBlock
  | GoldBlock
  | Sphere _
  | Steel _
  | Wire _
  | Transponder
    => true
  | _ => false
  end.

Definition clsValid (cls: CellLayerState) :=
  match cls.(l_panel) with
  | None => true
  | _ => tileTypeAllowsPanel cls.(l_tile)
  end.

Definition isNoBot bs :=
  match bs with
  | NoBot => true | _ => false
  end.

Definition isNormalActingEntity s ei :=
  match ei with
  | Player => true
  | Bot bi => match (getBot s bi).(b_status) with
              | Hugbot | Dalek => true
              | _ => false
              end
  end.

Definition travel (s: State) (p: Pos) (d: Direction) :=
  match d with
  | Up => if p.(p_y) =? 0 then None
          else Some {| p_x := p.(p_x); p_y := p.(p_y) - 1 |}
  | Down => if p.(p_y) =? s.(s_height) - 1 then None
            else Some {| p_x := p.(p_x); p_y := p.(p_y) + 1 |}
  | Left => if p.(p_x) =? 0 then None
            else Some {| p_x := p.(p_x) - 1; p_y := p.(p_y) |}
  | Right => if p.(p_x) =? s.(s_width) - 1 then None
             else Some {| p_x := p.(p_x) + 1; p_y := p.(p_y) |}
  end.

Lemma travel_inBounds:
  forall (s: State) (p p': Pos) (d: Direction),
    (posInBounds s p).(bp_prop) -> travel s p d = Some p' ->
    (posInBounds s p').(bp_prop).
Proof.
  intros s p p' d Hpb Heq. destruct Hpb as [Hxb Hyb]. simpl in Hxb, Hyb.
  (* First prove the "match" version: slightly simpler *)
  assert (
      match travel s p d with
      | Some p' => (posInBounds s p').(bp_prop)
      | None => True
      end) as Hm.
  - destruct d; simpl.
    (* up *)
    + destruct (p.(p_y) =? 0) eqn:Ht. * exact I.
      * simpl. split. -- assumption. -- apply Nat.eqb_neq in Ht. lia.
    (* down *)
    + destruct (p.(p_y) =? s.(s_height) - 1) eqn:Ht. * exact I.
      * simpl. split. -- assumption. -- apply Nat.eqb_neq in Ht. lia.
    (* left *)
    + destruct (p.(p_x) =? 0) eqn:Ht. * exact I.
      * simpl. split. -- apply Nat.eqb_neq in Ht. lia. -- assumption.
    (* right *)
    + destruct (p.(p_x) =? s.(s_width) - 1) eqn:Ht. * exact I.
      * simpl. split. -- apply Nat.eqb_neq in Ht. lia. -- assumption.
  (* Back to equality formulation *)
  - destruct (travel s p d). + injection Heq as H0. rewrite <- H0. assumption.
    + discriminate Heq.
Qed.

Lemma travel_different:
  forall (s: State) (p p': Pos) (d: Direction),
    (posInBounds s p).(bp_prop) -> travel s p d = Some p' ->
    ~(Pos_eqbp p' p).(bp_prop).
Proof.
  intros s p p' d Hpb Heq Heqp'. simpl in Heqp'.
  destruct d; simpl in Heq.
  - destruct (p.(p_y) =? 0) eqn:Hyeq0. + discriminate Heq.
    + apply Nat.eqb_neq in Hyeq0. (* needed because nat subtraction stops at 0 *)
      injection Heq as Heq. subst p'. simpl in Heqp'. lia.
  - destruct (p.(p_y) =? s.(s_height) - 1). + discriminate Heq.
    + injection Heq as Heq. subst p'. simpl in Heqp'. lia.
  - destruct (p.(p_x) =? 0) eqn:Hxeq0. + discriminate Heq.
    + apply Nat.eqb_neq in Hxeq0. (* needed because nat subtraction stops at 0 *)
      injection Heq as Heq. subst p'. simpl in Heqp'. lia.
  - destruct (p.(p_x) =? s.(s_width) - 1). + discriminate Heq.
    + injection Heq as Heq. subst p'. simpl in Heqp'. lia.
Qed.

Definition cellInfoValid (s : State) (ci : CellInfo) :=
  (* A panel is only under a floor or an object that can move onto panels.
     It's dubious whether we need to check this, but it feels right. *)
  (leafBoolProp (clsValid ci.(c_state)))
    &&& leafBoolProp (clsValid ci.(c_ostate))
    (* Destination in bounds *)
    &&& posInBounds s ci.(c_dest).

Definition isBomb (st: BotStatus) :=
  match st with
  | Bomb _ _ => true
  | NoBot => ShouldNotReach false
  | _ => false
  end.

(* We validate only invariants that someone might reasonably care about, not
   every possible way a state could fail to be reachable from an initial state. *)
(* Note: Parentheses around each conjunct are not always needed by the Coq parser
   but are always needed by the automatic indentation. *)
(* TODO: Review uses of leafBoolProp, consider pushing BoolProp further in *)
(* TODO: Is there any way we can make the propositional version of this a record?  There
   are so many parts that it would be nice to access them by projection rather than by
   destructing with a heavily left-nested pattern. *)
Definition stateValid (s : State) :=
  (* Cells array is the claimed size *)
  (length s.(s_cells) =?? s.(s_height))
    &&& (forallbp (fun row => length row =?? s.(s_width)) s.(s_cells))
    &&& (forallbp (fun row => forallbp (cellInfoValid s) row) s.(s_cells))
    (* Entities in bounds *)
    &&& (posInBounds s s.(s_playerPos))
    &&& (forallbp (fun b => posInBounds s b.(b_pos)) s.(s_bots))
    (* Properties involving two bots... *)
    &&& forall_range_bp 0 (length s.(s_bots)) (
      fun j =>
        forall_range_bp 0 j (
                          fun i =>
                            let b1 := getBot s i in
                            let b2 := getBot s j in
                            (leafBoolProp (isNoBot b1.(b_status)))
                              ||| (leafBoolProp (isNoBot b2.(b_status)))
                              ||| (
                              (* No two bots (except NoBots) are in the same place *)
                              (negbp (Pos_eqbp b1.(b_pos) b2.(b_pos)))
                                (* Non-bombs before bombs (except NoBots).
                                   I.e., if b2 is not a bomb, then b1 must not be either. *)  
                                &&& match isBomb b1.(b_status), isBomb b2.(b_status) with
                                    | _, true => leafBoolProp true
                                    | true, _ => leafBoolProp false
                                    | _, _ => leafBoolProp true
                                    end
                              )
    ))
.

Definition stateValidAndCompatible (s s2 : State) :=
  (stateValid s2).(bp_prop) /\ statesCompatible s s2.

Lemma stateValidAndCompatible_reflexive:
  forall (s1: State), (stateValid s1).(bp_prop) -> stateValidAndCompatible s1 s1.
Proof.
  intros s1 Hv.
  split.
  - assumption.
  - apply statesCompatible_reflexive.
Qed.

(* Not exactly transitivity, but a form that will be useful in other proofs. *)
Lemma stateValidAndCompatible_transitive:
  forall (s1 s2 s3 : State),
    stateValidAndCompatible s1 s2 ->
    (* Compatibility here is sometimes useful when code makes several changes
       in a row based on information taken from the initial state. *)
    (stateValidAndCompatible s1 s2 -> stateValidAndCompatible s2 s3) ->
    stateValidAndCompatible s1 s3.
Proof.
  intros s1 s2 s3 H12 H23.
  destruct (H12) as [H2v H12c]. specialize (H23 H12). destruct H23 as [H3v H23c].
  split.
  - assumption.
  - unshelve eapply statesCompatible_transitive; cycle 1.
    + eassumption.
    + eassumption.
Qed.

Lemma getCell_valid:
  forall (s: State) (p: Pos),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    (cellInfoValid s (getCell s p)).(bp_prop).
Proof.
  intros s p Hv Hpb.
  destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
  unfold getCell.
  destruct Hpb as [Hxb Hyb].
  unshelve eapply (Forall_nth _ _ p.(p_x) _ (Forall_nth _ _ p.(p_y) _ hc _) _).
  - rewrite hh. assumption.
  - unshelve erewrite (Forall_nth _ _ p.(p_y) _ hw _).
    + rewrite hh. assumption.
    + assumption.
Qed.

Lemma getBot_valid:
  forall (s: State) (bi: nat),
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    (posInBounds s (getBot s bi).(b_pos)).(bp_prop).
Proof.
  intros s bi Hv Hbi.
  destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
  unfold getBot. apply Forall_nth.
  - assumption.
  - assumption.
Qed.

(* Replacement for "repeat split" now that it incorrectly splits hpb.
   We might do something like this for state compatibility later. *)
Ltac split_stateValid := split; [split; [split; [split; [split |] |] |] |].

(* XXX: We've set up the ShouldNotReach cases so that we don't actually need the
   posInBounds assumption.  Do we nevertheless want to force callers to prove it? *)
(* XXX: Want "compatible" here or is it obvious? *)
Lemma setCell_valid:
  forall (s: State) (p: Pos) (ci: CellInfo),
    (stateValid s).(bp_prop) ->
    (posInBounds s p).(bp_prop) -> (cellInfoValid s ci).(bp_prop) ->
    stateValidAndCompatible s (setCell s p ci).
Proof.
  intros s p ci Hv Hpb Hciv.
  unfold setCell.
  destruct (nth_error s.(s_cells) p.(p_y)) eqn:Heqrow;
    (* It would be philosophically better to rule this out from the posInBounds
       assumption rather than rely on the value of the ShouldNotReach,
       but I can't be bothered to do the extra work. *)
    [| destruct s; split; [assumption | apply statesCompatible_reflexive]].
  split.
  - destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
    split_stateValid; simpl.
    + (* hh *) rewrite length_update_nth. apply hh.
    + (* hw *) apply Forall_update_nth.
      * assumption.
      * rewrite length_update_nth.
        pose (Hyw := Forall_nth_error _ _ p.(p_y) _ hw Heqrow).
        assumption.
    + (* hc *)
      apply Forall_update_nth.
      * assumption.
      * apply Forall_update_nth.
        -- pose (Hyc := Forall_nth_error _ _ p.(p_y) _ hc Heqrow).
           assumption.
        -- assumption.
    + (* hpb *) assumption.
    + (* hbb *) assumption.
    + (* hb *) assumption.
  - repeat split; reflexivity.
Qed.

Lemma setTile_valid:
  forall (s: State) (p: Pos) (t: TileType),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    let c := getCell s p in
    (* Of course, the sub-hypothesis is provable, but providing it is a convenience. *)
    (clsValid c.(c_state) = true ->
      clsValid {| l_tile := t; l_panel := c.(c_state).(l_panel) |} = true) ->
    stateValidAndCompatible s (setTile s p t).
Proof.
  intros s p t Hv Hpb c Hlv.
  apply setCell_valid.
  - assumption.
  - assumption.
  - destruct (getCell_valid s p Hv Hpb) as [[Hcs Hcos] Hcd].
    split; [split |]; simpl.
    + apply Hlv. assumption.
    + assumption.
    + assumption.
Qed.

Lemma setTile_oldTypeDisallowsPanel_valid:
  forall (s: State) (p: Pos) (t: TileType),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    let c := getCell s p in
    tileTypeAllowsPanel c.(c_state).(l_tile) = false ->
    stateValidAndCompatible s (setTile s p t).
Proof.
  intros s p t Hv Hpb c Hap.
  apply setTile_valid.
  - assumption.
  - assumption.
  - intro Hov. unfold clsValid in Hov. fold c in Hov |- *. rewrite Hap in Hov.
    destruct c.(c_state).(l_panel).
    + discriminate Hov.
    + reflexivity.
Qed.

Lemma getCell_setCell_different:
  forall (s: State) (pg ps: Pos) (c: CellInfo),
    (stateValid s).(bp_prop) ->
    (posInBounds s pg).(bp_prop) -> (posInBounds s ps).(bp_prop) ->
    ~ (Pos_eqbp pg ps).(bp_prop) ->
    getCell (setCell s ps c) pg = getCell s pg.
Proof.
  intros s pg ps c Hv Hpg Hps Hdiff.
  destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
  destruct Hps as [Hxs Hys].
  simpl in Hdiff.
  unfold getCell. unfold setCell. simpl.
  destruct (Nat.eq_dec pg.(p_y) ps.(p_y)) as [Hy | Hy].
  - rewrite Hy. destruct (Nat.eq_dec pg.(p_x) ps.(p_x)) as [Hx | Hx].
    + (* This one can't happen. *)
      pose (conj Hx Hy). contradiction.
    + (* We could prove using hs that we won't hit the ShouldNotReach,
         but it's much easier to use its value. :( *)
      destruct (nth_error s.(s_cells) ps.(p_y)) as [row |] eqn:Heqrow.
      * rewrite nth_update_nth.
        -- rewrite mth_update_nth; [| apply Hx].
           (* A lemma will help.  For now, write it out.  This is a little obscure.
              Specifying an occurrence number instead of arguments to nth_default_eq didn't work. *)
           rewrite <- (nth_default_eq ps.(p_y) s.(s_cells) []).
           unfold nth_default. rewrite Heqrow. reflexivity.
        -- rewrite hh. apply Hys.
      * reflexivity.
  - destruct (nth_error s.(s_cells) ps.(p_y)) as [row |] eqn:Heqrow.
    + rewrite mth_update_nth; [| apply Hy]. reflexivity.
    + reflexivity.
Qed.

Lemma getEntityPos_inBounds:
  forall (s: State) (ei: EntityID),
    (stateValid s).(bp_prop) -> (entityIDValid s ei) -> (posInBounds s (getEntityPos s ei)).(bp_prop).
Proof.
  intros s ei Hv Hei.
  destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
  destruct ei as [| bi].
  - assumption.
  - apply (Forall_nth _ _ _ _ hbb). simpl in Hei. assumption.
Qed.

(* Internal things... *)
Definition findStartI (s: State) (p: Pos) (d: Direction) :=
  match d with
  | Up => p.(p_y)
  | Down => s.(s_height) - 1 - p.(p_y)
  | Left => p.(p_x)
  | Right => s.(s_width) - 1 - p.(p_x)
  end.

Definition findCandidatePos (s: State) (p: Pos) (d: Direction) (i: nat) :=
  match d with
  | Up => {| p_x := p.(p_x); p_y := i |}
  | Down => {| p_x := p.(p_x); p_y := s.(s_height) - 1 - i |}
  | Left => {| p_x := i; p_y := p.(p_y) |}
  | Right => {| p_x := s.(s_width) - 1 - i; p_y := p.(p_y) |}
  end.

Lemma findCandidatePos_inBounds:
  forall (s: State) (p: Pos) (d: Direction) (i: nat),
    (posInBounds s p).(bp_prop) -> i <= findStartI s p d ->
    (posInBounds s (findCandidatePos s p d i)).(bp_prop).
Proof.
  intros s p d i Hpb Hi.
  unfold findCandidatePos. unfold findStartI in Hi.
  simpl in Hpb. destruct Hpb as [Hxb Hyb].
  destruct d; split; simpl; lia.
Qed.

Definition findInDirection (s: State) (p: Pos) (d: Direction) (f : Pos -> bool) : (option Pos) :=
  let fix find1 i :=
      match i with
      | 0 => None
      | S i2 =>
        let candidatePos := findCandidatePos s p d i2 in
        if f candidatePos then Some candidatePos else find1 i2
      end in
  find1 (findStartI s p d).

Lemma findInDirection_inBounds:
  forall (s: State) (p p': Pos) (d: Direction) (f: Pos -> bool),
    (posInBounds s p).(bp_prop) -> findInDirection s p d f = Some p' ->
    (posInBounds s p').(bp_prop).
Proof.
  intros s p p' d f Hpb Heq.
  cbv beta delta [findInDirection] in Heq.
  remember (findStartI s p d) as i eqn:Heqi.
  assert (i <= findStartI s p d) as Hil; [lia |]. clear Heqi.
  induction i as [| i IHi].
  - discriminate.
  - simpl in Heq. destruct (f (findCandidatePos s p d i)).
    + injection Heq as Heq. rewrite <- Heq.
      apply findCandidatePos_inBounds.
      * assumption.
      * lia.
    + apply IHi.
      * apply Heq.
      * lia.
Qed.

Definition findLastConsecutiveInDirection (s: State) (p: Pos) (d: Direction) (f: Pos -> bool) : Pos :=
  let fix find1 i :=
      let here := findCandidatePos s p d i in
      match i with
      | 0 => here
      | S i2 =>
        if f (findCandidatePos s p d i2) then find1 i2 else here
      end in
  find1 (findStartI s p d).

Lemma findLastConsecutiveInDirection_inBounds:
  forall (s: State) (p: Pos) (d: Direction) (f: Pos -> bool),
    (posInBounds s p).(bp_prop) ->
    (posInBounds s (findLastConsecutiveInDirection s p d f)).(bp_prop).
Proof.
  intros s p d f Hpb.
  cbv beta delta [findLastConsecutiveInDirection].
  remember (findStartI s p d) as i eqn:Heqi.
  assert (i <= findStartI s p d) as Hil; [lia |]. clear Heqi.
  induction i as [| i IHi].
  - cbv beta iota zeta. apply findCandidatePos_inBounds. + apply Hpb. + apply Hil.
  - cbv beta iota zeta. destruct (f (findCandidatePos s p d i)).
    + apply IHi. lia.
    + apply findCandidatePos_inBounds. * apply Hpb. * apply Hil.
Qed.

Lemma findLastConsecutiveInDirection_satisfies:
  forall (s: State) (p: Pos) (d: Direction) (f: Pos -> bool),
    (posInBounds s p).(bp_prop) ->
    let p' := findLastConsecutiveInDirection s p d f in
    (Pos_eqbp p' p).(bp_prop) \/ f p' = true.
Proof.
  intros s p d f Hpb p'.
  cbv beta delta [findLastConsecutiveInDirection] in p'.
  remember (findStartI s p d) as i eqn:Heqi.
  assert (findCandidatePos s p d i = p) as Hfcpi.
  - destruct p as [x y]. unfold findCandidatePos. unfold findStartI in Heqi. simpl in Heqi |- *.
    destruct Hpb as [Hxb Hyb]. simpl in Hxb, Hyb.
    destruct d; f_equal; lia.
  - remember p as p1 eqn:Heqp1 in Hfcpi at 2.
    assert ((Pos_eqbp p1 p).(bp_prop) \/ f p1 = true) as Hp1.
    + left. rewrite Heqp1. split; reflexivity.
    + rewrite <- Hfcpi in Hp1.
      clear Heqp1 Hfcpi p1.
      assert (i <= findStartI s p d) as Hil; [lia |]. clear Heqi.
      induction i as [| i IHi].
      * cbv beta iota zeta in p'. apply Hp1.
      * cbv beta iota zeta in p'.
        destruct (f (findCandidatePos s p d i)) eqn:Hfi.
        -- apply IHi.
           ++ right. reflexivity.
           ++ lia.
        -- apply Hp1.
Qed.

Fixpoint botat1 (p: Pos) (bs: list BotInfo) :=
  match bs with
  | nil => None
  | b :: bs2 =>
    if negb (isNoBot b.(b_status)) && Pos_beq b.(b_pos) p
    then Some 0
    else option_map S (botat1 p bs2)
  end.

Definition botat (s: State) (p: Pos) := botat1 p s.(s_bots).

Variant EntityAtResult :=
| EA_None
| EA_One (ei: EntityID)
| EA_PlayerAndBot (bi: nat).

Definition entityAt (s: State) (p: Pos) :=
  match Pos_beq s.(s_playerPos) p, botat s p with
  | true, Some bi => EA_PlayerAndBot bi
  | false, Some bi => EA_One (Bot bi)
  | true, None => EA_One Player
  | false, None => EA_None
  end.

Definition botIsPresent (s: State) (p: Pos) (bi: nat) :=
  bi < length s.(s_bots) /\
  isNoBot (getBot s bi).(b_status) = false /\
  (* Uglier than simple equality to p, but most callers want this. *)
  (Pos_eqbp (getBot s bi).(b_pos) p).(bp_prop).

(* Weaker statement first, then we'll get the full statement using hb *)
Lemma botat_valid1 :
  forall (s: State) (p: Pos) (bi: nat),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    (botat1 p s.(s_bots) = Some bi -> botIsPresent s p bi) /\
    (botIsPresent s p bi -> botat1 p s.(s_bots) <> None).
Proof.
  intros s p bi Hv Hpb. unfold botat.
  unfold botIsPresent. unfold getBot.
  remember s.(s_bots) as l eqn:Heql. clear s Heql Hv Hpb.
  revert bi. induction l as [| b l IHl]; intros bi.
  - (* Empty list *)
    simpl. split.
    + intro H. discriminate H.
    + intro H. lia.
  - (* Nonempty list *)
    simpl.
    destruct (negb (isNoBot b.(b_status)) && Pos_beq b.(b_pos) p) eqn:Heqmatch. (* less oink *)
    + (* Found here *) split.
      * intro Hbi. injection Hbi as Hbi. subst bi. split; [lia |].
        apply andb_prop in Heqmatch. destruct Heqmatch as [Hnobot Hpos]. split.
        -- destruct (isNoBot b.(b_status)); try discriminate Hnobot. reflexivity.
        -- apply (Pos_eqbp b.(b_pos) p).(bp_equiv). assumption.
      * intros Hpres H. discriminate H.
    + destruct (botat1 p l) as [bi1 |] eqn:Hba1.
      * (* Found later on *) simpl. split.
        -- intro Hbi. injection Hbi as Hbi. subst bi.
           destruct (IHl bi1) as [H _]. unshelve epose (H1 := H _); [reflexivity |].
           destruct H1 as [Hl Hrest]. split.
           ++ lia. ++ assumption.
        -- intros Hmatch H. discriminate H.
      * (* Not found later on *) simpl. split.
        -- intro H. discriminate H.
        -- intro Hmatch. destruct bi as [| bi].
           ++ (* Disagreement about whether the first matches *)
             destruct Hmatch as [Hl [Hnobot Hpos]].
             apply Bool.andb_false_elim in Heqmatch. destruct Heqmatch as [Hnobot' | Hpos'].
             ** rewrite Hnobot in Hnobot'. discriminate Hnobot'.
             ** apply (Pos_eqbp b.(b_pos) p).(bp_equiv) in Hpos.
                unfold Pos_beq in Hpos'. rewrite Hpos in Hpos'. discriminate Hpos'.
           ++ (* Disagreement about a later match *)
             destruct (IHl bi) as [_ H]. apply H.
             destruct Hmatch as [Hl Hrest]. split.
             ** lia. ** assumption.
Qed.

Require Arith.Compare_dec.

Lemma botat_valid :
  forall (s: State) (p: Pos) (bi: nat),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    botat s p = Some bi <-> botIsPresent s p bi.
Proof.
  intros s p bi Hv Hpb.
  split.
  - apply botat_valid1; assumption.
  - intro Hbp. destruct (botat s p) as [bi' |] eqn:Heqba.
    + apply botat_valid1 in Heqba; try assumption.
      (* now the argument using hb *)
      f_equal.
      (* We'll have to do this twice depending on the comparison, so assert first *)
      assert (forall (bi1 bi2 : nat),
                 bi1 < bi2 -> botIsPresent s p bi1 -> botIsPresent s p bi2 -> False) as H.
      * intros bi1 bi2 H12 Hp1 Hp2.
        destruct Hv as [[[[[_ _] _] _] _] hb].
        destruct Hp1 as [Hl1 [Hnobot1 Hpos1]].
        destruct Hp2 as [Hl2 [Hnobot2 Hpos2]].
        unshelve epose (hb' := hb bi2 _ bi1 _).
        -- lia.
        -- lia.
        -- clearbody hb'. simpl in hb'.
           destruct hb' as [[hnb1 | hnb2] | hboth].
           ++ rewrite Hnobot1 in hnb1. discriminate hnb1.
           ++ rewrite Hnobot2 in hnb2. discriminate hnb2.
           ++ (* This is silly.  Should I change the definition of Pos_eqbp?
                 Can I avoid huge disruption? *)
             assert (Pos_eqbp (getBot s bi1).(b_pos) (getBot s bi2).(b_pos)).(bp_prop) as Hpos.
             ** simpl in Hpos1, Hpos2.
                destruct Hpos1 as [Hpos1x Hpos1y]. destruct Hpos2 as [Hpos2x Hpos2y].
                simpl. split.
                --- rewrite Hpos1x. rewrite Hpos2x. reflexivity.
                --- rewrite Hpos1y. rewrite Hpos2y. reflexivity.
             ** destruct hboth as [Hnotpos _]. contradiction.
      * destruct (Compare_dec.lt_eq_lt_dec bi bi') as [[Hc | Hc] | Hc].
        -- exfalso. apply (H bi bi'); assumption.
        -- symmetry. assumption.
        -- exfalso. apply (H bi' bi); assumption.
    + apply botat_valid1 in Hbp; try assumption.
      contradiction.
Qed.

(* Note: Despite the name, this is a much weaker statement than botat_valid. *)
Lemma entityAt_valid:
  forall (s: State) (p: Pos),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    match entityAt s p with
    | EA_None => True
    | EA_One ei => entityIDValid s ei
    | EA_PlayerAndBot bi => bi < length s.(s_bots)
    end.
Proof.
  intros s p Hv Hpb.
  unfold entityAt.
  destruct (Pos_beq s.(s_playerPos) p) eqn:Hpp;
    destruct (botat s p) as [bi |] eqn:Hba.
  - eapply botat_valid; eassumption.
  - exact I.
  - eapply botat_valid; eassumption.
  - exact I.
Qed.

Definition anyBotAt (s: State) (p: Pos) :=
  match botat s p with
  | None => false
  | Some _ => true
  end.

Definition anyEntityAt (s: State) (p: Pos) :=
  Pos_beq s.(s_playerPos) p || anyBotAt s p.
(* Equivalent definition would be: entityAt s p <> EA_None *)

Definition allowbeam (t: TileType) :=
  match t with
  (* Floorlike tiles *)
  | Floor
  | Rough
  | Trap2
  | Trap1
  | Togglable false _
  (* A few more *)
  | BlackSpace
  | Hole
  | Electric
    => true
  | _ => false
  end.

(* Can apply to both lasers and transponders *)
Definition beamStopped (s: State) (p: Pos) :=
  negb (allowbeam (getCell s p).(c_state).(l_tile)) || anyEntityAt s p.

(* Precondition: stateValid *)
Definition stateIsSeverelyLost (s: State) :=
  (* Player on bot *)
  (leafBoolProp (anyBotAt s s.(s_playerPos)))
    (* Player killed by bomb *)
    ||| leafBoolProp s.(s_playerBombed).

Definition stateIsLost (s: State) :=
  let isLaser p :=
      match (getCell s p).(c_state).(l_tile) with
      | Laser => true
      | _ => false
      end
  in
  (* Laser hit *)
  existsbp (fun d => leafBoolProp (match findInDirection s s.(s_playerPos) d (beamStopped s) with
                                   | None => false
                                   | Some p => isLaser p
                                   end)) allDirections
  (* Player on bot or killed by bomb *)
  ||| stateIsSeverelyLost s.

(* Precondition: stateValid and not stateIsLost *)
Definition stateIsWon (s: State) :=
  leafBoolProp (match (getCell s s.(s_playerPos)).(c_state).(l_tile) with
                | Exit => true
                | _ => false
                end).

(*
- blocks on panels
- lit bombs
- dead bots
- player on bot
- player killed by bomb
 *)

Definition noBlockOnPanel (l: CellLayerState) :=
  match l.(l_tile), l.(l_panel) with
  | Floor, _ => leafBoolProp true
  | _, Some _ => leafBoolProp false
  | _, None => leafBoolProp true
  end.

(* Precondition: stateValid *)
Definition stateIsInitial (s: State) :=
  (negbp (stateIsSeverelyLost s))
    (* No blocks on panels *)
    &&& forallbp (fun row =>
                    forallbp (fun c =>
                                noBlockOnPanel c.(c_state) &&& noBlockOnPanel c.(c_ostate)) row) s.(s_cells)
    (* No lit bombs or dead bots *)
    &&& forallbp (fun e => match e.(b_status) with
                           | NoBot => leafBoolProp false
                           | Bomb _ None => leafBoolProp true
                           | Bomb _ (Some _) => leafBoolProp false
                           | _ => leafBoolProp true
                           end) s.(s_bots).

Definition entityMayEnter (s: State) (ei: EntityID) (p: Pos) :=
  match ei with
  | Player => True
  | Bot _ => anyBotAt s p = false
  end.

Lemma moveEntity_valid:
  forall (s: State) (ei: EntityID) (p: Pos),
    (stateValid s).(bp_prop) -> entityIDValid s ei ->
    (posInBounds s p).(bp_prop) -> entityMayEnter s ei p ->
    stateValidAndCompatible s (moveEntity s ei p).
Proof.
  intros s ei p Hv Hei Hpb Haba.
  set (s' := moveEntity s ei p).
  assert (length s'.(s_bots) = length s.(s_bots)) as Hle.
  - unfold moveEntity. unfold setBot. destruct ei.
    + simpl. reflexivity.
    + apply length_update_nth.
  - split.
    + destruct (Hv) as [[[[[hh hw] hc] hpb] hbb] hb].
      (* It's going to be easiest to give separate proofs for player and bot. *)
      destruct ei as [| bi]; split_stateValid; try assumption.
      (* Left with two subgoals for bot case *)
      * (* hbb *) apply Forall_update_nth; assumption.
      * (* hb *)
        (* Assertions: *)
        (* (1) Bots other than bi do not change from s to s'. *)
        assert (forall k, k <> bi -> getBot s' k = getBot s k) as Hkss'.
        -- intros k Hk. unfold s'. unfold moveEntity. unfold setBot. unfold getBot. simpl.
           rewrite mth_update_nth; [reflexivity | assumption].
        (* (2) Bots (including bi) do not change status from s to s'. *)
        -- assert (forall k, (getBot s' k).(b_status) = (getBot s k).(b_status)) as Hst.
           ++ intros k. unfold s'. unfold moveEntity. unfold setBot. unfold getBot. simpl.
              destruct (Nat.eq_dec k bi).
              ** subst k. rewrite nth_update_nth; [reflexivity | assumption].
              ** rewrite mth_update_nth; [reflexivity | assumption].
           (* (3) Existing bots other than bi are not at p in s' *)
           ++ assert (forall k, k < length s'.(s_bots) -> k <> bi ->
                             isNoBot (getBot s' k).(b_status) = false ->
                             ~(Pos_eqbp (getBot s' bi).(b_pos) (getBot s' k).(b_pos)).(bp_prop)) as Hkp.
              ** intros k Hkl Hkbi Hknb Hkp'.
                 assert ((getBot s' bi).(b_pos) = p) as Hbip.
                 --- unfold s'. unfold moveEntity. unfold setBot. unfold getBot. simpl.
                     rewrite nth_update_nth; [reflexivity | assumption].
                 --- rewrite Hbip in Hkp'. rewrite (Hkss' k Hkbi) in Hknb, Hkp'.
                     assert (botIsPresent s p k) as Hkpres.
                     +++ split; [| split].
                         *** lia.
                         *** assumption.
                         *** simpl in Hkp' |- *. lia. (* symmetry *)
                     +++ apply botat_valid in Hkpres; [| assumption | assumption].
                         simpl in Haba. unfold anyBotAt in Haba. rewrite Hkpres in Haba. discriminate Haba.
              (* Main proof *)
              ** intros j Hj i Hi. simpl.
                 (* First rule out NoBot cases (needed to apply Hkp later, not just for hb1) *)
                 destruct (isNoBot (getBot s' i).(b_status)) eqn:Hinbot; [left; left; reflexivity |].
                 destruct (isNoBot (getBot s' j).(b_status)) eqn:Hjnbot; [left; right; reflexivity |].
                 right.
                 (* Have previous validity handy *)
                 unshelve epose (hb1 := hb j _ i _); [lia | lia |]; clearbody hb1. cbv beta in hb1.
                 destruct hb1 as [[Hinb | Hjnb] | [Hijp Hijbo]].
                 --- simpl in Hinb. rewrite (Hst i) in Hinbot. rewrite Hinbot in Hinb. discriminate Hinb.
                 --- simpl in Hjnb. rewrite (Hst j) in Hjnbot. rewrite Hjnbot in Hjnb. discriminate Hjnb.
                 --- split.
                     +++ destruct (Nat.eq_dec i bi) as [Hie | Hine].
                         *** subst i. apply Hkp; assumption || lia.
                         *** destruct (Nat.eq_dec j bi) as [Hje | Hjne].
                             ---- subst j. (* fiddle for symmetry *)
                                  unshelve epose (Hip := Hkp i _ _ _); [lia | assumption | assumption |]. clearbody Hip.
                                  intros [Hix' Hiy']. symmetry in Hix', Hiy'. apply Hip. split; assumption.
                             ---- (* Both unequal: refer to previous validity *)
                               rewrite (Hkss' i Hine). rewrite (Hkss' j Hjne). apply Hijp.
                     +++ (* bomb order: refer to previous *)
                       rewrite (Hst i). rewrite (Hst j). apply Hijbo.
    + destruct ei; repeat split. lia.
Qed.

Lemma moveBot_noBotLeft:
  forall (s: State) (bi: nat) (p: Pos),
    (* The first few hypotheses are the same as those of moveEntity_valid,
       specialized to ei = Bot bi. *)
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    (posInBounds s p).(bp_prop) -> anyBotAt s p = false ->
    isNoBot (getBot s bi).(b_status) = false ->
    Pos_beq p (getBot s bi).(b_pos) = false ->
    anyBotAt (moveEntity s (Bot bi) p) (getBot s bi).(b_pos) = false.
Proof.
  intros s bi p Hv Hbi Hpb Heme Hnb Hpd.
  assert (posInBounds s (getBot s bi).(b_pos)).(bp_prop) as Hopb.  (* TODO: Lemma?  getBot_inBounds? *)
  - destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
    unfold getBot. apply Forall_nth. + apply hbb. + assumption.
  - pose (Hmvc := moveEntity_valid s (Bot bi) p Hv Hbi Hpb Heme).
    destruct (Hmvc) as [Hmv Hmc].
    unfold anyBotAt.
    destruct (botat (moveEntity s (Bot bi) p) (getBot s bi).(b_pos)) as [bi' |] eqn:Hba; [| reflexivity].
    apply botat_valid in Hba; cycle 1.
    (* Why is a hypothesis H generated here?? *)
    + apply Hmv.
    + apply (posInBounds_compatible s).
      * apply Hmc.
      * apply Hopb.
    + assert (bi <> bi') as Hbine.
      * intro Hbine'. destruct Hba as [Hbal [Hbas Hbap]].
        rewrite <- Hbine' in Hbap.
        unfold moveEntity in Hbap. unfold setBot in Hbap. unfold getBot in Hbap. simpl in Hbap.
        rewrite nth_update_nth in Hbap; [| assumption]. simpl in Hbap.
        apply (Pos_eqbp p (getBot s bi).(b_pos)).(bp_equiv) in Hbap.
        unfold Pos_beq in Hpd. rewrite Hbap in Hpd. discriminate Hpd.
      * assert (botIsPresent s (getBot s bi).(b_pos) bi') as Hbip'.
        -- unfold botIsPresent in Hba |- *.
           unfold moveEntity in Hba. unfold setBot in Hba. unfold getBot in Hba. simpl in Hba.
           rewrite mth_update_nth in Hba.
           ++ rewrite length_update_nth in Hba. apply Hba.
           ++ lia.
       -- assert (botIsPresent s (getBot s bi).(b_pos) bi) as Hbip.
          ++ split; [| split].
             ** assumption.
             ** assumption.
             ** simpl. lia.
          ++ apply botat_valid in Hbip'; cycle 1.
             ** assumption.
             ** apply Hopb.
             ** apply botat_valid in Hbip; cycle 1.
                 --- assumption.
                 --- apply Hopb.
                 --- rewrite Hbip in Hbip'. injection Hbip' as Hbip'. contradiction.
Qed.

Lemma destroyBot_valid:
  forall (s: State) (bi: nat),
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    stateValidAndCompatible s (destroyBot s bi).
Proof.
  intros s bi Hv Hbi.
  assert (length (destroyBot s bi).(s_bots) = length s.(s_bots)) as Hle.
  - unfold destroyBot. unfold setBot. simpl. apply length_update_nth.
  - split.
    + destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
      split_stateValid; try assumption.
      * (* hbb *)
        apply Forall_update_nth; [assumption |].
        simpl. unshelve eapply (Forall_nth _ _ _ _ hbb _). assumption.
      * (* hb *)
        intros j Hj i Hi. unfold destroyBot. unfold setBot. unfold getBot. simpl.
        destruct (Nat.eq_dec i bi) as [Hie | Hine].
        -- rewrite Hie in *. rewrite nth_update_nth; [| lia]. simpl.
           left. left. reflexivity. (* the destroyed bot *)
        -- destruct (Nat.eq_dec j bi) as [Hje | Hjne].
           ++ rewrite Hje in *. rewrite nth_update_nth; [| lia]. simpl.
              left. right. reflexivity. (* the destroyed bot again *)
           ++ rewrite mth_update_nth; [| lia]. rewrite mth_update_nth; [| lia].
              apply hb. ** rewrite Hle in Hj. assumption. ** assumption.
    + repeat split. lia.
Qed.

Lemma setBotStatus_valid:
  forall (s: State) (bi: nat) (st: BotStatus),
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    let b0 := getBot s bi in
    isNoBot b0.(b_status) = false -> isBomb st = isBomb b0.(b_status) ->
    stateValidAndCompatible s (setBotStatus s bi st).
Proof.
  intros s bi st Hv Hbi b0 Hb0nb Hstb.
  assert (length (setBotStatus s bi st).(s_bots) = length s.(s_bots)) as Hle.
  - unfold setBotStatus. unfold setBot. simpl. apply length_update_nth.
  - split.
    + destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
      split_stateValid; try assumption.
      * (* hbb *)
        apply Forall_update_nth; [assumption |].
        simpl. unshelve eapply (Forall_nth _ _ _ _ hbb _). lia.
      * (* hb *)
        (* So, here's another proof structured like moveEntity_valid used to be,
           with duplication between i = bi and j = bi cases.  But this time, the
           amount of code duplicated is less, so I'll leave it. :/ *)
        intros j Hj i Hi. unfold setBotStatus. unfold setBot. unfold getBot. simpl.
        unshelve epose (hb1 := hb j _ i _); [lia | lia |]. clearbody hb1. simpl in hb1.
        destruct (Nat.eq_dec i bi) as [Hie | Hine].
        -- rewrite Hie in *. rewrite nth_update_nth; [| lia]. simpl.
           rewrite mth_update_nth; [| lia].
           destruct hb1 as [[hb1 | hb1] | hb1].
           ++ (* This contradicts Hb0nb *)
             unfold b0 in Hb0nb. rewrite Hb0nb in hb1. discriminate hb1.
           ++ left. right. assumption.
           ++ right. destruct hb1 as [hbp1 hbb1].
              split.
              ** apply hbp1.
              ** unfold b0 in Hstb. rewrite Hstb. apply hbb1.
        -- destruct (Nat.eq_dec j bi) as [Hje | Hjne].
           ++ rewrite Hje in *. rewrite nth_update_nth; [| lia]. simpl.
              rewrite mth_update_nth; [| lia].
              destruct hb1 as [[hb1 | hb1] | hb1].
              ** left. left. assumption.
              ** (* This contradicts Hb0nb *)
                unfold b0 in Hb0nb. rewrite Hb0nb in hb1. discriminate hb1.
              ** right. destruct hb1 as [hbp1 hbb1].
                 split.
                 --- apply hbp1.
                 --- unfold b0 in Hstb. rewrite Hstb. apply hbb1.
           ++ rewrite mth_update_nth; [| lia]. rewrite mth_update_nth; [| lia].
              apply hb. ** rewrite Hle in Hj. assumption. ** assumption.
    + repeat split. symmetry. assumption.
Qed.

(* setBotStatus on an existing bot cannot create a bot at a position (but may remove one).
   This unidirectional result is what we need for proving action validity. *)
(* I'm being more liberal than usual about operating on potentially invalid data.
   See if I want to change this later. *)
Lemma setBotStatus_noMoreBots:
  forall (s: State) (bi: nat) (st: BotStatus) (bi': nat) (p: Pos),
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    (isNoBot (getBot s bi).(b_status) = false \/ st = NoBot) ->
    botIsPresent (setBotStatus s bi st) p bi' -> botIsPresent s p bi'.
Proof.
  intros s bi st bi' p Hv Hbi Hbis Hbp.
  destruct Hbp as [Hbpl [Hbps Hbpp]].
  (* As things stand, we can't use setBotStatus_valid for compatibility
     because we don't have all the hypotheses.  Fortunately, the proof is
     easy. *)
  unfold setBotStatus in Hbpl. unfold setBot in Hbpl. simpl in Hbpl.
  rewrite length_update_nth in Hbpl.
  unfold setBotStatus in Hbps, Hbpp. unfold setBot in Hbps, Hbpp.
  unfold getBot in Hbps, Hbpp. simpl in Hbps, Hbpp.
  destruct (Nat.eq_dec bi' bi).
  - subst bi'. rewrite nth_update_nth in Hbpp, Hbps; [| assumption | assumption].
    simpl in Hbps.
    destruct Hbis as [Hbis | Hbis].
    + split; [| split].
      * assumption.
      * assumption.
      * assumption.
    + rewrite Hbis in Hbps. discriminate Hbps.
  - rewrite mth_update_nth in Hbpp, Hbps; [| assumption | assumption].
    split; [| split].
    + assumption.
    + assumption.
    + assumption.
Qed.
