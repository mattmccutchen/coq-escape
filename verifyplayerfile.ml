open Escape;;
open Util;;
open Highlevel;;
open Playerfile;;
open Leveltree;;

(* Parse the player file and check all solutions and bookmarks. *)
let verifyPlayerFile inputPlayerFile inputLevelRoot =
  let pfInChannel = open_in_bin inputPlayerFile in
  let pf = readPlayerFileFromChannel pfInChannel in
  close_in pfInChannel;
  let levelPaths = scanLevels inputLevelRoot in
  let checkOneLevel lp =
    let levInChannel = open_in_bin lp in
    let buf = Buffer.create 0 in
    fullRead levInChannel buf;
    close_in levInChannel;
    let binDigest = Digest.string (Buffer.contents buf) in
    let hexDigest = Digest.to_hex binDigest (* for diagnostics *) in
    match StringMap.find_opt binDigest pf with
    | None -> ()  (* nothing to do, no need to decode the esx *)
    | Some nmsList ->
      let ts = bufferToTitledState buf in
      let checkOneNms nmsi nms =
        let outPfx = Printf.sprintf "%s (%s) NMS %d" lp hexDigest nmsi in
        let pr = play ts.ts_state nms.moves in
        let movesPlayed = nat_to_int pr.pr_numMoves in
        let isGood =
        if movesPlayed < List.length nms.moves
        then (
          let why = match pr.pr_status with
                    | Okay -> "illegal move"
                    | Lost -> "loss"
                    | Won -> "win"
          in Printf.printf "%s: playback stopped at %d moves due to %s\n" outPfx movesPlayed why; false
        ) else (
          match nms.msType with
          | MS_Solution ->
            if pr.pr_status = Won
            then (Printf.printf "%s: valid solution\n" outPfx; true)
            else (Printf.printf "%s: claimed to be a solution but reached the end without a win\n" outPfx; false)
          | MS_Bookmark -> Printf.printf "%s: valid bookmark\n" outPfx; true
        ) in
        if not isGood then (
	  let rec writeSnapshots i s =
	    if i > 0 then (
	      if stateIsInitial s then (
                let ts2 = {
                  ts_title = Printf.sprintf "%s (NMS %d after %d moves)" ts.ts_title nmsi i;
                  ts_author = ts.ts_author;
                  ts_state = s
                } in
                let outBuf = titledStateToBuffer ts2 in
		let outPath = Printf.sprintf "%s_%d_%d" lp nmsi i in
                let outChannel = open_out_bin outPath in
                Buffer.output_buffer outChannel outBuf;
                close_out outChannel;
                Printf.printf "  - State after %d moves written to %s\n" i outPath
              ) else
                Printf.printf "  - State after %d moves could not be written because it is not a valid initial state.  Sorry.\n" i
            );
            if i < movesPlayed then
              match attemptRound s (List.nth nms.moves i) with
              | Some s2 -> writeSnapshots (i + 1) s2
              | None -> failwith "Internal error: move was legal before but is illegal now?"
	  in
	  writeSnapshots 0 ts.ts_state
	)
      in List.iteri checkOneNms nmsList
  in List.iter checkOneLevel levelPaths
;;
