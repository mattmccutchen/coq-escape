Require Export StateHelpers.

Variant TileClass :=
(* Entities may be able to walk onto this tile even if there is an entity there,
   which will crush or push the existing entity.  Entities can also be pushed onto this tile. *)
| Floorlike
(* Entities may be able to act on this tile only if no entity is standing on it.
   Entities cannot be crushed on or pushed from this tile, nor pushed onto it
   (except Electric, which is checked specially in the appropriate place). *)
| Actable
(* No action is possible on this tile, nor can entities be pushed onto it. *)
| Unactable.

Definition classifyTile (t: TileType) :=
  match t with
  | Floor | Rough | Trap2 | Trap1
  | Togglable false _ =>
    Floorlike

  | BlackSpace | SleepingDoor | Laser
  | Hole | BlueBlock | StopSign | ArrowSign _
  | ButtonOff | Remote | Light _
  | Togglable true _ =>
    Unactable

  (* Action by moving/pushing *)
  | Exit | HeartFramer
  | GrayBlock | RedBlock | Steel _
  | GreenBlock | Electric
  | SliderVert | SliderHoriz
  | Wire _ | Transponder
  | Teleporter (* It doesn't really matter if this is considered "with moving" *)
  (* Action without moving *)
  | GoldBlock | Sphere _
  | BrokenBlock | ButtonOn | Button0 | Button1
  | Button =>
    Actable
  end.

Definition Flips := list Pos.

Definition flipsValid (s: State) (flips: Flips) :=
  Forall (fun f => (posInBounds s f).(bp_prop)) flips.

Lemma flipsValid_compatible:
  forall (s s2: State) (flips: Flips),
    statesCompatible s s2 -> flipsValid s flips -> flipsValid s2 flips.
Proof.
  intros s s2 flips Hc Hsv.
  unshelve eapply (Forall_impl _ _ Hsv).
  - cbv beta. intros p Hp.
    eapply posInBounds_compatible.
    + apply Hc.
    + apply Hp.
Qed.

Definition applyFlip (s: State) (f: Pos) :=
  let c_old := getCell s f in
  setCell s f {|
            c_state := c_old.(c_ostate);
            c_ostate := c_old.(c_state);
            c_dest := c_old.(c_dest)|}.

Definition applyFlips (s: State) (flips: Flips) :=
  fold_left applyFlip flips s.

Lemma applyFlips_valid:
  forall (s: State) (flips: Flips),
    (stateValid s).(bp_prop) -> flipsValid s flips ->
    stateValidAndCompatible s (applyFlips s flips).
Proof.
  intros s flips Hv Hfv.
  revert s Hv Hfv; induction flips as [| f flips IHflips]; intros s Hv Hfv.
  - apply stateValidAndCompatible_reflexive. assumption.
  - simpl.
    set (s' := applyFlip s f).
    inversion Hfv as [| ? ? Hf0v Hftv].  (* Inversion is a mess :( *)
    apply (stateValidAndCompatible_transitive _ s' _).
    + apply setCell_valid.
      * assumption.
      * assumption.
      * destruct (getCell_valid s f Hv Hf0v) as [[Hsv Hosv] Hdv].
        split; [split |]; simpl.
        -- assumption.
        -- assumption.
        -- assumption.
    + intros [Hv' Hc']. apply IHflips.
      * assumption.
      * eapply flipsValid_compatible.
        -- eassumption.
        -- assumption.
Qed.

(* A thing (entity or tile per isEntity) of color "color" is entering or leaving pos.
 * If appropriate, generate a flip.  Note that an entity entering or leaving a
 * panel covered by a movable block does not generate a flip, but a movable
 * block leaving a panel covered by a movable block (namely, itself) does generate a flip.
 * (A movable block should never enter a panel covered by a movable block.) *)
Definition doesPanelTrigger (s: State) (p: Pos) (isEntity: bool) (color: option Color) :=
  let c := getCell s p in
  let panelExposed :=
      match c.(c_state).(l_tile), isEntity with
      | _, false | Floor, _ => true
      | _, true => false
      end in
  let colorMatch :=
      match c.(c_state).(l_panel), color with
      | None, _ => false  (* No panel at all *)
      | Some None, _ => true  (* Colorless panel matches anything *)
      | Some (Some _), None => false  (* Colored panel doesn't match colorless thing *)
      (* Colored panel matches colored thing if the colors are the same *)
      | Some (Some pcolor), Some ocolor => Color_beq pcolor ocolor
      end in
  panelExposed && colorMatch.

Definition checkPanel (s: State) (p: Pos) (isEntity: bool) (color: option Color) :=
  let c := getCell s p in
  if doesPanelTrigger s p isEntity color then [c.(c_dest)] else [].

Definition checkPanel_flipsValid:
  forall (s: State) (p: Pos) (isEntity: bool) (color: option Color),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    flipsValid s (checkPanel s p isEntity color).
Proof.
  intros s p isEntity color Hv Hpb.
  unfold checkPanel.
  destruct (doesPanelTrigger s p isEntity color).
  - apply Forall_cons.
    + destruct (getCell_valid s p Hv Hpb) as [[Hsv Hosv] Hdv].
      assumption.
    + apply Forall_nil.
  - apply Forall_nil.
Qed.

(* Convenience for a thing leaving one position and entering another. *)
Definition checkPanels (s: State) (p1 p2: Pos) (isEntity: bool) (color: option Color) :=
  checkPanel s p1 isEntity color ++ checkPanel s p2 isEntity color.

Definition checkPanels_flipsValid:
  forall (s: State) (p1 p2: Pos) (isEntity: bool) (color: option Color),
    (stateValid s).(bp_prop) -> (posInBounds s p1).(bp_prop) -> (posInBounds s p2).(bp_prop) ->
    flipsValid s (checkPanels s p1 p2 isEntity color).
Proof.
  intros s p1 p2 isEntity color Hv Hp1b Hp2b.
  unfold checkPanels.
  apply Forall_app. split; apply checkPanel_flipsValid; assumption.
Qed.

Definition degradeTrap (s: State) (p: Pos) :=
  let c := getCell s p in
  match c.(c_state).(l_tile) with
  | Trap2 => setTile s p Trap1
  | Trap1 => setTile s p Hole
  | _ => s
  end.

Lemma degradeTrap_valid:
  forall (s: State) (p: Pos),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    stateValidAndCompatible s (degradeTrap s p).
Proof.
  intros s p Hv Hpb.
  unfold degradeTrap.
  assert (stateValidAndCompatible s s);
    [apply stateValidAndCompatible_reflexive; assumption |].
  destruct (getCell s p).(c_state).(l_tile) eqn:Heqt;
    (* If we "apply stateValidAndCompatible_reflexive; assumption" here, the apply
       takes ~19 s to fail on the nontrivial cases. *)
    assumption
    ||
    (* Same proof script for 2 cases *)
    (apply setTile_oldTypeDisallowsPanel_valid; assumption || (rewrite Heqt; reflexivity)).
Qed.

(* Many successful actions involve the entity walking onto the new tile without changing
   status.  In addition to changing the entity's position, the walk can trigger trap
   degradation and panels.  defaultWalk returns a new state and flip list as a starting
   point for action implementations that may make further changes. *)
Definition defaultWalk (s: State) (ei: EntityID) (oldPos: Pos) (newPos: Pos) :=
  (degradeTrap (moveEntity s ei newPos) oldPos,
   checkPanels s oldPos newPos true None).

Definition stateValidAndCompatibleWithValidFlips (s s2: State) (flips: Flips) :=
  stateValidAndCompatible s s2 /\
  Forall (fun f => (posInBounds s f).(bp_prop)) flips.

Lemma defaultWalk_valid:
  forall (s: State) (ei: EntityID) (oldPos: Pos) (newPos: Pos),
    (stateValid s).(bp_prop) -> entityIDValid s ei ->
    (posInBounds s oldPos).(bp_prop) -> (posInBounds s newPos).(bp_prop) ->
    entityMayEnter s ei newPos ->
    let (sw, fw) := defaultWalk s ei oldPos newPos in
    stateValidAndCompatibleWithValidFlips s sw fw.
Proof.
  intros s ei oldPos newPos Hv Hei Hob Hnb Heme.
  unfold defaultWalk. split.
  - apply (stateValidAndCompatible_transitive _ (moveEntity s ei newPos) _).
    + apply moveEntity_valid; assumption.
    + intros [Hv1 Hc1]. apply degradeTrap_valid.
      * assumption.
      * apply (posInBounds_compatible _ _ _ Hc1). assumption.
  - apply checkPanels_flipsValid; assumption.
Qed.

(* Require a farPos with no entity on it (because all callers want that).
   Save a little more boilerplate by doing the higher-order thing than by just
   returning an option value. *)
Definition requireFarPos (s: State) (d: Direction) (newPos: Pos)
           (f: forall (farPos: Pos), option (State * Flips)) :=
  match travel s newPos d with
  | None => None
  | Some farPos =>
    if anyEntityAt s farPos then None
    else f farPos
  end.

(* Unfortunately, we can no longer pass EntityInfo for the acting entity because
   it doesn't exist.  We'll have to look up the status as needed.  This
   inconvenience is offset by convenience elsewhere. *)
Definition AttemptActionSub :=
  forall (s: State) (ei: EntityID) (oldPos: Pos) (d: Direction) (newPos: Pos) (c1: CellInfo),
    option (State * Flips).

(* attemptAction* is like MoveEnt* in the original implementation.  Notable difference: for
   all but Floorlike, it has been already checked that no entity is on the newPos. *)

(* Precondition: newPos <> current position of bi, Some bi1 = botat s newPos. *)
Definition handleDalekCrush s bi newPos bi1 :=
  (* Dalek becomes a broken bot, other bot becomes NoBot (consistent with
     original implementation, so we don't violate the bomb order). *)
  let s1 := destroyBot s bi1 in
  let (s2, f2) := defaultWalk s1 (Bot bi) (getBot s bi).(b_pos) newPos in
  (setBotStatus s2 bi BrokenBot, f2).

Lemma handleDalekCrush_valid:
  forall (s: State) (bi bi1: nat) (newPos: Pos),
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    (posInBounds s newPos).(bp_prop) ->
    (* Without this hypothesis, we could mess up the bomb order. *)
    (getBot s bi).(b_status) = Dalek ->
    botat s newPos = Some bi1 ->
    (* Although the conclusion holds even without this hypothesis, it requires a
       different argument and is not the intended use case. *)
    ~(Pos_eqbp (getBot s bi).(b_pos) newPos).(bp_prop) ->
    let (s2, f) := handleDalekCrush s bi newPos bi1 in
    stateValidAndCompatibleWithValidFlips s s2 f.
Proof.
  intros s bi bi1 newPos Hv Hbi Hpb Heqbis Hbi1 Honp.
  destruct (handleDalekCrush s bi newPos bi1) as [s2 f] eqn:Heq.
  (* TODO: Take from attemptActionFloorlike_valid *)
  unfold handleDalekCrush in Heq.
  destruct (defaultWalk (destroyBot s bi1) (Bot bi) (getBot s bi).(b_pos) newPos) as [sw fw] eqn:Heq1.
  inversion Heq. subst s2 f.
  pose (Hban := Hbi1). clearbody Hban. (* Compatibility with copied proof script *)
  apply botat_valid in Hbi1; [| apply Hv | apply Hpb].
  destruct (Hbi1) as [Hbi1l [Hbi1s Hbi1p]].
  pose (Hdbvc := destroyBot_valid s bi1 Hv Hbi1l). destruct (Hdbvc) as [Hdbv Hdbc].
  pose (Hopb := getEntityPos_inBounds s (Bot bi) Hv Hbi). unfold getEntityPos in Hopb.
  set (oldPos := (getBot s bi).(b_pos)).
  unshelve epose (Hdwvcf := defaultWalk_valid (destroyBot s bi1) (Bot bi) oldPos newPos Hdbv _ Hopb Hpb _).
  - simpl. rewrite length_update_nth. assumption.
  - simpl. unfold anyBotAt.
    destruct (botat (destroyBot s bi1) newPos) as [bi2 |] eqn:Hbadn; [| reflexivity].
    apply botat_valid in Hbadn; cycle 1.
    + apply Hdbv.
      + eapply posInBounds_compatible. * apply Hdbc. * apply Hpb.
      + destruct (Nat.eq_dec bi2 bi1) as [He | Hne].
        * (* The bot can't be present because we set its status to NoBot.
             XXX: Move this reasoning into setBotStatus_noMoreBots? *)
          subst bi2. destruct Hbadn as [Hbadnl [Hbadns Hbadnp]].
          unfold destroyBot in Hbadns. unfold setBot in Hbadns. unfold getBot in Hbadns.
          simpl in Hbadns. rewrite nth_update_nth in Hbadns; [| apply Hbi1l].
          simpl in Hbadns. discriminate Hbadns.
        * apply setBotStatus_noMoreBots in Hbadn; cycle 1.
          --- apply Hv. --- apply Hbi1l. --- right. reflexivity.
          --- assert (botIsPresent (destroyBot s bi1) newPos bi2) as Hbp2.
              +++ destruct Hbadn as [Hbadnl [Hbadns Hbadnp]].
                  unfold destroyBot. unfold setBot.
                  split; [| split]; unfold getBot; simpl.
                  *** rewrite length_update_nth. apply Hbadnl.
                  *** rewrite mth_update_nth; [| assumption]. apply Hbadns.
                  *** rewrite mth_update_nth; [| assumption]. apply Hbadnp.
              +++ apply botat_valid in Hbadn; cycle 1.
                  *** apply Hv. *** apply Hpb.
                  *** rewrite Hban in Hbadn. injection Hbadn as Hbadn. lia.
  - clearbody Hdwvcf.
    unfold oldPos in Hdwvcf. unfold defaultWalk in Hdwvcf.
    (* rewrite Heqei in Hdwvcf. unfold getEntityPos in Hdwvcf. *)
    destruct Hdwvcf as [Hdwvc Hdwf].
    unfold defaultWalk in Heq1.
    inversion Heq1 as [[Heq1s Heq1f]].  (* I can't get "injection as" to work, so do this. :/ *)
    (* Why is inversion unfolding?  Workaround. *)
    fold (moveEntity (destroyBot s bi1) (Bot bi) newPos) in Heq1s.
    split.
    + eapply stateValidAndCompatible_transitive.
      * apply Hdbvc.
      * intros _. eapply stateValidAndCompatible_transitive.
        -- apply Hdwvc.
        -- intros _. destruct Hdwvc as [Hdwv Hdwc]. destruct Hdwc as [Hdwcw [Hdwch Hdwcb]].
           assert ((getBot sw bi).(b_status) = Dalek) as Hbis'.
           ++ (* Crash through and see that nothing changed the status *)
             assert (bi <> bi1) as Hbibi1.
             ** (* This is because they have different positions. *)
               intro Hcontra. subst bi1.
               exact (Honp Hbi1p).
             ** rewrite <- Heq1s. unfold degradeTrap.
                destruct (getCell (moveEntity (destroyBot s bi1) (Bot bi) newPos) (getBot s bi).(b_pos)).(c_state).(l_tile); (* oink *)
                  (unfold moveEntity; unfold destroyBot; unfold setBot; unfold getBot; simpl;
                   rewrite nth_update_nth; [
                     rewrite mth_update_nth; [
                       simpl; apply Heqbis |
                       apply Hbibi1
                     ] |
                     rewrite length_update_nth; assumption
                  ]).
           ++ apply setBotStatus_valid.
                ++++ apply Hdwv.
                ++++ (* hack *) unfold moveEntity in Hdwcb. rewrite <- Hdwcb.
                     destruct Hdbc as [Hdbcw [Hdbch Hdbcb]]. rewrite <- Hdbcb. assumption.
                ++++ rewrite <- Heq1s in Hbis'. unfold moveEntity in Hbis'. rewrite Hbis'. reflexivity.
                ++++ rewrite <- Heq1s in Hbis'. unfold moveEntity in Hbis'. rewrite Hbis'. reflexivity.
    + assumption.
Qed.

(* Internal things pulled out to name them in the proof :/ *)
Definition resetIfBomb (s: State) (ei1: EntityID) :=
  match ei1 with
  | Player => s
  | Bot bi1 => match (getBot s bi1).(b_status) with
               | Bomb resetTime _ => setBotStatus s bi1 (Bomb resetTime (Some resetTime))
               | _ => s
               end
  end.

Lemma resetIfBomb_valid:
  forall (s: State) (ei1: EntityID),
    (stateValid s).(bp_prop) -> entityIDValid s ei1 ->
    stateValidAndCompatible s (resetIfBomb s ei1).
Proof.
  intros s ei1 Hv Hei1.
  unfold resetIfBomb.
  (* Familiar problem where "apply stateValidAndCompatible_reflexive" runs off in the weeds. *)
  assert (stateValidAndCompatible s s) as Hssvc;
    [apply stateValidAndCompatible_reflexive; assumption |].
  destruct ei1 as [| bi1] eqn:Heqei1; [assumption |].
  destruct (getBot s bi1).(b_status) eqn:Hbi1s; try assumption.
  apply setBotStatus_valid.
  - assumption.
  - apply Hei1.
  - rewrite Hbi1s. reflexivity.
  - rewrite Hbi1s. reflexivity.
Qed.

Definition tryFinishPushEntityBasedOnTargetTile (s sPushed: State) (ei1: EntityID) (farPos: Pos) :=
  let t2 := (getCell s farPos).(c_state).(l_tile) in
  match classifyTile t2 with
  | Floorlike => Some sPushed
  | _ => match t2, ei1 with
         | _, Player => None (* Cannot push the player onto Electric *)
         | Electric, Bot bi1 => (* Will push the bot onto Electric and destroy it *)
           Some (destroyBot sPushed bi1)
         | _, _ => None (* Cannot push onto this space *)
         end
  end.

Definition tryPushEntity (s: State) (ei: EntityID) (oldPos: Pos) (d: Direction) (newPos: Pos) (ei1: EntityID) :=
  requireFarPos
    s d newPos (
      fun farPos =>
        (* Just move the pushed entity to farPos, resetting a bomb timer if applicable. *)
        let (sPushed, fPushed) := defaultWalk (resetIfBomb s ei1) ei1 newPos farPos in
        (* Now see whether that's legal and what happens based on the target tile. *)
        match tryFinishPushEntityBasedOnTargetTile s sPushed ei1 farPos with
        | None => None
        | Some s1 =>
          (* Now let the original entity walk. *)
          let (sw1, fw1) := defaultWalk s1 ei oldPos newPos in
          Some (sw1, fPushed ++ fw1)
        end).

Definition attemptActionFloorlike: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    let (sw, fw) := defaultWalk s ei oldPos newPos in
    match entityAt s newPos with
    | EA_PlayerAndBot _ =>
      (* Special case in original implementation: player on bot, cannot push or crush *)
      None
    | EA_None =>
      (* Simple case: walk onto the new cell *)
      Some (sw, fw)
    | EA_One ei1 =>
      match ei with
      | Player => tryPushEntity s ei oldPos d newPos ei1
      | Bot bi => match (getBot s bi).(b_status) with
                  | Dalek => match ei1 with
                             (* For consistency with the original implementation, just move onto the player
                                without destroying the player.  The state will be severely lost. *)
                             | Player => Some (sw, fw)
                             | Bot bi1 => Some (handleDalekCrush s bi newPos bi1)
                             end
                  | Hugbot => tryPushEntity s ei oldPos d newPos ei1
                  | _ => ShouldNotReach None
                  end
      end
    end.

(* Really simple. *)
Definition attemptActionExit: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    Some (defaultWalk s ei oldPos newPos).

Definition attemptActionBroken: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    Some (setTile s newPos Floor, []).

Definition mapTiles (s: State) (tfun : TileType -> TileType) :=
  {|
      s_width := s.(s_width);
      s_height := s.(s_height);
      s_cells := map (fun row =>
                        map (fun c => {|
                                 c_state := {|
                                             l_tile := tfun c.(c_state).(l_tile);
                                             l_panel := c.(c_state).(l_panel)
                                           |};
                                 c_ostate := c.(c_ostate);
                                 c_dest := c.(c_dest)
                               |}) row) s.(s_cells);
      s_playerPos := s.(s_playerPos);
      s_playerBombed := s.(s_playerBombed);
      s_bots := s.(s_bots)
    |}.

Lemma mapTiles_valid:
  forall (s: State) (tfun: TileType -> TileType),
    (stateValid s).(bp_prop) ->
    (forall (t : TileType), tileTypeAllowsPanel t = true -> tileTypeAllowsPanel (tfun t) = true) ->
    stateValidAndCompatible s (mapTiles s tfun).
Proof.
  intros s tfun Hv Htfun.
  split.
  - destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
    unfold mapTiles. simpl.
    split_stateValid; try assumption.
    + (* hh *) rewrite length_map. assumption.
    + (* hw *) eapply Forall_map.
      * apply hw.
      * intro a. rewrite length_map. tauto.
    + (* hc *) eapply Forall_map.
      * apply hc.
      * cbv beta. intros a Ha. eapply Forall_map.
        -- apply Ha.
        -- simpl. intros a0 Ha0. destruct Ha0 as [[Ha0s Ha0os] Ha0d].
           split; [split |].
           ++ unfold clsValid in Ha0s |- *. destruct a0.(c_state).(l_panel); simpl.
              ** apply Htfun. assumption.
              ** reflexivity.
           ++ assumption.
           ++ assumption.
  - repeat split; reflexivity.
Qed.

Definition hasframers (s: State) :=
  existsb (fun row =>
             existsb (fun c =>
                        match c.(c_state).(l_tile) with
                        | HeartFramer => true
                        | _ => false
                        end) row)
          s.(s_cells).

Definition doHeartframerWakeupTiles (s: State) :=
  mapTiles s (fun t => match t with
                       | SleepingDoor => Exit
                       | _ => t
                       end).

Definition checkHeartframerWakeup (s: State) :=
  if hasframers s
  then s  (* Nothing to do yet *)
  else let s1 := doHeartframerWakeupTiles s in
       {|
         s_width := s1.(s_width);
         s_height := s1.(s_height);
         s_cells := s1.(s_cells);
         s_playerPos := s1.(s_playerPos);
         s_playerBombed := s1.(s_playerBombed);
         s_bots := map (fun b => {| b_pos := b.(b_pos);
                                    b_status := match b.(b_status) with
                                                | SleepingHugbot => Hugbot
                                                | SleepingDalek => Dalek
                                                | _ => b.(b_status)
                                                end
                                 |}) s1.(s_bots)
    |}.

Definition attemptActionHeartframer: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    match ei with
    | Player =>
      let (sw, fw) := defaultWalk s ei oldPos newPos in
      let s1 := setTile sw newPos Floor in
      Some (checkHeartframerWakeup s1, fw)
    | _ => None
    end.

Definition attemptActionElectric: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    match ei with
    | Player => None
    | Bot bi =>
      match (getBot s bi).(b_status) with
      | Dalek =>
        let (sw, fw) := defaultWalk s ei oldPos newPos in
        (* Dalek kills itself in electricity *)
        Some (destroyBot sw bi, fw)
      | _ => None
      end
    end.

Definition sliderCanSlide (d: Direction) (t: TileType) :=
  match d, t with
  | Up, SliderVert | Down, SliderVert => true
  | Left, SliderHoriz | Right, SliderHoriz => true
  | _, _ => ShouldNotReach false
  end.

Definition attemptActionSlider: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    if sliderCanSlide d c1.(c_state).(l_tile)
    then requireFarPos
           s d newPos (
             fun farPos =>
               let c2 := getCell s farPos in
               match c2.(c_state).(l_tile), c2.(c_state).(l_panel) with
               | Floor, None =>
                 let s1 := setTile (setTile s newPos Floor) farPos c1.(c_state).(l_tile) in
                 Some (defaultWalk s1 ei oldPos newPos)
               | _, _ => None
               end
           )
    else None.

Definition isRegularPushable (t: TileType) :=
  match t with
  | GrayBlock | RedBlock
  | Wire _ | Transponder => true
  | _ => false
  end.

(* Note: This does not check panels. *)
Definition tryDepositPushable (s: State) (t: TileType) (farPos: Pos) :=
  let c2 := getCell s farPos in
  match c2.(c_state).(l_tile) with
  | Floor =>
    Some (setTile s farPos t)
  | Electric => Some s
  | Hole =>
    match t with
    | GrayBlock => Some (setTile s farPos Floor)
    | _ => None
    end
  | _ => None  (* Cannot push onto obstacle *)
  end.

(* Unlike in original implementation, this does not include sliders. *)
Definition attemptActionPushable: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    requireFarPos
      s d newPos (
        fun farPos =>
          match tryDepositPushable s c1.(c_state).(l_tile) farPos with
          | None => None
          | Some s1 =>
            let s2 := setTile s1 newPos Floor in
            let f2 := checkPanels s newPos farPos false None in   (* non-entity colorless *)
            let (sw, fw) := defaultWalk s2 ei oldPos newPos in
            Some (sw, f2 ++ fw)
          end
      ).

Definition attemptActionOn: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    Some (mapTiles (setTile s newPos ButtonOff) (fun t => match t with
                                                          | Electric => Floor
                                                          | _ => t
                                                          end), []).

Definition switchTile01 (t: TileType) :=
  match t with
  | Button0 => Button1
  | Button1 => Button0
  | t => ShouldNotReach t
  end.

Definition attemptAction01: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    let newSwitchTile := switchTile01 c1.(c_state).(l_tile) in
    Some (mapTiles (setTile s newPos newSwitchTile) (fun t => match t with
                                                              | SliderHoriz => SliderVert
                                                              | SliderVert => SliderHoriz
                                                              | _ => t
                                                              end), []).

Definition attemptActionTeleporter: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    let dest := c1.(c_dest) in
    (* Use defaultWalk with a newPos other than the original newPos. :) *)
    let (sw, fw) := defaultWalk s ei oldPos dest in
    match ei with
    | Player => Some (sw, fw)
    | Bot bi =>
      match (getBot s bi).(b_status) with
      | Dalek =>
        (* Careful, player might also be at the dest.  Still crush any bot.  (The
           state is severely lost anyway, but let's get this right.) *)
        match botat s dest with
        | None => Some (sw, fw)
        | Some bi1 =>
          if bi1 =? bi
          then (* Dalek teleported back to its original cell and shouldn't crush itself *)
            (* defaultWalk requires entityMayEnter and isn't smart for the case of an
               entity returning to the same cell.  Using degradeTrap directly here is easiest. *)
            Some (degradeTrap s oldPos, [])
          else Some (handleDalekCrush s bi dest bi1)
        end
      | _ => ShouldNotReach None
      end
    end.

Definition attemptActionGreen: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    requireFarPos
      s d newPos (
        fun farPos =>
          let c2 := getCell s farPos in
          match c2.(c_state).(l_tile), c2.(c_state).(l_panel) with
          | Floor, None =>
            let s1 := setTile (setTile s newPos Floor) farPos BlueBlock in
            Some (defaultWalk s1 ei oldPos newPos)
          | _, _ => None
          end
      ).

Definition isSphere (t: TileType) :=
  match t with
  | Sphere _ => true
  | _ => false
  end.

(* All these helpers so we can name things in the proof... *)

Definition findLaunchTarget (s: State) (p: Pos) (d: Direction) :=
  findLastConsecutiveInDirection
    s p d (fun p2 => match anyEntityAt s p2, (getCell s p2).(c_state).(l_tile) with
                     | false, Floor => true
                     | _, _ => false
                     end
          ).

Definition colorOfLaunchable (t: TileType) :=
  match t with
  | GoldBlock => None
  | Sphere cl => cl
  | _ => ShouldNotReach None
  end.

Definition launchPart1 (s: State) (p: Pos) (d: Direction) (color: option Color) :=
  (setTile s p Floor, checkPanel s p false color).

Definition launchShouldZap (s: State) (p': Pos) (d: Direction) :=
  match travel s p' d with
  | None => false
  | Some p'' => match anyEntityAt s p'', (getCell s p'').(c_state).(l_tile) with
                | false, Electric => true
                | _, _ => false
                end
  end.

Definition launch (s: State) (p: Pos) (d: Direction) :=
  let t := (getCell s p).(c_state).(l_tile) in
  let color := colorOfLaunchable t in
  let p' := findLaunchTarget s p d in
  (* Leave the starting point *)
  let (s1, f1) := launchPart1 s p d color in
  if launchShouldZap s p' d
  then Some (s1, f1)
  else if Pos_beq p p' then None  (* Never mind, not a valid action *)
       else Some (setTile s1 p' t, f1 ++ checkPanel s p' false color).

Lemma launch_valid:
  forall (s s2: State) (f: Flips) (p: Pos) (d: Direction),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    let t := (getCell s p).(c_state).(l_tile) in
    (t = GoldBlock \/ isSphere t = true) ->
    launch s p d = Some (s2, f) ->
    stateValidAndCompatibleWithValidFlips s s2 f.
Proof.
  intros s s2 f p d Hv Hpb t Ht Heq.
  (* "cbv delta" doesn't inline enough local definitions, "unfold" inlines too many...
     There's probably a better way to do this. *)
  unfold launch in Heq.
  set (p' := findLaunchTarget s p d) in Heq.
  destruct (launchPart1 s p d (colorOfLaunchable (getCell s p).(c_state).(l_tile))) as [s1 f1] eqn:Hlp1.
  assert (stateValidAndCompatibleWithValidFlips s s1 f1) as Hvcf1.
  - unfold launchPart1 in Hlp1. inversion Hlp1 as [[Hs1 Hf1]]. subst s1 f1.
    split.
    + apply setTile_valid.
      * apply Hv.
      * apply Hpb.
      * intro Hclsv. unfold clsValid. simpl.
        destruct (getCell s p).(c_state).(l_panel); reflexivity.
    + apply checkPanel_flipsValid. * apply Hv. * apply Hpb.
  - destruct (launchShouldZap s p' d).
    + inversion Heq. subst s2 f. apply Hvcf1.
    + destruct (Pos_beq p p'); [discriminate Heq |].
      inversion Heq. subst s2 f.
      assert (posInBounds s p').(bp_prop) as Hp'b.
      * unfold p'. unfold findLaunchTarget.
        apply findLastConsecutiveInDirection_inBounds. apply Hpb.
      * destruct Hvcf1 as [Hvc1 Hf1].
        split.
        -- apply (stateValidAndCompatible_transitive _ s1 _).
           ++ apply Hvc1.
           ++ intros _. destruct Hvc1 as [Hv1 Hc1].
              apply setTile_valid.
              ** apply Hv1.
              ** eapply posInBounds_compatible. --- apply Hc1. --- apply Hp'b.
              ** intro Hclsv. fold t. unfold clsValid. simpl.
                 destruct (getCell s1 p').(c_state).(l_panel); [| reflexivity].
                 destruct Ht as [Ht | Ht].
                 --- rewrite Ht. reflexivity.
                 --- destruct t; try discriminate Ht. reflexivity.
        -- apply Forall_app. split.
           ++ apply Hf1.
           ++ apply checkPanel_flipsValid. ** apply Hv. ** apply Hp'b.
Qed.

(* Goldlike from original implementation is more conveniently split into
   GoldBlock and Sphere here. *)
Definition attemptActionGoldBlock: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    launch s newPos d.

Definition attemptActionSphere: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    (* Just look for the last sphere with no entity on it.  If the next cell
       has a sphere with an entity, we will detect that we can't launch lastSphere
       anywhere. *)
    let lastSphere := findLastConsecutiveInDirection
                        s newPos d (fun p2 => negb (anyEntityAt s p2) &&
                                                   isSphere (getCell s p2).(c_state).(l_tile)) in
    launch s lastSphere d.

Definition isSteel (t: TileType) :=
  match t with
  | Steel _ => true
  | _ => false
  end.

(* Precondition: (findCandidatePos s startPos d i) points to a steel without an entity. *)
(* Only one component of startPos matters.  This is the usual way findCandidatePos is used. *)
Fixpoint tryPushSteel (s: State) (startPos: Pos) (d: Direction) (i: nat) :=
  let curPos := findCandidatePos s startPos d i in
  match i, (getCell s curPos).(c_state).(l_tile) with
  | O, _ => None  (* Hit edge of level, cannot push *)
  | S i', Steel color =>
    let nextPos := findCandidatePos s startPos d i' in
    if anyEntityAt s nextPos
    then None (* Entity, cannot push *)
    else
      let removeFromCur (s1: State) (f1: Flips) :=
          (setTile s1 curPos Floor, checkPanel s1 curPos false color ++ f1) in
      let pushThis (s1: State) (f1: Flips) :=
          (* We are cheating a bit by using s, so we know the tile is a steel.
             If we used s1, the proof of state validity would need to show that
             the tile is still a steel. *)
          removeFromCur (setTile s1 nextPos (getCell s curPos).(c_state).(l_tile))
                        (checkPanel s1 nextPos false color ++ f1)
      in
      match (getCell s nextPos).(c_state).(l_tile) with
      | Floor => Some (pushThis s [])
      | Electric => Some (removeFromCur s [])
      | Steel _ =>
        match tryPushSteel s startPos d i' with
        | None => None (* Could not push the rest of the chain *)
        | Some (s1, f1) => Some (pushThis s1 f1)
        end
      | _ => None (* Cannot push onto this tile *)
      end
  | _, _ => ShouldNotReach None  (* Not a steel: violates precondition *)
  end.

Definition attemptActionSteel: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    match tryPushSteel s newPos d (findStartI s newPos d) with
    | None => None
    | Some (s1, f1) =>
      let (sw, fw) := defaultWalk s1 ei oldPos newPos in
      Some (sw, f1 ++ fw)
    end.

Definition followWire (w: WireType) (d: Direction) :=
  match w, d with
  | W_NS, Up | W_NS, Down => Some d
  | W_WE, Left | W_WE, Right => Some d
  | W_NW, Down => Some Left
  | W_NW, Right => Some Up
  | W_NE, Down => Some Right
  | W_NE, Left => Some Up
  | W_SE, Up => Some Right
  | W_SE, Left => Some Down
  | W_SW, Up => Some Left
  | W_SW, Right => Some Down
  | W_NSWE, _ => Some d
  | _, _ => None
  end.

Fixpoint followCircuit (limit: nat) (s: State) (leavePos: Pos) (d: Direction):
  (* lights, may have duplicates *) list Color * Flips :=
  let brokenCircuit := ([], []) in
  match limit with
  | O => ShouldNotReach brokenCircuit
  | S limit' =>
    match travel s leavePos d with
    | None => brokenCircuit
    | Some newPos =>
      let c := getCell s newPos in
      match c.(c_state).(l_tile) with
      | Wire w =>
        match followWire w d with
        | None => brokenCircuit
        | Some d2 => followCircuit limit' s newPos d2
        end
      | Transponder =>
        match findInDirection s newPos d (beamStopped s) with
        | None => brokenCircuit
        | Some destPos =>
          match (getCell s destPos).(c_state).(l_tile) with
          | Transponder => followCircuit limit' s destPos d
          | _ => brokenCircuit
          end
        end
      | Light color => ([color], [])
      | Remote =>
        let (lightsRest, flipsRest) := followCircuit limit' s newPos d in
        (lightsRest, c.(c_dest) :: flipsRest)
      | _ => brokenCircuit
      end
    end
  end.

Lemma followCircuit_flipsValid:
  forall (limit: nat) (s: State) (leavePos: Pos) (d: Direction),
    (stateValid s).(bp_prop) -> (posInBounds s leavePos).(bp_prop) ->
    flipsValid s (snd (followCircuit limit s leavePos d)).
Proof.
  intros limit s leavePos d Hv Hpb.
  destruct (followCircuit limit s leavePos d) as [l f] eqn:Hfc1.
  revert leavePos Hpb d l f Hfc1.
  induction limit as [| limit IHlimit]; intros leavePos Hpb d l f Hfc1.
  * inversion Hfc1. apply Forall_nil.
  * unfold followCircuit in Hfc1.
    destruct (travel s leavePos d) as [p |] eqn:Ht;
      try (inversion Hfc1; apply Forall_nil).
    pose (Hpb' := travel_inBounds s leavePos p d Hpb Ht).
    destruct (getCell s p).(c_state).(l_tile)
      (* Oink, but I haven't learned a better approach using matching. *)
      as [| | | | | | | | | | | | | | | | | | | | | | | | | | | | | wt | | | |];
      try (inversion Hfc1; apply Forall_nil).  (* circuit ends without a flip *)
    -- (* wire *) destruct (followWire wt d) as [d0 |]; try (inversion Hfc1; apply Forall_nil).
       apply (IHlimit p Hpb' d0 l f); assumption.
    -- (* transponder *) destruct (findInDirection s p d (beamStopped s)) as [p0 |] eqn:Heqp0;
         try (inversion Hfc1; apply Forall_nil).
       destruct (getCell s p0).(c_state).(l_tile);
         try (inversion Hfc1; apply Forall_nil).
       unshelve eapply (IHlimit p0 _ d l f); try assumption.
       apply (findInDirection_inBounds _ _ _ _ _ Hpb' Heqp0).
    -- (* remote *)
      fold followCircuit in Hfc1.
      destruct (followCircuit limit s p d) as [l0 f0] eqn:Hfc2.
      inversion Hfc1 as [[Hfc1l Hfc1f]].
      apply Forall_cons.
      ++ destruct (getCell_valid s p Hv Hpb'). assumption.
      ++ apply (IHlimit p Hpb' d l0). assumption.
Qed.

Definition followCircuits (s: State) (buttonPos: Pos) :=
  let limit := s.(s_width) * s.(s_height) in
  fold_left (fun (those: list Color * Flips) d2 =>
               let (thoseLights, thoseFlips) := those in
               let (theseLights, theseFlips) := followCircuit limit s buttonPos d2 in
               (theseLights ++ thoseLights, theseFlips ++ thoseFlips)
            ) allDirections ([], []).

Lemma followCircuits_flipsValid:
  forall (s: State) (buttonPos: Pos),
    (stateValid s).(bp_prop) -> (posInBounds s buttonPos).(bp_prop) ->
    flipsValid s (snd (followCircuits s buttonPos)).
Proof.
  intros s buttonPos Hv Hpb.
  destruct (followCircuits s buttonPos) eqn:Heqfc.
  unfold followCircuits in Heqfc.
  remember ([], []) as init eqn:Heqinit in Heqfc.
  destruct init as [initLights initFlips]. inversion Heqinit as [[Heqinitl Heqinitf]].
  assert (flipsValid s initFlips) as Hifv.
  - rewrite Heqinitf. apply Forall_nil.
  - simpl. clear Heqinit Heqinitl Heqinitf. revert initLights initFlips Hifv Heqfc.
    remember allDirections as directionList eqn:HeqdirectionList. clear HeqdirectionList.
    induction directionList as [| d directionList IHdirectionList];
      intros initLights initFlips Hifv Heqfc; simpl in Heqfc.
    + inversion Heqfc as [[Heqfcl Heqfcf]]. rewrite <- Heqfcf. assumption.
    + destruct (followCircuit (s.(s_width) * s.(s_height)) s buttonPos d) as [theseLights theseFlips] eqn:Hfc1.
      unshelve eapply (IHdirectionList _ _ _ Heqfc).
      apply Forall_app. split.
      * rewrite (surjective_pairing (followCircuit (s.(s_width) * s.(s_height)) s buttonPos d)) in Hfc1.
        inversion Hfc1.
        apply followCircuit_flipsValid.
        -- assumption.
        -- assumption.
      * assumption.
Qed.

Definition applyLights (s: State) (lights: list Color) :=
  fold_left (fun s1 color =>
               mapTiles s1 (fun t => match t with
                                     | Togglable up col => if Color_beq col color
                                                           then Togglable (negb up) col
                                                           else t
                                     | _ => t
                                     end)) lights s.

Lemma applyLights_valid:
  forall (s: State) (lights: list Color),
    (stateValid s).(bp_prop) -> stateValidAndCompatible s (applyLights s lights).
Proof.
  intros s lights Hv.
  revert s Hv; induction lights as [| ? lights IHlights]; intros s Hv.
  - apply stateValidAndCompatible_reflexive. assumption.
  - apply IHlights.
    unshelve epose (Hmtvc := mapTiles_valid _ _ _ _); cycle -1.
    + destruct Hmtvc as [Hmtv Hmtc]. apply Hmtv.
    + assumption.
    + intros t Httap.
      destruct t; try assumption. simpl in Httap. discriminate Httap.
Qed.

Definition attemptActionButton: AttemptActionSub :=
  fun s ei oldPos d newPos c1 =>
    let (lights, flips) := followCircuits s newPos in
    Some (applyLights s lights, flips).

Definition attemptActionPreFlips (s: State) (ei: EntityID) (d: Direction) : option (State * Flips) :=
  let oldPos := getEntityPos s ei in
  match travel s oldPos d with
  | None => None
  | Some newPos =>
    let c1 := getCell s newPos in
    let t1 := c1.(c_state).(l_tile) in
    let callSub sub := sub s ei oldPos d newPos c1 in
    match classifyTile t1 with
    | Floorlike => callSub attemptActionFloorlike
    | Unactable => None
    | Actable =>
      if anyEntityAt s newPos then None
      else
        match t1 with
        | Exit => callSub attemptActionExit
        | HeartFramer => callSub attemptActionHeartframer
        | BrokenBlock => callSub attemptActionBroken
        | GrayBlock | RedBlock
        | Wire _ | Transponder => callSub attemptActionPushable
        | SliderHoriz | SliderVert => callSub attemptActionSlider
        | Electric => callSub attemptActionElectric
        | ButtonOn => callSub attemptActionOn
        | Button0 | Button1 => callSub attemptAction01
        | GreenBlock => callSub attemptActionGreen
        | Teleporter => callSub attemptActionTeleporter
        | GoldBlock => callSub attemptActionGoldBlock
        | Sphere _ => callSub attemptActionSphere
        | Steel _ => callSub attemptActionSteel
        | Button => callSub attemptActionButton
        | _ => ShouldNotReach None
        end
    end
  end.

(* Precondition: stateValid s, ei refers to a normal acting entity. *)
Definition attemptAction (s: State) (ei: EntityID) (d: Direction) : option State :=
  match attemptActionPreFlips s ei d with
  | None => None
  | Some (s2, flips) =>
    Some (applyFlips s2 flips)
  end.

(* Using a module seems to be the only way to stop oldPos, Hopb, etc. from
   entering the global namespace.  (For definitions, we could use Let, but I
   think allowing qualified references will lead to less hassle than inlining
   the definitions when generalizing exported items.) *)
Module ActionSubsModule.

Section ActionSubs.
  Variable s: State.
  Variable ei: EntityID.
  Local Definition oldPos := getEntityPos s ei.
  Variable d: Direction.
  Variable newPos: Pos.
  Hypothesis Ht: travel s oldPos d = Some newPos.
  Local Definition c1 := getCell s newPos.
  Local Definition t1 := c1.(c_state).(l_tile).
  Hypothesis Hv: (stateValid s).(bp_prop).
  Hypothesis Hei: entityIDValid s ei.
  Hypothesis Hnae: isNormalActingEntity s ei = true.
  Local Lemma Hopb: (posInBounds s oldPos).(bp_prop).
  Proof.
    apply getEntityPos_inBounds.
    - assumption.
    - assumption.
  Qed.
  Hypothesis Hpb: (posInBounds s newPos).(bp_prop).
  Variable s2: State.
  Variable f: Flips.

  Definition actionSubValid sub :=
    sub s ei oldPos d newPos c1 = Some (s2, f) -> stateValidAndCompatibleWithValidFlips s s2 f.

  Lemma attemptActionFloorlike_valid:
    classifyTile t1 = Floorlike -> actionSubValid attemptActionFloorlike.
  Proof.
    intros Htl Heq.
    cbv beta delta [attemptActionFloorlike] in Heq.
    destruct (defaultWalk s ei oldPos newPos) as [sw fw] eqn:Hdw.
    pose (Hei1 := entityAt_valid s newPos Hv Hpb). clearbody Hei1.
    destruct (entityAt s newPos) as [ | ei1 | ?] eqn:Hea.
    - (* No entity at newPos: just walk *)
      inversion Heq as [[Heqs Heqf]]; subst s2 f.
      unshelve epose (Hdwv := defaultWalk_valid s ei oldPos newPos Hv Hei Hopb Hpb _).
      + (* This part is fiddly, and maybe we could improve our definitions to make it less so. *)
        unfold entityMayEnter. unfold anyBotAt. unfold entityAt in Hea.
        destruct ei; try exact I. destruct (botat s newPos); try reflexivity.
        destruct (Pos_beq s.(s_playerPos) newPos); discriminate Hea.
      + rewrite Hdw in Hdwv. apply Hdwv.
    - (* One entity: the complicated case *)
      (* Assert this because it is used in two cases and is in fact always true. *)
      assert (tryPushEntity s ei oldPos d newPos ei1 = Some (s2, f) ->
              stateValidAndCompatibleWithValidFlips s s2 f) as Hpushvc.
      + intro Hpusheq. unfold tryPushEntity in Hpusheq.
        unfold requireFarPos in Hpusheq.
        destruct (travel s newPos d) as [farPos |] eqn:Htfar; [| discriminate Hpusheq].
        pose (Htfarv := travel_inBounds s newPos farPos d Hpb Htfar).
        destruct (anyEntityAt s farPos) eqn:Haeaf; [discriminate Hpusheq |].
        destruct (defaultWalk (resetIfBomb s ei1) ei1 newPos farPos) as [sPushed fPushed] eqn:Hdp.
        destruct (tryFinishPushEntityBasedOnTargetTile s sPushed ei1 farPos) as [s1 |] eqn:Htfpe; [| discriminate Hpusheq].
        destruct (defaultWalk s1 ei oldPos newPos) as [sw1 fw1] eqn:Hdw1.
        pose (Hresetvc := resetIfBomb_valid s ei1 Hv Hei1).
        destruct (Hresetvc) as [Hresetv Hresetc].
        assert (anyBotAt (resetIfBomb s ei1) farPos = false) as Hresetbaf.
        * unfold resetIfBomb in Hresetv, Hresetc |- *.
          (* Trivial assertion to set up assumption *)
          assert (anyBotAt s farPos = false) as H.
          -- apply Bool.orb_false_elim in Haeaf.
             destruct Haeaf as [Hpaf Habaf]. apply Habaf.
          -- destruct ei1 as [| bi1] eqn:Heqei1; [apply H |].
             destruct (getBot s bi1).(b_status) as [| | | | | | resetTime currentTime] eqn:Heqbi1s;
               try apply H.
             unfold anyBotAt.
             destruct (botat (setBotStatus s bi1 (Bomb resetTime (Some resetTime))) farPos) as [bif |] eqn:Heqbif; try reflexivity.
             apply botat_valid in Heqbif; cycle 1.
             ** apply Hresetv.
             ** eapply posInBounds_compatible.
                --- apply Hresetc.
                --- assumption.
             ** apply setBotStatus_noMoreBots in Heqbif.
                --- apply botat_valid in Heqbif; cycle 1.
                    +++ assumption.
                    +++ assumption.
                    +++ unfold anyBotAt in H. rewrite Heqbif in H. discriminate H.
                --- assumption.
                --- assumption.
                --- rewrite Heqbi1s. left. reflexivity.
        * (* Weird if the far tile doesn't allow pushing, but again, used in two cases
             and always true, so if our goal is to write a proof, we might as well take advantage. *)
          assert (stateValidAndCompatibleWithValidFlips s sPushed fPushed) as Hpush1vcf.
          -- unshelve epose (Hdpv := defaultWalk_valid (resetIfBomb s ei1) ei1 newPos farPos _ _ _ _ _).
             ++ apply Hresetv.
             ++ eapply entityIDValid_compatible.
                ** apply Hresetc. ** assumption.
             ++ eapply posInBounds_compatible.
                ** apply Hresetc. ** assumption.
             ++ eapply posInBounds_compatible.
                ** apply Hresetc. ** assumption.
             ++ destruct ei1 as [| bi1] eqn:Heqei1; try exact I.
                apply Hresetbaf.
             ++ rewrite Hdp in Hdpv. destruct Hdpv as [Hdpvv Hdpvf].
                split.
                ** eapply stateValidAndCompatible_transitive.
                   --- apply Hresetvc.
                   --- intros _. apply Hdpvv.
                ** eapply flipsValid_compatible.
                   --- apply statesCompatible_symmetric. apply Hresetc.
                   --- apply Hdpvf.
          -- destruct Hpush1vcf as [Hpush1vc Hpush1f].
             (* now deal with tryFinishPushEntityBasedOnTargetTile *)
             assert (stateValidAndCompatible s s1) as Hs1vc.
             ++ unfold tryFinishPushEntityBasedOnTargetTile in Htfpe.
                destruct (classifyTile (getCell s farPos).(c_state).(l_tile)) eqn:Hctf.
                ** injection Htfpe as Htfpe. rewrite <- Htfpe.
                   apply Hpush1vc.
                ** destruct (getCell s farPos).(c_state).(l_tile); destruct ei1 as [| bi1] eqn:Heqei1;
                     try discriminate Htfpe.  (* tiles other than Electric *)
                   injection Htfpe as Htfpe. rewrite <- Htfpe.
                   apply (stateValidAndCompatible_transitive _ sPushed _); [assumption |].
                   intros _.
                   destruct Hpush1vc as [Hpush1v Hpush1c].
                   apply destroyBot_valid.
                   --- apply Hpush1v.
                   --- pose (H := entityIDValid_compatible s sPushed (Bot bi1) Hpush1c Hei1). simpl in H.
                       apply H.
                ** destruct (getCell s farPos).(c_state).(l_tile); try discriminate Hctf;
                     destruct ei1 as [| bi1] eqn:Heqei1; discriminate Htfpe.  (* no Electric here *)
             ++ (* finally, the original entity walks from oldPos to newPos *)
               destruct (Hs1vc) as [Hs1v Hs1c].
               unshelve epose (Hdwvcf' := defaultWalk_valid s1 ei oldPos newPos _ _ _ _ _).
               ** assumption.
               ** eapply entityIDValid_compatible.
                  --- apply Hs1c. --- assumption.
               ** eapply posInBounds_compatible.
                  --- apply Hs1c. --- apply Hopb.
               ** eapply posInBounds_compatible.
                  --- apply Hs1c. --- assumption.
               ** (* another entityMayEnter *)
                 assert (entityMayEnter sPushed ei newPos) as Hemesp.
                 --- unfold defaultWalk in Hdp. inversion Hdp as [[Hdps Hdpf]].
                     destruct ei as [| bi] eqn:Heqei; try exact I.
                     unfold entityMayEnter. unfold anyBotAt.
                     destruct (botat (degradeTrap (moveEntity (resetIfBomb s ei1) ei1 farPos) newPos) newPos) as [bin |] eqn:Heqbin; [| reflexivity].
                     assert (botat (moveEntity (resetIfBomb s ei1) ei1 farPos) newPos = Some bin) as Hbam.
                     +++ unfold degradeTrap in Heqbin. unfold setTile in Heqbin. unfold setCell in Heqbin.
                         unfold botat in Heqbin |- *.
                         destruct (getCell (moveEntity (resetIfBomb s ei1) ei1 farPos) newPos).(c_state).(l_tile); apply Heqbin.
                     +++ destruct ei1 as [| bi1] eqn:Heqei1.
                         *** (* Use Hea to prove there was no bot at newPos.  Then, we didn't move any bot. *)
                           (* XXX: Can we deduplicate these 2 lines? *)
                           unfold entityAt in Hea.
                           destruct (Pos_beq s.(s_playerPos) newPos);
                             destruct (botat s newPos) as [bi1' |] eqn:Hban; try discriminate Hea.
                           unfold botat in Hban, Hbam.
                           unfold resetIfBomb in Hbam.
                           unfold moveEntity in Hbam. simpl in Hbam.
                           rewrite Hban in Hbam. discriminate Hbam.
                         *** unfold entityAt in Hea.
                             destruct (Pos_beq s.(s_playerPos) newPos);
                               destruct (botat s newPos) as [bi1' |] eqn:Hban; try discriminate Hea.
                             injection Hea as Hea. subst bi1'.
                             apply botat_valid in Hban; [| assumption | assumption].
                             destruct Hban as [Hbnl [Hbns Hbnp]].
                             pose (bi1rPos := (getBot (resetIfBomb s (Bot bi1)) bi1).(b_pos)).
                             assert (bi1rPos = newPos) as Hbi1rp.
                             ---- unfold bi1rPos. unfold resetIfBomb.
                                  assert ((getBot s bi1).(b_pos) = newPos) as Hbnp'.
                                  (* This is ridiculous.  Write a lemma? *)
                                  ++++ simpl in Hbnp.
                                       destruct newPos as [newX newY].
                                       destruct (getBot s bi1).(b_pos) as [bi1X bi1Y].
                                       simpl in Hbnp. destruct Hbnp as [Hbnx Hbny].
                                       rewrite Hbnx. rewrite Hbny. reflexivity.
                                  ++++ destruct (getBot s bi1).(b_status); try apply Hbnp'.
                                       unfold setBotStatus. unfold setBot. unfold getBot.
                                       simpl. rewrite nth_update_nth; [| assumption]. simpl. apply Hbnp'.
                             ---- unshelve epose (Hnbl := moveBot_noBotLeft (resetIfBomb s (Bot bi1)) bi1 farPos _ _ _ _ _ _).
                                  ++++ apply Hresetv.
                                  ++++ destruct Hresetc as [Hresetch [Hresetcw Hresetcb]]. rewrite <- Hresetcb. assumption.
                                  ++++ eapply posInBounds_compatible. **** apply Hresetc. **** assumption.
                                  ++++ apply Hresetbaf.
                                  ++++ (* Prove that bi1 originally wasn't NoBot so
                                          that it leaving newPos is meaningful *)
                                    unfold resetIfBomb.
                                    destruct (getBot s bi1).(b_status) eqn:Hbi1st;
                                      try discriminate Hbns;  (* NoBot *)
                                      try (rewrite Hbi1st; reflexivity).  (* Non-bombs *)
                                    unfold setBotStatus. unfold setBot. unfold getBot.
                                    simpl. rewrite nth_update_nth; [| assumption]. reflexivity.
                                  ++++ (* Prove that newPos <> farPos so the bot actually left newPos *)
                                    fold bi1rPos.
                                    (* Proof by contradiction, since we don't
                                       have all the lemmas we'd need to do this more directly. *)
                                    destruct (Pos_beq farPos bi1rPos) eqn:Hbi1rfp; [| reflexivity].
                                    pose (Hnfdif := travel_different s newPos farPos d Hpb Htfar).
                                    unfold Pos_beq in Hbi1rfp.
                                    apply (Pos_eqbp farPos bi1rPos).(bp_equiv) in Hbi1rfp.
                                    rewrite Hbi1rp in Hbi1rfp. contradiction.
                                  ++++ clearbody Hnbl.
                                       fold bi1rPos in Hnbl. rewrite Hbi1rp in Hnbl.
                                       unfold anyBotAt in Hnbl. rewrite Hbam in Hnbl. discriminate Hnbl.
                 --- unfold tryFinishPushEntityBasedOnTargetTile in Htfpe.
                     destruct (classifyTile (getCell s farPos).(c_state).(l_tile)) eqn:Hctf.
                     +++ injection Htfpe as Htfpe. rewrite Htfpe in Hemesp. assumption.
                     +++ destruct (getCell s farPos).(c_state).(l_tile) eqn:Htf; destruct ei1 as [| bi1] eqn:Heqei1; try discriminate Htfpe.
                         injection Htfpe as Htfpe.
                         unfold entityMayEnter in Hemesp |- *.
                         destruct ei as [| bi] eqn:Heqei; [exact I |].
                         unfold anyBotAt in Hemesp |- *.
                         destruct (botat s1 newPos) as [s1bi |] eqn:Hbas1n; [| reflexivity].
                         apply botat_valid in Hbas1n; cycle 1.
                         *** apply Hs1v.
                         *** eapply posInBounds_compatible.
                             ---- apply Hs1c.
                             ---- assumption.
                         *** rewrite <- Htfpe in Hbas1n.
                             destruct Hpush1vc as [Hpush1v Hpush1c].
                             destruct (Hpush1c) as [Hpush1ch [Hpush1cw Hpush1cb]].
                             apply setBotStatus_noMoreBots in Hbas1n; cycle 1.
                             ---- apply Hpush1v.
                             ---- rewrite <- Hpush1cb. assumption.
                             ---- right. reflexivity.
                             ---- apply botat_valid in Hbas1n; cycle 1.
                                  ++++ apply Hpush1v.
                                  ++++ eapply posInBounds_compatible.
                                       **** apply Hpush1c.
                                       **** assumption.
                                  ++++ rewrite Hbas1n in Hemesp. discriminate Hemesp.
                     +++ destruct (getCell s farPos).(c_state).(l_tile) eqn:Htf; destruct ei1; try discriminate Htfpe.
                         discriminate Hctf.  (* Electric is not Unactable *)
               ** (* put everything together *)
                 rewrite Hdw1 in Hdwvcf'.
                 inversion Hpusheq. subst f s2.
                 destruct Hdwvcf' as [Hdwvc' Hdwvf'].
                 split.
                 --- apply (stateValidAndCompatible_transitive _ s1 _).
                     +++ assumption. +++ intros _. assumption.
                 --- apply Forall_app. split.
                     +++ assumption.
                     +++ eapply flipsValid_compatible.
                         *** apply statesCompatible_symmetric. apply Hs1c.
                         *** apply Hdwvf'.
      + (* Workaround for destructing ei changing the type of Hei and breaking Hopb *)
        pose (Hopb1 := Hopb). clearbody Hopb1.
        (* Now look at the entity type *)
        destruct ei as [| bi] eqn:Heqei.
        * apply Hpushvc. apply Heq.
        * destruct (getBot s bi).(b_status) eqn:Heqbis; try discriminate Heq.
          -- (* Hugbot *)
            apply Hpushvc. apply Heq.
          -- (* Dalek *)
            clear Hpushvc.
            destruct ei1 as [| bi1] eqn:Heqei1.
            ++ (* Moving onto player: just a walk *)
              inversion Heq. subst s2 f.
              unshelve epose (Hdwvcf := defaultWalk_valid s (Bot bi) oldPos newPos Hv Hei Hopb1 Hpb _).
              ** unfold entityMayEnter. unfold anyBotAt.
                 unfold entityAt in Hea.
                 destruct (Pos_beq s.(s_playerPos) newPos);
                   destruct (botat s newPos) eqn:Hban; try discriminate Hea.
                 reflexivity.
              ** clearbody Hdwvcf. rewrite Hdw in Hdwvcf. apply Hdwvcf.
            ++ (* Moving onto a bot *)
              (* XXX: Can we deduplicate this reasoning across the two cases? *)
              unfold entityAt in Hea.
              destruct (Pos_beq s.(s_playerPos) newPos);
                destruct (botat s newPos) as [bi1' |] eqn:Hban; try discriminate Hea.
              inversion Hea; subst bi1'.
              unshelve epose (Hdcvcf := handleDalekCrush_valid s bi bi1 newPos Hv Hei Hpb Heqbis Hban _).
              ** unshelve epose (Htd := travel_different s oldPos newPos d Hopb1 Ht).
                 simpl in Htd. simpl. unfold oldPos in Htd. unfold getEntityPos in Htd.
                 rewrite Heqei in Htd.
                 lia.
              ** clearbody Hdcvcf. destruct (handleDalekCrush s bi newPos bi1) as [s2' f'].
                 inversion Heq. subst s2' f'.  (* Crazy, but I'm struggling with these pairs. *)
                 apply Hdcvcf.
    - (* Player and bot: no move *) discriminate Heq.
  Qed.

  Section ActableSubs.
    Hypothesis Haea: anyEntityAt s newPos = false.

    Lemma attemptActionExit_valid:
      t1 = Exit -> actionSubValid attemptActionExit.
    Proof.
      intros Htl Heq.
      unfold attemptActionExit in Heq. inversion Heq as [[Heqs Heqf]].
      set (s' := moveEntity s ei newPos).
      split.
      - apply (stateValidAndCompatible_transitive _ s' _).
        + apply moveEntity_valid; try assumption.
          (* restriction on existing entity *)
          destruct ei; [exact I |].
          unfold anyEntityAt in Haea. apply Bool.orb_false_elim in Haea.
          destruct Haea. assumption.
        + intros [Hv' Hc'].
          apply degradeTrap_valid.
          * assumption.
          * eapply posInBounds_compatible.
            -- eassumption.
            -- apply Hopb.
      - apply checkPanels_flipsValid; try assumption. apply Hopb.
    Qed.

    Lemma attemptActionHeartframer_valid:
      t1 = HeartFramer -> actionSubValid attemptActionHeartframer.
    Proof.
      intros Htl Heq.
      unfold attemptActionHeartframer in Heq.
      pose (Hopb1 := Hopb). clearbody Hopb1.
      destruct ei; [| discriminate Heq].
      destruct (defaultWalk s Player oldPos newPos) as [sw fw] eqn:Heqdw.
      inversion Heq. subst s2 f.
      unshelve epose (Hdwvcf := defaultWalk_valid s Player oldPos newPos Hv Hei Hopb1 Hpb _);
        [reflexivity |]. clearbody Hdwvcf. rewrite Heqdw in Hdwvcf.
      destruct Hdwvcf as [Hdwvc Hdwf].
      split.
      - set (sst := setTile sw newPos Floor).
        assert (stateValidAndCompatible s sst) as Hstvc.
        + apply (stateValidAndCompatible_transitive _ sw _).
          * apply Hdwvc.
          * intros _.
            destruct Hdwvc as [Hdwv Hdwc].
            apply setTile_valid.
            -- apply Hdwv.
            -- eapply posInBounds_compatible. ++ apply Hdwc. ++ apply Hpb.
            -- intro Hclsv. unfold clsValid. simpl.
               destruct (getCell sw newPos).(c_state).(l_panel); reflexivity.
        + unfold checkHeartframerWakeup.
          destruct (hasframers sst); [apply Hstvc |].
          eapply stateValidAndCompatible_transitive; [apply Hstvc |].
          intros _.
          set (swt := doHeartframerWakeupTiles sst).
          apply (stateValidAndCompatible_transitive _ swt _).
          * unfold doHeartframerWakeupTiles.
            apply mapTiles_valid.
            -- destruct Hstvc as [Hstv Hstc]. apply Hstv.
            -- intros t Httap. destruct t; discriminate Httap || reflexivity.
          * intro Hwtvc. split.
            -- (* This one we're going to have to prove by hand because of manual manipulation of the bots. *)
              destruct Hwtvc as [[[[[[hh hw] hc] hpb] hbb] hb] _].
              split_stateValid; [apply hh | apply hw | apply hc | apply hpb | |].
              ++ (* hbb *) simpl.
                 eapply Forall_map.
                 ** apply hbb.
                 ** simpl. intros a H. apply H. (* We didn't change the position *)
              ++ (* hb *)
                intros j Hj i Hi. simpl in Hj. rewrite length_map in Hj.
                pose (hb1 := hb j Hj i Hi). clearbody hb1. cbv beta in hb1.
                (* Try to stop this from becoming a more terrible mess *)
                unfold getBot in hb1 |- *. unfold s_bots; cbv iota.
                rewrite (nth_map _ _ _ shouldNotReachBot _); [| reflexivity].
                rewrite (nth_map _ _ _ shouldNotReachBot _); [| reflexivity].
                unfold swt. unfold doHeartframerWakeupTiles. unfold mapTiles. simpl.
                simpl in hb1. destruct hb1 as [[hb1 | hb1] | hb1].
                ** unfold isNoBot in hb1.
                   destruct (nth i sw.(s_bots) shouldNotReachBot).(b_status); try discriminate hb1.
                   left. left. reflexivity.
                ** unfold isNoBot in hb1.
                   destruct (nth j sw.(s_bots) shouldNotReachBot).(b_status); try discriminate hb1.
                   left. right. reflexivity.
                ** right. destruct hb1 as [hb1p hb1o].
                   split.
                   --- apply hb1p.
                   --- destruct (nth i sw.(s_bots) shouldNotReachBot).(b_status);
                         destruct (nth j sw.(s_bots) shouldNotReachBot).(b_status);
                         try discriminate hb1o; reflexivity.
            -- split; [| split]; simpl.
               ++ reflexivity.
               ++ reflexivity.
               ++ rewrite length_map. reflexivity.
      - apply Hdwf.
    Qed.

    Lemma attemptActionElectric_valid:
      t1 = Electric -> actionSubValid attemptActionElectric.
    Proof.
      intros Htl Heq.
      unfold attemptActionElectric in Heq.
      pose (Hopb1 := Hopb). clearbody Hopb1.
      destruct ei as [| bi] eqn:Heqei; [discriminate Heq |].
      destruct (getBot s bi).(b_status) eqn:Hbis; try discriminate Heq.
      destruct (defaultWalk s (Bot bi) oldPos newPos) as [sw fw] eqn:Heqdw.
      inversion Heq. subst s2 f.
      unshelve epose (Hdwvcf := defaultWalk_valid s (Bot bi) oldPos newPos Hv Hei Hopb1 Hpb _).
      - simpl. unfold anyEntityAt in Haea. apply Bool.orb_false_elim in Haea.
        destruct Haea as [Hpa Haba]. apply Haba.
      - clearbody Hdwvcf. rewrite Heqdw in Hdwvcf. destruct Hdwvcf as [Hdwvc Hdwf].
        split.
        + apply (stateValidAndCompatible_transitive _ sw _).
          * apply Hdwvc.
          * intros _. destruct Hdwvc as [Hdwv Hdwc].
            apply destroyBot_valid.
            -- apply Hdwv.
            -- destruct Hdwc as [Hdwcw [Hdwch Hdwcb]].
               rewrite <- Hdwcb. apply Hei.
        + apply Hdwf.
    Qed.

    Lemma attemptActionBroken_valid:
      t1 = BrokenBlock -> actionSubValid attemptActionBroken.
    Proof.
      intros Htl Heq.
      unfold attemptActionBroken in Heq. inversion Heq as [[Heqs Heqf]].
      split; [| apply Forall_nil].
      apply setTile_oldTypeDisallowsPanel_valid; try assumption.
      fold c1. fold t1.
      rewrite Htl. reflexivity.
    Qed.

    Lemma attemptActionButton_valid:
      t1 = Button -> actionSubValid attemptActionButton.
    Proof.
      intros Htl Heq.
      unfold attemptActionButton in Heq.
      destruct (followCircuits s newPos) as [l f0] eqn:Hfc.
      injection Heq as Heq. subst f0. subst s2.
      rewrite (surjective_pairing (followCircuits s newPos)) in Hfc.
      inversion Hfc. subst l f.
      split.
      - apply applyLights_valid. assumption.
      - apply followCircuits_flipsValid.
        + assumption.
        + assumption.
    Qed.

    Lemma attemptActionOn_valid:
      t1 = ButtonOn -> actionSubValid attemptActionOn.
    Proof.
      intros Htl Heq.
      unfold attemptActionOn in Heq.
      injection Heq as Heq. subst f s2.
      split.
      - apply (stateValidAndCompatible_transitive _ (setTile s newPos ButtonOff) _).
        + apply setTile_valid; [apply Hv | apply Hpb |].
          intro Hclsv.
          unfold clsValid in Hclsv |- *.
          destruct (getCell s newPos).(c_state).(l_panel); simpl.
          * unfold t1 in Htl. unfold c1 in Htl. rewrite Htl in Hclsv. discriminate Hclsv.
          * reflexivity.
        + intros [Hsv Hsc].
          apply mapTiles_valid; [apply Hsv |].
          intros t Httap. destruct t; try apply Httap. reflexivity.
      - apply Forall_nil.
    Qed.

    Lemma attemptAction01_valid:
      (t1 = Button0 \/ t1 = Button1) -> actionSubValid attemptAction01.
    Proof. (* Same idea as attemptActionOn_valid *)
      intros Htl Heq.
      unfold attemptAction01 in Heq.
      injection Heq as Heq. subst f s2.
      split.
      - apply (stateValidAndCompatible_transitive _ (setTile s newPos (switchTile01 c1.(c_state).(l_tile))) _).
        + apply setTile_valid; [apply Hv | apply Hpb |].
          intro Hclsv.
          unfold clsValid in Hclsv |- *.
          destruct (getCell s newPos).(c_state).(l_panel); simpl.
          * unfold t1 in Htl. unfold c1 in Htl.
            destruct Htl as [Htl | Htl]; rewrite Htl in Hclsv; discriminate Hclsv.
          * reflexivity.
        + intros [Hsv Hsc].
          apply mapTiles_valid; [apply Hsv |].
          intros t Httap. destruct t; try apply Httap.
      - apply Forall_nil.
    Qed.

    Lemma attemptActionTeleporter_valid:
      t1 = Teleporter -> actionSubValid attemptActionTeleporter.
    Proof.
      intros Htl Heq.
      unfold attemptActionTeleporter in Heq.
      assert (posInBounds s c1.(c_dest)).(bp_prop) as Hdb.
      - destruct Hv as [[[[[hh hw] hc] hbp] hbb] hb].
        destruct (getCell_valid s newPos Hv Hpb) as [[Hnvn Hnva] Hnvd]. apply Hnvd.
      - destruct (defaultWalk s ei oldPos c1.(c_dest)) as [sw fw] eqn:Heqdw.
        assert (entityMayEnter s ei c1.(c_dest) ->
                stateValidAndCompatibleWithValidFlips s sw fw) as Hdwvcf'.
        + intro Heme.
          (* XXX It's annoying that defaultWalk_valid always has to be posed and not applied
             because of the let binding.  Fix this. *)
          unshelve epose (Hdwvcf := defaultWalk_valid s ei oldPos c1.(c_dest) Hv Hei Hopb _ _).
          * destruct Hv as [[[[[hh hw] hc] hbp] hbb] hb].
            destruct (getCell_valid s newPos Hv Hpb) as [[Hnvn Hnva] Hnvd]. apply Hnvd.
          * assumption.
          * rewrite Heqdw in Hdwvcf. apply Hdwvcf.
        + pose (Hopb1 := Hopb). clearbody Hopb1.
          destruct ei as [| bi] eqn:Heqei.
          * inversion Heq. subst s2 f. apply Hdwvcf'. reflexivity.
          * destruct (getBot s bi).(b_status) eqn:Hbis; try discriminate Heq.
            destruct (botat s c1.(c_dest)) as [bid |] eqn:Hbad.
            -- destruct (bid =? bi) eqn:Heqbbid.
               ++ (* Teleporting back to the original square. *)
                 inversion Heq. subst s2 f. split.
                 ** apply degradeTrap_valid. --- apply Hv. --- apply Hopb1.
                 ** apply Forall_nil.
               ++ unshelve epose (Hdcvcf := handleDalekCrush_valid s bi bid c1.(c_dest) Hv Hei Hdb Hbis Hbad _).
                  ** intro Hcontra.
                     assert ((getBot s bi).(b_pos) = c1.(c_dest)) as Hcontra2.
                     --- simpl in Hcontra. destruct (getBot s bi).(b_pos) as [opx opy]. simpl in Hcontra.
                         destruct Hcontra; subst opx opy. destruct c1.(c_dest). simpl. reflexivity.
                     --- assert (botat s oldPos = Some bi) as Hbao.
                         +++ apply botat_valid; [apply Hv | apply Hopb1 |].
                             split; [| split].
                             *** apply Hei.
                             *** unfold isNoBot. rewrite Hbis. reflexivity.
                             *** unfold oldPos. unfold getEntityPos. rewrite Heqei.
                                 simpl. lia.
                         +++ unfold oldPos in Hbao. unfold getEntityPos in Hbao. rewrite Heqei in Hbao.
                             rewrite Hcontra2 in Hbao. rewrite Hbao in Hbad. injection Hbad as Hbad.
                             apply EqNat.beq_nat_false in Heqbbid. symmetry in Hbad. contradiction.
                  ** clearbody Hdcvcf. destruct (handleDalekCrush s bi c1.(c_dest) bid) as [s2' f'].
                     inversion Heq. subst s2' f'.  (* Crazy, but I'm struggling with these pairs. *)
                     apply Hdcvcf.
            -- inversion Heq. subst s2 f. apply Hdwvcf'.
               unfold entityMayEnter. unfold anyBotAt. rewrite Hbad. reflexivity.
    Qed.

    (* Put this in the section for convenient use of the section hypotheses. *)
    Lemma walkAfterPushBlock_valid:
      forall (s1: State) (f1: Flips),
        stateValidAndCompatible s s1 -> s.(s_bots) = s1.(s_bots) ->
        flipsValid s f1 ->  (* It seems to be standard to check based on s. :/ *)
        (* This is a little awkward, but quantifying over fw gave me grief. *)
        let (sw, fw) := defaultWalk s1 ei oldPos newPos in
        s2 = sw -> f = f1 ++ fw ->
        stateValidAndCompatibleWithValidFlips s s2 f.
    Proof.
      intros s1 f1 Hs1vc Hss1b Hf1 Heqs2 Heqf.
      destruct (Hs1vc) as [Hs1v Hs1c].
      unshelve epose (Hdwvcf := defaultWalk_valid s1 ei oldPos newPos _ _ _ _ _).
      - apply Hs1v.
      - unfold entityIDValid in Hei |- *. rewrite <- Hss1b. apply Hei.
      - eapply posInBounds_compatible; [apply Hs1c | apply Hopb].
      - eapply posInBounds_compatible; [apply Hs1c | apply Hpb].
      - unfold entityMayEnter. destruct ei as [| bi] eqn:Heqei; [exact I |].
        unfold anyEntityAt in Haea. apply Bool.orb_false_elim in Haea.
        destruct Haea as [Hpa Haba]. unfold anyBotAt, botat in Haba |- *.
        rewrite <- Hss1b. apply Haba.
      - clearbody Hdwvcf.
        unfold defaultWalk in Hdwvcf. subst s2 f. (* seems to be what's needed here *)
        destruct Hdwvcf as [Hdwvc Hdwf].
        split.
        + apply (stateValidAndCompatible_transitive _ s1 _).
          * apply Hs1vc.
          * intros _. apply Hdwvc.
        + apply Forall_app. split.
          * apply Hf1.
          * eapply flipsValid_compatible.
            -- apply statesCompatible_symmetric. apply Hs1c.
            -- apply Hdwf.
    Qed.

    Lemma attemptActionPushable_valid:
      isRegularPushable t1 = true -> actionSubValid attemptActionPushable.
    Proof.
      intros Htl Heq.
      unfold attemptActionPushable in Heq.
      unfold requireFarPos in Heq.
      destruct (travel s newPos d) as [farPos |] eqn:HeqfarPos; [| discriminate Heq].
      pose (Hfpb := travel_inBounds s newPos farPos d Hpb HeqfarPos). clearbody Hfpb.
      destruct (anyEntityAt s farPos); [discriminate Heq |].
      destruct (tryDepositPushable s c1.(c_state).(l_tile) farPos) as [s1 |] eqn:Heqtdp;
        [| discriminate Heq].
      (* Keep these two properties together as we go across the cases of tryDepositPushable. *)
      assert (stateValidAndCompatible s s1 /\
              s.(s_bots) = s1.(s_bots)) as Hs1.  (* Important for entityMayEnter in s1 *)
      - unfold tryDepositPushable in Heqtdp.
        destruct (getCell s farPos).(c_state).(l_tile) eqn:Heqtf; try discriminate Heqtdp.
        + injection Heqtdp as Heqtdp. subst s1.
          split.
          * apply setTile_valid. -- apply Hv. -- apply Hfpb.
            -- intro Hclsv. unfold clsValid. simpl.
               destruct (getCell s farPos).(c_state).(l_panel); [| reflexivity].
               fold t1. unfold tileTypeAllowsPanel. destruct t1; try discriminate Htl; reflexivity.
          * reflexivity.
        + destruct c1.(c_state).(l_tile) eqn:Heqtn; try discriminate Heqtdp.
          injection Heqtdp as Heqtdp. subst s1.
          split.
          * apply setTile_valid. -- apply Hv. -- apply Hfpb.
            -- intro Hclsv. unfold clsValid. simpl.
               destruct (getCell s farPos).(c_state).(l_panel); reflexivity.
          * reflexivity.
        + injection Heqtdp as Heqtdp. subst s1. split.
          * apply stateValidAndCompatible_reflexive. apply Hv. * reflexivity.
      - set (sc := setTile s1 newPos Floor) in Heq.
        assert (stateValidAndCompatible s sc /\ s.(s_bots) = sc.(s_bots)) as Hsc.
        + destruct Hs1 as [Hs1vc Hs1b].
          split.
          * apply (stateValidAndCompatible_transitive _ s1 _).
            -- apply Hs1vc.
            -- intros _. destruct Hs1vc as [Hs1v Hs1c]. apply setTile_valid.
               ++ apply Hs1v.
               ++ eapply posInBounds_compatible; [apply Hs1c | apply Hpb].
               ++ intro Hclsv. unfold clsValid. simpl.
                  destruct (getCell s1 newPos).(c_state).(l_panel); reflexivity.
          * rewrite Hs1b. reflexivity.
        + destruct Hsc as [Hscvc Hscb].
          destruct (defaultWalk sc ei oldPos newPos) as [sw fw] eqn:Heqdw.
          eapply walkAfterPushBlock_valid.
          * apply Hscvc.
          * apply Hscb.
          * apply checkPanels_flipsValid. -- apply Hv. -- apply Hpb. -- apply Hfpb.
          (* This is crazy but I don't have the skills right now to do better. *)
          * inversion Heq as [[Heqs Heqf]]. subst s2 f.
            unfold defaultWalk in Heqdw. inversion Heqdw as [[Heqsw Heqfw]].
            reflexivity.
          * inversion Heq as [[Heqs Heqf]]. subst s2 f.
            unfold defaultWalk in Heqdw. inversion Heqdw as [[Heqsw Heqfw]].
            reflexivity.
    Qed.

    Lemma attemptActionGreen_valid:
      t1 = GreenBlock -> actionSubValid attemptActionGreen.
    Proof.
      intros Htl Heq.
      unfold attemptActionGreen in Heq.
      unfold requireFarPos in Heq.
      destruct (travel s newPos d) as [farPos |] eqn:HeqfarPos; [| discriminate Heq].
      pose (Hfpb := travel_inBounds s newPos farPos d Hpb HeqfarPos). clearbody Hfpb.
      destruct (anyEntityAt s farPos); [discriminate Heq |].
      (* end boilerplate *)
      destruct (getCell s farPos).(c_state).(l_tile) eqn:Heqft; try discriminate Heq.
      destruct (getCell s farPos).(c_state).(l_panel) eqn:Heqfp; try discriminate Heq.
      apply (walkAfterPushBlock_valid (setTile (setTile s newPos Floor) farPos BlueBlock) []).
      - eapply stateValidAndCompatible_transitive.
        + apply (setTile_valid s newPos Floor).
          * apply Hv.
          * apply Hpb.
          * intro Hclsv. unfold clsValid. simpl. destruct (getCell s newPos).(c_state).(l_panel); reflexivity.
        + intros [Hv1 Hc1]. apply setTile_valid.
          * apply Hv1.
          * eapply posInBounds_compatible. -- apply Hc1. -- apply Hfpb.
          * intro Hclsv. unfold clsValid. simpl.
            (* Need to prove that setting newPos doesn't change farPos. *)
            unfold setTile.
            rewrite getCell_setCell_different.
            -- rewrite Heqfp. reflexivity.
            -- apply Hv.
            -- apply Hfpb.
            -- apply Hpb.
            -- apply (travel_different s newPos farPos d).
               ++ apply Hpb. ++ apply HeqfarPos.
      - reflexivity.
      - apply Forall_nil.
      - inversion Heq as [[Heqs2 Heqf]]. reflexivity.
      - inversion Heq as [[Heqs2 Heqf]]. reflexivity.
    Qed.

    Lemma attemptActionSlider_valid:
      (t1 = SliderHoriz \/ t1 = SliderVert) -> actionSubValid attemptActionSlider.
    Proof.
      intros Htl Heq.
      unfold attemptActionSlider in Heq.
      unfold requireFarPos in Heq.
      destruct (sliderCanSlide d c1.(c_state).(l_tile)); [| discriminate Heq].
      destruct (travel s newPos d) as [farPos |] eqn:HeqfarPos; [| discriminate Heq].
      pose (Hfpb := travel_inBounds s newPos farPos d Hpb HeqfarPos). clearbody Hfpb.
      destruct (anyEntityAt s farPos); [discriminate Heq |].
      (* end boilerplate *)
      destruct (getCell s farPos).(c_state).(l_tile) eqn:Heqft; try discriminate Heq.
      destruct (getCell s farPos).(c_state).(l_panel) eqn:Heqfp; try discriminate Heq.
      apply (walkAfterPushBlock_valid (setTile (setTile s newPos Floor) farPos c1.(c_state).(l_tile)) []).
      - eapply stateValidAndCompatible_transitive.
        + apply (setTile_valid s newPos Floor).
          * apply Hv.
          * apply Hpb.
          * intro Hclsv. unfold clsValid. simpl. destruct (getCell s newPos).(c_state).(l_panel); reflexivity.
        + intros [Hv1 Hc1]. apply setTile_valid.
          * apply Hv1.
          * eapply posInBounds_compatible. -- apply Hc1. -- apply Hfpb.
          * intro Hclsv. unfold clsValid. simpl.
            (* Need to prove that setting newPos doesn't change farPos. *)
            unfold setTile.
            rewrite getCell_setCell_different.
            -- rewrite Heqfp. reflexivity.
            -- apply Hv.
            -- apply Hfpb.
            -- apply Hpb.
            -- apply (travel_different s newPos farPos d).
               ++ apply Hpb. ++ apply HeqfarPos.
      - reflexivity.
      - apply Forall_nil.
      - inversion Heq as [[Heqs2 Heqf]]. reflexivity.
      - inversion Heq as [[Heqs2 Heqf]]. reflexivity.
    Qed.

    Lemma attemptActionGoldBlock_valid:
      t1 = GoldBlock -> actionSubValid attemptActionGoldBlock.
    Proof.
      intros Htl Heq.
      unfold attemptActionGoldBlock in Heq.
      unshelve eapply (launch_valid s s2 f newPos d Hv Hpb _ Heq).
      left. apply Htl.
    Qed.

    Lemma attemptActionSphere_valid:
      isSphere t1 = true -> actionSubValid attemptActionSphere.
    Proof.
      intros Htl Heq.
      unfold attemptActionSphere in Heq.
      unshelve eapply (launch_valid s s2 f _ d Hv _ _ Heq).
      - apply findLastConsecutiveInDirection_inBounds. apply Hpb.
      - right.
        pose (sf := fun p => negb (anyEntityAt s p)
                                  && isSphere (getCell s p).(c_state).(l_tile)).
        pose (Hfls := findLastConsecutiveInDirection_satisfies s newPos d sf Hpb).
        set (p' := findLastConsecutiveInDirection s newPos d sf) in *. cbv zeta in Hfls.
        fold sf. fold p'.
        destruct Hfls as [Hfls | Hfls].
        + (* Pos_eqbp to identity; maybe make a lemma *)
          assert (p' = newPos) as Hp'.
          * destruct p'. destruct newPos. simpl in Hfls. f_equal; lia.
          * rewrite Hp'. apply Htl.
        + unfold sf in Hfls. apply andb_prop in Hfls. destruct Hfls as [Hflse Hflst]. apply Hflst.
    Qed.

    Lemma attemptActionSteel_valid:
      isSteel t1 = true -> actionSubValid attemptActionSteel.
    Proof.
      intros Htl Heq.
      unfold attemptActionSteel in Heq.
      destruct (tryPushSteel s newPos d (findStartI s newPos d)) as [[s1 f1] |] eqn:Heq2;
        [| discriminate Heq].
      destruct (defaultWalk s1 ei oldPos newPos) as [sw fw] eqn:Heqdw.
      assert (stateValidAndCompatibleWithValidFlips s s1 f1 /\
             s.(s_bots) = s1.(s_bots)) as Hs1vcfb.
      - inversion Heq; subst s2 f. clear Heq Heqdw sw fw ei Hei Ht Htl Hnae Haea.
        remember (findStartI s newPos d) as i eqn:Heqi.
        assert (i <= findStartI s newPos d) as Hil; [lia |]. clear Heqi.
        revert s1 f1 Heq2; induction i as [| i IHi]; intros s1 f1 Heq2.
        + discriminate Heq2.
        + cbv beta delta [tryPushSteel] iota in Heq2. fold tryPushSteel in Heq2.
          set (curPos := findCandidatePos s newPos d (S i)) in *.
          set (nextPos := findCandidatePos s newPos d i) in *.
          (* Bummer we can't inline just curPos.  But the mess isn't too bad. *)
          cbv zeta in Heq2.
          destruct (getCell s curPos).(c_state).(l_tile)
            (* Oink, but I haven't learned a better approach using matching. *)
            as [| | | | | | | | | | | color | | | | | | | | | | | | | | | | | | | | | |]
                 eqn:Heqct; try discriminate Heq2.
          destruct (anyEntityAt s nextPos); [discriminate Heq2 |].
          (* Cut down on the indentation a little. :( *)
          assert ((posInBounds s curPos).(bp_prop) /\ (posInBounds s nextPos).(bp_prop)) as Hcnpb.
          * split.
            -- apply findCandidatePos_inBounds. ++ apply Hpb. ++ apply Hil.
            -- apply findCandidatePos_inBounds. ++ apply Hpb. ++ lia.
          * destruct Hcnpb as [Hcpb Hnpb].
            pose (removeFromCur := fun (s02: State) (f02: Flips) =>
                                     (setTile s02 curPos Floor,
                                      checkPanel s02 curPos false color ++ f02)).
            pose (pushThis := fun (s01: State) (f01: Flips) =>
                                removeFromCur (setTile s01 nextPos (getCell s curPos).(c_state).(l_tile))
                                              (checkPanel s01 nextPos false color ++ f01)).
            (* We're lucky that a steel can be validly placed on any cell in any compatible state,
               otherwise we would need a more complex hypothesis (and proof) that the recursive
               call doesn't change the cell in a way that prevents placing the steel. *)
            assert (forall (s02: State) (f02: Flips),
                       (stateValidAndCompatibleWithValidFlips s s02 f02 /\ s.(s_bots) = s02.(s_bots)) ->
                       let (s03, f03) := removeFromCur s02 f02 in
                       (stateValidAndCompatibleWithValidFlips s s03 f03 /\ s.(s_bots) = s03.(s_bots)))
              as HremoveFromCur.
            -- intros s02 f02 Hs02vcfb. unfold removeFromCur.
               destruct Hs02vcfb as [[Hs02vc Hs02f] Hs02b]. destruct (Hs02vc) as [Hs02v Hs02c].
               split; [split |].
               ++ apply (stateValidAndCompatible_transitive _ s02 _).
                  ** apply Hs02vc.
                  ** intros _. apply setTile_valid.
                     --- apply Hs02v.
                     --- eapply posInBounds_compatible. +++ apply Hs02c. +++ apply Hcpb.
                     --- intro Hclsv. unfold clsValid. simpl.
                         (* TODO Lemma? *)
                         destruct (getCell s02 curPos).(c_state).(l_panel); reflexivity.
               ++ apply Forall_app. split.
                  ** eapply flipsValid_compatible.
                     --- apply statesCompatible_symmetric. apply Hs02c.
                     --- apply checkPanel_flipsValid.
                         +++ apply Hs02v.
                         +++ eapply posInBounds_compatible. *** apply Hs02c. *** apply Hcpb. (* dup *)
                  ** apply Hs02f.
               ++ rewrite Hs02b. reflexivity.
            -- assert (forall (s01: State) (f01: Flips),
                       (stateValidAndCompatibleWithValidFlips s s01 f01 /\ s.(s_bots) = s01.(s_bots)) ->
                       let (s03, f03) := pushThis s01 f01 in
                       (stateValidAndCompatibleWithValidFlips s s03 f03 /\ s.(s_bots) = s03.(s_bots)))
                as HpushThis.
               ++ intros s01 f01 Hs01vcfb. unfold pushThis.
                  (* How unusual: we have "let" in the conclusion and can apply a matching assertion! *)
                  apply HremoveFromCur.
                  destruct Hs01vcfb as [[Hs01vc Hs01f] Hs01b]. destruct (Hs01vc) as [Hs01v Hs01c].
                  split; [split |].
                  ** apply (stateValidAndCompatible_transitive _ s01 _).
                     --- apply Hs01vc.
                     --- intros _. apply setTile_valid.
                         +++ apply Hs01v.
                         +++ eapply posInBounds_compatible. *** apply Hs01c. *** apply Hnpb.
                         +++ intro Hclsv. unfold clsValid. rewrite Heqct. simpl.
                             destruct (getCell s01 nextPos).(c_state).(l_panel); reflexivity.
                  ** apply Forall_app. split.
                     --- eapply flipsValid_compatible.
                         +++ apply statesCompatible_symmetric. apply Hs01c.
                         +++ apply checkPanel_flipsValid.
                             *** apply Hs01v.
                             *** eapply posInBounds_compatible. ---- apply Hs01c. ---- apply Hnpb.
                     --- apply Hs01f.
                  ** rewrite Hs01b. reflexivity.
               ++ destruct (getCell s nextPos).(c_state).(l_tile) eqn:Heqnt; try discriminate Heq2.
                  ** (* Floor *) inversion Heq2. subst s1 f1. split; [| reflexivity].
                     unshelve epose (H := HpushThis s [] _).
                     --- split; [split |].
                         +++ apply stateValidAndCompatible_reflexive. apply Hv.
                         +++ apply Forall_nil.
                         +++ reflexivity.
                     --- clearbody H. unfold pushThis in H. simpl in H. rewrite Heqct in H.
                         destruct H as [H _]. apply H.
                  ** (* Steel *)
                    destruct (tryPushSteel s newPos d i) as [[s01 f01] |] eqn:Heq01; [| discriminate Heq2].
                    inversion Heq2. subst s1 f1.
                    unshelve epose (H01 := IHi _ s01 f01 _); [lia | reflexivity |]. clearbody H01.
                    apply HpushThis in H01. unfold pushThis in H01. unfold removeFromCur in H01.
                    rewrite Heqct in H01. apply H01.
                  ** (* Electric *) inversion Heq2. subst s1 f1.
                     unshelve epose (H := HremoveFromCur s [] _).
                     --- split; [split |].
                         +++ apply stateValidAndCompatible_reflexive. apply Hv.
                         +++ apply Forall_nil.
                         +++ reflexivity.
                     --- apply H.
      - destruct Hs1vcfb as [[Hs1vc Hs1f] Hs1b]. destruct (Hs1vc) as [Hs1v Hs1c].
        eapply walkAfterPushBlock_valid.
        + apply Hs1vc.
        + apply Hs1b.
        + apply Hs1f.
        + inversion Heq. subst s2 f.
          unfold defaultWalk in Heqdw. inversion Heqdw.
          reflexivity.
        + inversion Heq. subst s2 f.
          unfold defaultWalk in Heqdw. inversion Heqdw.
          reflexivity.
    Qed.

  End ActableSubs.
End ActionSubs.

End ActionSubsModule.
Import ActionSubsModule.

Lemma attemptAction_valid:
  forall (s s2: State) (ei: EntityID) (d: Direction),
    (stateValid s).(bp_prop) -> entityIDValid s ei ->
    isNormalActingEntity s ei = true ->
    attemptAction s ei d = Some s2 ->
    stateValidAndCompatible s s2.
Proof.
  intros s s2 ei d Hv Hei Hnae Heq.
  unfold attemptAction in Heq.
  destruct (attemptActionPreFlips s ei d) as [[s' f] |] eqn:Haapf; [| discriminate Heq].

  assert (stateValidAndCompatibleWithValidFlips s s' f) as Hvcf'.  (* The big assertion! *)
  - unfold attemptActionPreFlips in Haapf.
    pose (Hob := getEntityPos_inBounds s ei Hv Hei).
    set (oldPos := getEntityPos s ei) in *.
    destruct (travel s oldPos d) as [newPos |] eqn:Ht; [| discriminate Haapf].
    pose (Hnb := travel_inBounds s oldPos newPos d Hob Ht).
    set (t := (getCell s newPos).(c_state).(l_tile)) in *.
    destruct (classifyTile t) eqn:Hct.
    + (* Floorlike *) eapply attemptActionFloorlike_valid; eassumption.
    + (* Actable *) destruct (anyEntityAt s newPos) eqn:Haea.
      * discriminate Haapf.  (* no move *)
      * destruct t eqn:Heqt;
          try discriminate Haapf;  (* throw out non-actable tiles *)
          revert Haapf.
        (* TODO: Avoid duplication in {GrayBlock, RedBlock, Wire _, Transponder} and Slider{Vert,Horiz}. *)
        -- (* Exit *) apply attemptActionExit_valid; assumption.
        -- (* HeartFramer *) apply attemptActionHeartframer_valid; assumption.
        -- (* GrayBlock *) apply attemptActionPushable_valid; try assumption.
           unfold ActionSubsModule.t1, ActionSubsModule.c1. fold t. rewrite Heqt. reflexivity.
        -- (* RedBlock *) apply attemptActionPushable_valid; try assumption.
           unfold ActionSubsModule.t1, ActionSubsModule.c1. fold t. rewrite Heqt. reflexivity.
        -- (* GoldBlock *) apply attemptActionGoldBlock_valid; assumption.
        -- (* Sphere _ *) apply attemptActionSphere_valid; try assumption.
           unfold ActionSubsModule.t1, ActionSubsModule.c1. fold t. rewrite Heqt. reflexivity.
        -- (* Steel _ *) apply attemptActionSteel_valid; try assumption.
           unfold ActionSubsModule.t1, ActionSubsModule.c1. fold t. rewrite Heqt. reflexivity.
        -- (* BrokenBlock *) apply attemptActionBroken_valid; assumption.
        -- (* GreenBlock *) apply attemptActionGreen_valid; assumption.
        -- (* Electric *) apply attemptActionElectric_valid; assumption.
        -- (* ButtonOn *) apply attemptActionOn_valid; assumption.
        -- (* SliderVert *) apply attemptActionSlider_valid; try assumption; right; assumption.
        -- (* SliderHoriz *) apply attemptActionSlider_valid; try assumption; left; assumption.
        -- (* Button0 *) apply attemptAction01_valid; try assumption; left; assumption.
        -- (* Button1 *) apply attemptAction01_valid; try assumption; right; assumption.
        -- (* Teleporter *) apply attemptActionTeleporter_valid; assumption.
        -- (* Button *) apply attemptActionButton_valid; assumption.
        -- (* Wire _ *) apply attemptActionPushable_valid; try assumption.
           unfold ActionSubsModule.t1, ActionSubsModule.c1. fold t. rewrite Heqt. reflexivity.
        -- (* Transponder *) apply attemptActionPushable_valid; try assumption.
           unfold ActionSubsModule.t1, ActionSubsModule.c1. fold t. rewrite Heqt. reflexivity.
    + (* Unactable *) discriminate Haapf.

  - injection Heq as Heq. rewrite <- Heq.
    destruct Hvcf' as [Hvc' Hf'].
    apply (stateValidAndCompatible_transitive _ s' _).
    + assumption.
    + intros _. apply applyFlips_valid.
      * destruct Hvc' as [Hv' Hc']. assumption.
      * eapply flipsValid_compatible.
        -- destruct Hvc' as [Hv' Hc']. apply Hc'.
        -- assumption.
Qed.
