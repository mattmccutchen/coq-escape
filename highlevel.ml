open Escape;;
open Util;;
open Lowlevel;;

(* state <-> lowlevel *)

(* XXX Rather use arrays for faster random access?  That's probably the least of our problems. *)

let llTileTypes: cellLayerState list =
  List.map (fun (t, p) -> {l_tile = t; l_panel = p})
[
  Floor, None;
  RedBlock, None;
  BlueBlock, None;
  GrayBlock, None;
  GreenBlock, None;
  Exit, None;
  Hole, None;
  GoldBlock, None;
  Laser, None;
  Floor, Some None;
  StopSign, None;
  ArrowSign Right, None;
  ArrowSign Left, None;
  ArrowSign Up, None;
  ArrowSign Down, None;
  Rough, None;
  Electric, None;
  ButtonOn, None;
  ButtonOff, None;
  Teleporter, None;
  BrokenBlock, None;
  SliderHoriz, None;
  SliderVert, None;
  Button0, None;
  Button1, None;
  Wire W_NS, None;
  Wire W_NE, None;
  Wire W_NW, None;
  Wire W_SE, None;
  Wire W_SW, None;
  Wire W_WE, None;
  Button, None;
  Light Blue, None;
  Light Red, None;
  Light Green, None;
  BlackSpace, None;
  Togglable (true, Blue), None;
  Togglable (false, Blue), None;
  Togglable (true, Red), None;
  Togglable (false, Red), None;
  Togglable (true, Green), None;
  Togglable (false, Green), None;
  Sphere (Some Blue), None;
  Sphere (Some Red), None;
  Sphere (Some Green), None;
  Sphere None, None;
  Trap2, None;
  Trap1, None;
  Floor, Some (Some Blue);
  Floor, Some (Some Red);
  Floor, Some (Some Green);
  Steel None, None;
  Steel (Some Blue), None;
  Steel (Some Red), None;
  Steel (Some Green), None;
  HeartFramer, None;
  SleepingDoor, None;
  Transponder, None;
  Wire W_NSWE, None;
  Remote, None
];;

let panelFlagsNormal = (1, 4, 8);;
let panelFlagsAlt = (2, 16, 32);;
let getLayerPanelFlags (f_base, f_lo, f_hi) cls =
  match cls.l_panel with
  | None -> 0
  | Some None -> f_base
  | Some (Some Blue) -> f_base lor f_lo
  | Some (Some Green) -> f_base lor f_hi
  | Some (Some Red) -> f_base lor (f_lo lor f_hi)
;;
let getPanelFlags c =
  getLayerPanelFlags panelFlagsNormal c.c_state lor
  getLayerPanelFlags panelFlagsAlt c.c_ostate
;;

let llBotTypes: botStatus list = [
  BrokenBot;
  Dalek;
  Hugbot;
  SleepingDalek;
  SleepingHugbot
] @
  (* Because we aren't raising the bomb timer limit, we can just generate all the possible timers. *)
  let rec bombs t =
    if t > 10
    then []
    else Bomb (int_to_nat t, None) :: bombs (t + 1)
  in bombs 0;;

(* Because we don't know whether we have a lowlevel or a state yet, just take the width.
   XXX: These may be adjusted as needed. *)
let index w x y = y * w + x;;
let indexOfPos w p = index w (nat_to_int p.p_x) (nat_to_int p.p_y);;
let wherePos w i = {p_x = int_to_nat (i mod w); p_y = int_to_nat (i / w)};;

let lowlevelToTitledState ll =
  let state = {
    s_width = int_to_nat ll.width;
    s_height = int_to_nat ll.height;
    s_cells = List.init ll.height (fun y ->
      List.init ll.width (fun x ->
        let i = index ll.width x y in
	let c = {
	  c_state = List.nth llTileTypes ll.tiles.(i);
	  c_ostate = List.nth llTileTypes ll.otiles.(i);
	  c_dest = wherePos ll.width ll.dests.(i)
	} in
	(* Validate flags, so we won't accept blocks on panels, like the original
	   implementation.  In the future, we might decode the flags to generate
	   panels under blocks. *)
	if ll.flags.(i) <> getPanelFlags c then failwith "Wrong cell flags";
	c
      ));
    s_playerPos = {p_x = int_to_nat ll.playerX; p_y = int_to_nat ll.playerY};
    s_playerBombed = false;
    s_bots = List.init ll.nbots (fun j ->
        let botType = ll.bott.(j) in
        let botLoc = ll.boti.(j) in
        (* Note: if a bomb timer is greater than 10, List.nth will fail *)
        {b_pos = wherePos ll.width botLoc; b_status = List.nth llBotTypes botType}
      )
  } in
  let titledState = {
    ts_title = ll.title;
    ts_author = ll.author;
    ts_state = state
  } in
  if stateValid state && stateIsInitial state
  then titledState
  else failwith "State is not a valid initial state"
;;

let titledStateToLowlevel titledState =
  let state = titledState.ts_state in
  if not (stateValid state && stateIsInitial state)
  then failwith "State is not a valid initial state"
  else
  let cells1d = List.concat state.s_cells in
  (* For initial states, the tile+panel should exactly match one of the entries in llTileTypes. *)
  let clsToLlt cls = list_mem_idx llTileTypes cls in
  let entityStatusToLlbt es = list_mem_idx llBotTypes es in
  let w = nat_to_int state.s_width in
  {
    width = w;
    height = nat_to_int state.s_height;
    title = titledState.ts_title;
    author = titledState.ts_author;

    playerX = nat_to_int state.s_playerPos.p_x;
    playerY = nat_to_int state.s_playerPos.p_y;

    tiles = Array.of_list (List.map (fun c -> clsToLlt c.c_state) cells1d);
    otiles = Array.of_list (List.map (fun c -> clsToLlt c.c_ostate) cells1d);
    dests = Array.of_list (List.map (fun c -> indexOfPos w c.c_dest) cells1d);
    flags = Array.of_list (List.map getPanelFlags cells1d);
    nbots = List.length state.s_bots;
    boti = Array.of_list (List.map (fun b -> indexOfPos w b.b_pos) state.s_bots);
    bott = Array.of_list (List.map (fun b -> entityStatusToLlbt b.b_status) state.s_bots)
  }
;;

let bufferToTitledState buf = lowlevelToTitledState (bufferToLowlevel buf);;
let titledStateToBuffer ts = lowlevelToBuffer (titledStateToLowlevel ts);;
