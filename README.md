# Escape in Coq (coq-escape)

https://mattmccutchen.net/escape/#coq

This is a formalization of the rules of the puzzle game Escape
(http://escape.spacebar.org/) in the Coq mechanized proof system
(https://coq.inria.fr/), anticipating use cases such as:

- Proving that the current speed record for a given puzzle cannot be beaten;
- Formalizing another problem and its embedding in Escape, proving the embedding
  correct, and generating the Escape puzzle via extraction.

In addition to the core Escape logic in Coq, there is OCaml code that works with
the data types extracted from Coq and can read and write puzzle files (esx) and
read the solutions from a player file (esp).

(Caveat: It's a _huge_ amount of work to prove anything interesting in Coq about
a system as complex as Escape, at least without Coq skills orders of magnitude
better than mine.  So I don't know if the use cases will end up being realistic.
So far, I just had fun reimplementing and testing the rules.)

The name `coq-escape` follows the naming convention for Coq packages, suggesting
that this could be the Coq package formalizing Escape, although I don't know if
the extra OCaml code could be included in such a package.

## Status

The formalization of the rules is believed to be complete and has been partially
tested by running it on all the solutions and bookmarks in Matt's player file
(covering almost 3000 puzzles from the main server) to verify that they remain
valid solutions and bookmarks under the Coq implementation.  This test has
caught a number of corner cases that weren't handled properly, but there's no
guarantee it will catch everything.  In particular, it won't catch states that
are lost under the original implementation but not under the Coq implementation,
but that logic is simple enough that I'm pretty confident there are no bugs.

In addition, there is a proof that running a round (a player turn followed by
bot turns) starting from a valid state yields a valid state.  Given the concept
of a valid state, this seemed a natural theorem to want to prove, though it's
unclear if any of the anticipated use cases for the formalization will actually
use this theorem.

## Building

I've tested this only on Fedora 30 (Linux), x86_64, with newer Coq and OCaml
packages taken from Fedora 31:

coq-8.9.1-5.fc31.x86_64
ocaml-4.08.1-0.rc2.1.fc31.x86_64
ocaml-base64-2.1.2-18.fc31.x86_64

I won't proactively test other environments, but please let me know if there are
specific changes I could make to support other environments.

1. coq_makefile -o CoqMakefile -f _CoqProject
2. make -f CoqMakefile
3. Open Extract.v in Proof General and run to the end.
4. ./ocaml-build

## Usage

You can add Coq test code to `Test.v`; see the example there for inspiration.

The OCaml executable (`./main`) is currently set up to verify all solutions and
bookmarks in a player file against the Coq implementation of the rules, as a way
of catching most kinds of bugs in the rules.  You can change it (in `main.ml`)
to do something else using the available code.
