Require Export Basics.

(* Record field names cannot be used in projection syntax without being entered in the
   global scope?  Have we made no progress since C??? *)
Record BoolProp :=
  {
    bp_bool: bool;
    bp_prop: Prop;
    bp_equiv: (bp_bool = true) <-> bp_prop
  }.

Definition leafBoolProp (b : bool) : BoolProp.
  refine {|
    bp_bool := b;
    bp_prop := b = true
    |}.
  reflexivity.
Defined.

Definition andbp (a b : BoolProp) : BoolProp.
  refine {|
      bp_bool := andb a.(bp_bool) b.(bp_bool);
      bp_prop := a.(bp_prop) /\ b.(bp_prop);
    |}.
  apply (iff_trans (Bool.andb_true_iff a.(bp_bool) b.(bp_bool))).
  split; intro H.
  - destruct H. split.
    + apply a.(bp_equiv). assumption.
    + apply b.(bp_equiv). assumption.
  - destruct H. split.
    + apply a.(bp_equiv). assumption.
    + apply b.(bp_equiv). assumption.
  (* Really only the bool part needs to be transparent; ideally the equiv part would be opaque.
     But we can probably get away with this. *)
Defined.

(* "Infix" does not work under "Set Mangle Names":
   https://github.com/coq/coq/issues/11730 *)
Notation "x &&& y" := (andbp x y) (at level 40, left associativity).

Definition orbp (a b : BoolProp) : BoolProp.
  refine {|
      bp_bool := orb a.(bp_bool) b.(bp_bool);
      bp_prop := a.(bp_prop) \/ b.(bp_prop);
    |}.
  split; intro H.
  - remember a.(bp_bool) as ab eqn:Heqab. symmetry in Heqab. destruct ab.
    + apply a.(bp_equiv) in Heqab. left. assumption.
    + simpl in H. apply b.(bp_equiv) in H. right. assumption.
  - destruct H as [H | H].
    + apply a.(bp_equiv) in H. rewrite H. reflexivity.
    + apply b.(bp_equiv) in H. rewrite H. destruct a.(bp_bool); reflexivity.
Defined.

Notation "x ||| y" := (orbp x y) (at level 50, left associativity).

Definition negbp (b : BoolProp) : BoolProp.
  refine {|
      bp_bool := negb b.(bp_bool);
      bp_prop := ~b.(bp_prop);
    |}.
  split; intro H.
  - intro H0. apply b.(bp_equiv) in H0. rewrite H0 in H. inversion H.
  - remember b.(bp_bool) as bb eqn:Heqbb. symmetry in Heqbb. destruct bb.
    + apply b.(bp_equiv) in Heqbb. contradiction.
    + reflexivity.
Defined.
