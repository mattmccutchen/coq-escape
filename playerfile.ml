open Escape;;
open Util;;
open Lowlevel;;

type moveSequenceType = MS_Solution | MS_Bookmark;;

(* Everything but the level hash! *)
type namedMoveSequence = {
  date: int32;
  name: string;
  author: string;
  moves: moveSequence;  (* defined in Escape *)
  msType: moveSequenceType
};;

module StringMap = Map.Make(String);;

(* Well, this is the only part of the player file we care about.
   NOTE: Level hashes are converted to binary strings. *)
type playerFile = (namedMoveSequence list) StringMap.t;;

let llDirections = [Up; Down; Left; Right];;
let intToDirection x =
  if x = 0 then failwith "Invalid direction"
  else List.nth llDirections (x - 1);;

let decodeSolution str =
  let bts = Bytes.of_string (B64.decode str) in
  let pos = 0 in
  let date = bytes_get_int32_be bts pos in
  let pos = pos + 4 in
  let nameLength = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  let name = Bytes.sub_string bts pos nameLength in
  let pos = pos + nameLength in
  let authorLength = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  let author = Bytes.sub_string bts pos authorLength in
  let pos = pos + authorLength in
  let msLength = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  let msNumMoves = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  let msStr = Bytes.sub_string bts pos (msLength - 4) in
  let pos = pos + (msLength - 4) in
  let bookmarkFlag = int32_to_int_strict (bytes_get_int32_be bts pos) in
  let pos = pos + 4 in
  let msType = match bookmarkFlag with
               | 0 -> MS_Solution
               | 1 -> MS_Bookmark
               | _ -> failwith "Invalid bookmark flag"
  in
  if pos <> Bytes.length bts then failwith "Trailing garbage in named solution";
  let msBuf = Buffer.create (String.length msStr) in
  Buffer.add_string msBuf msStr;
  let movesArr = Array.make msNumMoves 0 in
  let msBufPos = rleDecode msBuf 0 movesArr in
  if msBufPos <> Buffer.length msBuf then failwith "Trailing garbage in raw solution";
  {
    date = date;
    name = name;
    author = author;
    moves = List.map intToDirection (Array.to_list movesArr);
    msType = msType
  }
;;

(* NOTE: Does not consume the entire channel. *)
let readPlayerFileFromChannel inChannel =
  if input_line inChannel <> "ESPt" then failwith "Wrong magic in player file";
  (* 12 fields we don't care about *)
  for i = 0 to 11 do
    ignore (input_line inChannel)
  done;
  if input_line inChannel <> "-- solutions" then failwith "Expected -- solutions";
  let rec readSolutions m =
    let line = input_line inChannel in
    if line = "-- ratings" then (* done *) m else
    let textHash = String.sub line 0 32 in
    let binaryHash = Digest.from_hex textHash in
    if String.sub line 32 3 <> " * " then failwith "Bad syntax in solutions section";
    let firstSolBase64 = String.sub line 35 (String.length line - 35) in
    finishLevelSolutions m binaryHash [decodeSolution firstSolBase64]
  and finishLevelSolutions m h ssrev =
    let line = input_line inChannel in
    if line = "!" then
      readSolutions (StringMap.add h (List.rev ssrev) m)
    else (
      if String.sub line 0 2 <> "  " then failwith "Bad syntax in solutions section";
      let nextSolBase64 = String.sub line 2 (String.length line - 2) in
      finishLevelSolutions m h (decodeSolution nextSolBase64 :: ssrev)
    )
  in readSolutions (StringMap.empty)
;;
