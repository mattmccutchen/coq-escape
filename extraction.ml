(* Helpers to allow Coq to extract to OCaml types.  Nothing here should be
   referenced by other OCaml source files. *)

(* list -> list *)

let nil = [];;
let cons (h, t) = h :: t;;
let list_match fNil fCons l =
  match l with
  | [] -> fNil ()
  | h :: t -> fCons h t
;;

(* ascii -> char *)

let charCons (b0, b1, b2, b3, b4, b5, b6, b7) =
  char_of_int (b0 + 2*b1 + 4*b2 + 8*b3 + 16*b4 + 32*b5 + 64*b6 + 128*b7);;
let char_match fChar c =
  let i = int_of_char c in
  fChar
    (i land 1 <> 0)
    (i land 2 <> 0)
    (i land 4 <> 0)
    (i land 8 <> 0)
    (i land 16 <> 0)
    (i land 32 <> 0)
    (i land 64 <> 0)
    (i land 128 <> 0)
;;

(* string -> string *)

let emptyString = "";;
let stringCons (h, t) =
  String.make 1 h ^ t;;
let string_match fEmpty fCons s =
  if String.length s = 0
  then fEmpty ()
  else fCons s.[0] (String.sub s 1 (String.length s - 1))
