Require Export Basics.
Require Export BoolProp.
Require Export NatExt.
Require Export ListExt.
Require String.

Variant Direction := Up | Down | Left | Right.
Definition allDirections := [Up; Down; Left; Right].

(* Coincidentally the same for panels, spheres, and steels as for lights and togglable floors.  Use
the same definition rather than have to qualify all the names. *)
Variant Color := Red | Green | Blue.
(* Scheme Equality does not work under Mangle Names.  TODO: file a bug.
   Use a section rather than just setting Mangle Names again afterward so we can
   disable Mangle Names globally in one place. *)
Section ColorEquality.
Local Unset Mangle Names.
Scheme Equality for Color.
End ColorEquality.
Definition PanelType := option Color.

(* XXX: Parameterize the wires somehow?  That will lead to additional validation obligations.
Given there are only 7, it's probably easier to hard-code the behavior of each. *)
Variant WireType :=
| W_NS
| W_WE
| W_NW
| W_NE
| W_SE
| W_SW
| W_NSWE.

Variant TileType :=
(* Tiles are roughly grouped by function with some inspiration from the order used by the level editor. *)
| Floor
| Rough
| BlackSpace

| Exit
| SleepingDoor
| HeartFramer

| Laser

| GrayBlock
| RedBlock
| GoldBlock
| Sphere (c: PanelType)
| Steel (c: PanelType)

| BrokenBlock

| GreenBlock

(* Represent as parameterized by (2, 1, 0)? *)
| Trap2
| Trap1
| Hole

| BlueBlock
| StopSign
| ArrowSign (d: Direction)

| Electric
| ButtonOn
| ButtonOff

| SliderVert
| SliderHoriz
| Button0
| Button1

| Teleporter

| Button
(* Since most places that inspect a TileType don't distinguish among wires,
   grouping the wires makes things easier on net. *)
| Wire (wt: WireType)
| Transponder
| Remote
| Light (c: Color)
| Togglable (up: bool) (c: Color)
.


Record CellLayerState :=
  {
    l_tile : TileType;
    l_panel : option PanelType;
  }.

Record Pos :=
  {
    p_x : nat;
    p_y : nat;
  }.

Definition Pos_eqbp (p1 p2 : Pos) :=
  (p1.(p_x) =?? p2.(p_x)) &&& (p1.(p_y) =?? p2.(p_y)).

(* Deprecate? *)
Definition Pos_beq (p1 p2 : Pos) := (Pos_eqbp p1 p2).(bp_bool).

Record CellInfo :=
  {
    c_state : CellLayerState;
    c_ostate : CellLayerState;
    c_dest : Pos;
  }.

Variant BotStatus :=
| NoBot (* Bot hit by another bot or zapped by electricity or hit by a bomb *)
| BrokenBot
| Hugbot
| Dalek
| SleepingHugbot
| SleepingDalek
| Bomb (resetTime : nat) (currentTime : option nat).

Record BotInfo :=
  {
    b_pos: Pos;
    b_status: BotStatus;
  }.

Definition list2d (t : Type) := list (list t).

Record State :=
  {
    s_width: nat;
    s_height: nat;
    s_cells: list2d CellInfo;
    s_playerPos: Pos;
    s_playerBombed: bool;
    (* Regular bots, then bombs. *)
    s_bots: list BotInfo;
  }.

Record TitledState :=
  {
    ts_title: String.string;
    ts_author: String.string;
    ts_state: State;
  }.
