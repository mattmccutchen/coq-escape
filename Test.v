Require Export Round.

Definition testState : State :=
  {|
    s_width := 2;
    s_height := 2;
    s_cells := [
                [{|
                  c_state := {| l_tile := Floor; l_panel := None |};
                  c_ostate := {| l_tile := Floor; l_panel := None |};
                  c_dest := {| p_x := 0; p_y := 0 |}
                |};
                 {|
                   c_state := {| l_tile := Floor; l_panel := Some None |};
                   c_ostate := {| l_tile := Floor; l_panel := None |};
                   c_dest := {| p_x := 1; p_y := 1 |}
                 |}];
                [{|
                  c_state := {| l_tile := Floor; l_panel := None |};
                  c_ostate := {| l_tile := Floor; l_panel := None |};
                  c_dest := {| p_x := 0; p_y := 0 |}
                |};
                 {|
                   c_state := {| l_tile := Laser; l_panel := None |};
                   c_ostate := {| l_tile := Floor; l_panel := None |};
                   c_dest := {| p_x := 0; p_y := 0 |}
                 |}]
              ];
    s_entities := [{| e_pos := {| p_x := 0; p_y := 0 |}; e_status := Player |}]
  |}.

Compute (stateValid testState).(bp_bool).
Compute (stateIsLost testState).(bp_bool).

Compute (setCell testState {| p_x := 1; p_y := 0 |}
                 {|
                   c_state := {| l_tile := Floor; l_panel := None |};
                   c_ostate := {| l_tile := Laser; l_panel := None |};
                   c_dest := {| p_x := 0; p_y := 0 |}
                 |}).

Definition nextStateOpt := attemptRound testState Right.
Compute nextStateOpt.
Compute (option_map (fun nextState => (stateIsLost nextState).(bp_bool)) nextStateOpt).
