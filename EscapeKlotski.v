Require Export Round.
Require Klotski.

Definition embedKlotski (k: Klotski.State): option State. Abort.

(* Proposal: embedKlotski will be specific to the size parameters that I used.  Under that
   assumption, we can recover the Klotski state from the Escape state up to the order of
   pieces and the order of offsets in a type. *)