(* For use on OCaml < 4.08 *)
let bytes_get_int32_be bts pos =
  let rec getRestOfBytes p n x =
    if n = 0 then x
    else getRestOfBytes (p + 1) (n - 1)
      (Int32.add (Int32.shift_left x 8) (Int32.of_int (int_of_char (Bytes.get bts p))))
  in getRestOfBytes pos 4 Int32.zero
;;
let buffer_add_int32_be buf x =
  for n = 0 to 3 do
    Buffer.add_char buf
      (char_of_int (Int32.to_int (Int32.shift_right x (8 * (3 - n))) land 0xFF))
  done
;;
let rec list_filter_map f l =
  match l with
  | [] -> []
  | h :: t ->
    match f h with
    | None -> list_filter_map f t
    | Some h' -> h' :: list_filter_map f t
;;

(* What a pain. *)
let fullRead inChannel buf =
  let chunkSize = 4096 in
  let bts = Bytes.create chunkSize in
  (* We could use a while loop instead, but that would require a ref cell. *)
  let rec finishReading () =
    let len = input inChannel bts 0 chunkSize in
    if len > 0
    then (Buffer.add_subbytes buf bts 0 len; finishReading ())
    else ()
  in finishReading ()
;;

let int32_to_int_strict x =
  let y = Int32.to_int x in
  if Int32.of_int y = x then y
  (* XXX: Better exception depending on context *)
  else failwith "Sorry, int32 cannot be represented as int"
;;

let int_to_int32_strict x =
  let y = Int32.of_int x in
  if Int32.to_int y = x then y
  (* XXX: Better exception depending on context *)
  else failwith "Sorry, int cannot be represented as int32"
;;

(* Return the index of the first element of the list l that is structurally equal to x,
   or raise an exception if none. *)
let list_mem_idx l x =
  let rec helper l1 i =
    match l1 with
    | [] -> raise Not_found
    | h :: t -> if h = x then i else helper t (i+1)
  in helper l 0
;;

(*
Replaced with Digest.{from,to}_hex.

(* Lowercase only as seen in player files. *)
(* Or consider using ocaml-hex (not available in Fedora 30) or Digest.from_hex (doesn't validate lowercase). *)
let hex_decode s =
  if String.length s mod 2 <> 0 then failwith "hex_decode: odd length input";
  let outl = String.length s / 2 in
  let outbts = Bytes.create outl in
  for i = 0 to outl - 1 do
    let decode_char c =
      if c >= '0' && c <= '9' then int_of_char c - int_of_char '0'
      else if c >= 'a' && c <= 'f' then 10 + int_of_char c - int_of_char 'a'
      else failwith "hex_decode: Invalid character in hexadecimal string"
    in
    let binchar = char_of_int (16 * decode_char s.[2*i] + decode_char s.[2*i+1]) in
    Bytes.set outbts i binchar
  done;
  Bytes.to_string outbts
*)

(* These /depend on/ extracted code, so they don't go in extraction.ml . *)

let int_to_nat x =
  if x < 0 then failwith "int_to_nat: Negative input";
  (* Tail recursive FWIW *)
  let rec i2n y n = if y = 0 then n else i2n (y - 1) (Escape.S n)
  in i2n x Escape.O
;;

let rec nat_to_int n =
  match n with
  | Escape.O -> 0
  | Escape.S m ->
    let x = (nat_to_int m) + 1 in
    if x < 0 then failwith "nat_to_int: Input too large" else x
;;
