Require Export Action.

Definition bombable (t: TileType) :=
  match t with
    (* Floor must be bombable so that panels can be bombed,
       even though bombing the floor itself has no effect. *)
  | Floor => true

  | Rough
  | BlackSpace
  | Electric
  | Trap2
  | Trap1
  | Hole
  | Sphere _
  | Steel _
  | ButtonOn
  | ButtonOff
  | SliderHoriz
  | SliderVert
  | Button0
  | Button1
  | ArrowSign _
  | Togglable _ _
  | Teleporter
    => false

  | GrayBlock
  | RedBlock
  | GoldBlock
  | GreenBlock
  | BlueBlock
  | StopSign
  | BrokenBlock
  | Laser
  | Exit
  | SleepingDoor
  | HeartFramer
  | Button
  | Wire _
  | Transponder
  | Remote
  | Light _
    => true
  end.

(* Break into pieces for ease of proof. :/ *)

Definition bombTile (s: State) (p: Pos) :=
  let c := getCell s p in
  if bombable c.(c_state).(l_tile)
  then
    (* Notice we clear the panel iff the tile was bombable. *)
    setCell s p {| c_state := {| l_tile := Floor; l_panel := None |};
                   c_ostate := c.(c_ostate); c_dest := c.(c_dest) |}
  else s.

Definition bombPlayer (s: State) (p: Pos) :=
  if Pos_beq s.(s_playerPos) p
  then {|
      s_width := s.(s_width);
      s_height := s.(s_height);
      s_cells := s.(s_cells);
      s_playerPos := s.(s_playerPos);
      s_playerBombed := true;
      s_bots := s.(s_bots);
    |}
  else s.

Definition bombBot (s: State) (p: Pos) :=
  (* We could use map, but using destroyBot lets us use the destroyBot_valid lemma. *)
  match botat s p with
  | None => s
  | Some bi => destroyBot s bi
  end.

Definition bombLocally (s: State) (p: Pos) :=
  bombBot (bombPlayer (bombTile s p) p) p.

Lemma bombLocally_valid:
  forall (s: State) (p : Pos),
    (stateValid s).(bp_prop) -> (posInBounds s p).(bp_prop) ->
    stateValidAndCompatible s (bombLocally s p).
Proof.
  intros s p Hv Hpb.
  apply (stateValidAndCompatible_transitive _ (bombTile s p) _).
  (* = Tile bombing *)
  - unfold bombTile.
    destruct (bombable (getCell s p).(c_state).(l_tile));
      (* Not bombable *)
      [| split; [assumption | apply statesCompatible_reflexive]].
    (* Bombable *)
    apply setCell_valid.
    + assumption. + assumption.
    (* cell info valid *)
    + simpl.
      pose (Hxyc := getCell_valid s p Hv Hpb).
      destruct Hxyc as [[Hs Hos] Hd].
      destruct Hd as [Hdx Hdy].
      repeat split; assumption.
  (* = Entity bombing *)
  - intros [Hv2 Hc2]. unfold bombLocally.
    remember (bombTile s p) as s1.
    apply (stateValidAndCompatible_transitive _ (bombPlayer s1 p) _).
    + unfold bombPlayer.
      destruct (Hv2) as [[[[[hh hw] hc] hpb] hbb] hb].
      destruct (Pos_beq s1.(s_playerPos) p).
      * split.
        -- split_stateValid; try assumption.
        -- repeat split; assumption.
      * apply stateValidAndCompatible_reflexive. assumption.
    + intros [Hv3 Hc3]. remember (bombPlayer s1 p) as s2.
      unfold bombBot. destruct (botat s2 p) as [bi |] eqn:Hba.
      * apply destroyBot_valid.
        -- assumption.
        -- apply botat_valid in Hba.
           ++ destruct Hba as [Hbal [Hbanb Hbap]]. assumption.
           ++ assumption.
           ++ apply (posInBounds_compatible s1 s2 p Hc3).
              apply (posInBounds_compatible s s1 p Hc2).
              assumption.
      * apply stateValidAndCompatible_reflexive. assumption.
Qed.

(* Factored out just so we can name it in the proof. :/ *)
Definition attemptNormalBotTurnHoriz (s: State) (bi : nat) :=
  let b := getBot s bi in
  if s.(s_playerPos).(p_x) <? b.(b_pos).(p_x) then attemptAction s (Bot bi) Left
  else if s.(s_playerPos).(p_x) >? b.(b_pos).(p_x) then attemptAction s (Bot bi) Right
       else None.

Definition attemptNormalBotTurnVert (s: State) (bi : nat) :=
  let b := getBot s bi in
  if s.(s_playerPos).(p_y) <? b.(b_pos).(p_y) then attemptAction s (Bot bi) Up
  else if s.(s_playerPos).(p_y) >? b.(b_pos).(p_y) then attemptAction s (Bot bi) Down
       else None.

(* The bot tries to take a turn.  If it can't, return the original state.
   Precondition: stateValid s, bi < length s.(s_bots) *)
Definition attemptBotTurn (s: State) (bi: nat) : State :=
  let b := getBot s bi in
  if isNormalActingEntity s (Bot bi)
  then match attemptNormalBotTurnHoriz s bi with
       | Some s2 => s2
       | None => match attemptNormalBotTurnVert s bi with
                 | Some s2 => s2
                 | None => s
                 end
       end
  else match b.(b_status) with
       | Bomb rt ct =>
         match ct with
         | None => s
         | Some 0 =>
           (* bi is number of the bomb exploding *)
           let fix doExplosion depthLimit s1 bi :=
               match depthLimit with
               | 0 => ShouldNotReach s1
               | S depthLimit' =>
                 let b1 := getBot s1 bi in
                 (* First, bomb the original cell locally, which destroys the bomb itself.
                    Then, look in each direction.  If there's no bomb there, bomb the cell
                    locally.  If there is, trigger a new explosion (which will in turn
                    bomb the cell locally). *)
                 let s2 := bombLocally s1 b1.(b_pos) in
                 let bombOneDir s3 d :=
                     match travel s3 b1.(b_pos) d with
                     | None => s3
                     | Some p2 =>
                       match botat s3 p2 with
                       | None => bombLocally s3 p2
                       | Some bi' =>
                         match (getBot s3 bi').(b_status) with
                         | Bomb _ _ => doExplosion depthLimit' s3 bi'
                         | _ => bombLocally s3 p2
                         end
                       end
                     end in
                 fold_left bombOneDir allDirections s2
               end
           in doExplosion (length s.(s_bots)) s bi
         | Some (S ct2) => setBotStatus s bi (Bomb rt (Some ct2))
         end
       | _ => s  (* This type of bot does not take a turn *)
       end.

Lemma attemptBotTurn_valid:
  forall (s: State) (bi: nat),
    (stateValid s).(bp_prop) -> bi < length s.(s_bots) ->
    let s2 := attemptBotTurn s bi in
    (* Entity length needed so that attemptRound can iterate through
       the number of entities that it saw at the beginning. *)
    stateValidAndCompatible s s2.
Proof.
  intros s bi Hv Hbi s2.
  unfold attemptBotTurn in s2.
  destruct (isNormalActingEntity s (Bot bi)) eqn:Hnae.
  (* = Normal acting entities *)
  - pose (b := getBot s bi). (* save a little typing *)
    destruct (attemptNormalBotTurnHoriz s bi) eqn:Heq.
    (* horizontal move *)
    + unfold attemptNormalBotTurnHoriz in Heq. fold b in Heq.
      destruct (s.(s_playerPos).(p_x) <? b.(b_pos).(p_x)).
      * apply (attemptAction_valid s s2 (Bot bi) Left).
        -- assumption. -- assumption. -- assumption. -- assumption.
      * destruct (s.(s_playerPos).(p_x) >? b.(b_pos).(p_x)).
        -- apply (attemptAction_valid s s2 (Bot bi) Right).
           ++ assumption. ++ assumption. ++ assumption. ++ assumption.
        -- discriminate Heq.
    + clear Heq. destruct (attemptNormalBotTurnVert s bi) eqn:Heq.
      (* vertical move *)
      * unfold attemptNormalBotTurnVert in Heq. fold b in Heq.
        destruct (s.(s_playerPos).(p_y) <? b.(b_pos).(p_y)).
        -- apply (attemptAction_valid s s2 (Bot bi) Up).
           ++ assumption. ++ assumption. ++ assumption. ++ assumption.
        -- destruct (s.(s_playerPos).(p_y) >? b.(b_pos).(p_y)).
           ++ apply (attemptAction_valid s s2 (Bot bi) Down).
              ** assumption. ** assumption. ** assumption. ** assumption.
           ++ discriminate Heq.
      (* no move *)
      * apply stateValidAndCompatible_reflexive. assumption.
  - destruct (getBot s bi).(b_status) as [ | | | | | | resetTime currentTime] eqn:Heqes;
    (* Entity types that do not take a turn *)
    try (split; [assumption | repeat split; reflexivity]).
    (* = Bomb *)
    + destruct currentTime as [n |].
      (* == Lit *)
      * destruct n.
        (* === Explosion *)
        -- remember (length s.(s_bots)) as depthLimit eqn:HeqdepthLimit in s2 at 1; clear HeqdepthLimit.
           clear Hnae resetTime Heqes. revert s bi Hv Hbi s2.
           induction depthLimit as [| depthLimit IHdepthLimit]; intros s bi Hv Hbi s2.
           ++ (* limit 0 *) apply stateValidAndCompatible_reflexive. assumption.
           ++ (* limit nonzero *)
             (* Since we reverted it, s plays the role of s1 in doExplosion.  s' is different:
                it is, in general, an intermediate state between direction bombings.  So don't
                call it s1. *)
             set (s' := bombLocally s (getBot s bi).(b_pos)) in *.
             (* This proof could be written using stateValidAndCompatible_transitive,
                but surprisingly, that turns out to be less convenient. *)
             (* s' is good *)
             assert (stateValidAndCompatible s s') as Hvc'.
             ** apply bombLocally_valid. --- assumption.
                --- destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb]. unfold getBot.
                    apply Forall_nth. +++ assumption. +++ lia.
             (* end of s' is good *)
             ** clearbody s'.
                remember allDirections as directionList eqn:HeqdirectionList in s2 at 2; clear HeqdirectionList.
                revert s' s2 Hvc'.
                induction directionList as [| d directionList IHdirectionList]; intros s' s2 Hvc';
                  destruct (Hvc') as [Hv' Hc'].
                (* no directions left *)
                --- unfold fold_left. assumption.
                (* directions left *)
                --- apply IHdirectionList.
                    (* single bombing *)
                    destruct (travel s' (getBot s bi).(b_pos) d) as [p |] eqn:Heqo; [| assumption].
                    unshelve epose (Hpib := travel_inBounds s' (getBot s bi).(b_pos) p d _ Heqo).
                    +++ apply (posInBounds_compatible s s' _ Hc').
                        (* This is fiddly; maybe there should be a lemma. *)
                        destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb].
                        unfold getBot. apply Forall_nth; assumption.
                    +++ clearbody Hpib.
                        (* First we should prove that bombLocally s' p is good
                           because we use it in two different cases. *)
                        assert (stateValidAndCompatible s (bombLocally s' p)).
                        *** unshelve epose (Hbl := bombLocally_valid s' p _ _).
                            (* evars from the epose *)
                            ---- destruct Hvc'; assumption.
                            ---- unshelve eapply (travel_inBounds _ _ _ _ _ Heqo).
                                 eapply posInBounds_compatible. ++++ apply Hc'.
                                 ++++ destruct Hv as [[[[[hh hw] hc] hpb] hbb] hb]. unfold getBot.
                                      apply Forall_nth. **** apply hbb. **** assumption.
                            (* finished epose; back to assertion *)
                            ---- destruct Hbl.
                                 split; [assumption |].
                                 eapply statesCompatible_transitive. ++++ apply Hc'. ++++ assumption.
                        (* done bombLocally s' p *)
                        *** destruct (botat s' p) as [n |] eqn:Hba.
                            ---- destruct (getBot s' n).(b_status);
                                   try assumption. (* non-bombs: bomb locally *)
                                 (* bombs: bomb recursively *)
                                 unshelve epose (Hbr := IHdepthLimit s' n _ _).
                                 ++++ apply Hv'.
                                 ++++ apply (botat_valid s' p n).
                                      **** apply Hv'.
                                      **** assumption.
                                      **** assumption.
                                 ++++ destruct Hbr as [Hbrv Hbrc].
                                      split. **** apply Hbrv.
                                      **** eapply statesCompatible_transitive.
                                           ----- apply Hc'.
                                           ----- apply Hbrc.
                            (* no bot: bomb locally *)
                            ---- assumption.
        (* === Countdown *)
        -- apply setBotStatus_valid.
           ++ assumption.
           ++ assumption.
           ++ rewrite Heqes. reflexivity.
           ++ rewrite Heqes. reflexivity.
      (* == Unlit *)
      * apply stateValidAndCompatible_reflexive. assumption.
Qed.

(* Precondition: stateValid s and not stateIsSeverelyLost s *)
Definition attemptRound (s: State) (pd: Direction) : option State :=
  match attemptAction s Player pd with
  | None => None
  | Some s1 =>
    let fix doBotTurns revCounter s2 :=
        match revCounter with
        | 0 => s2
        | S revCounter2 =>
          doBotTurns revCounter2 (attemptBotTurn s2 (length s2.(s_bots) - revCounter))
        end in
    Some (doBotTurns (length s1.(s_bots)) s1)
  end.

Lemma attemptRound_valid:
  forall (s s1: State) (pd: Direction),
    (stateValid s &&& negbp (stateIsSeverelyLost s)).(bp_prop) ->
    attemptRound s pd = Some s1 ->
    (* I'm not sure if we need compatibility here, but I'll try including it for now. *)
    stateValidAndCompatible s s1.
Proof.
  intros s s1 pd H Heq. unfold attemptRound in Heq.
  destruct (attemptAction s Player pd) as [s0 |] eqn:Heqo;
    (* Case in which player can't move and nothing happens. *)
    [| discriminate Heq].
  (* Normal round *)
  
  (* First prove the state is valid after the player acts. *)
  assert (stateValidAndCompatible s s0).
  - destruct H as [Hv Hnsl].
    apply (attemptAction_valid s s0 Player pd Hv).
    + exact I.
    + reflexivity.
    + assumption.
  - (* Now the bot turns *)
    clear Heqo H pd.  (* to avoid confusion *)
    remember (length s0.(s_bots)) as rc0 eqn:Heqrc0.
    assert (rc0 <= length s0.(s_bots)); [lia |].
    clear Heqrc0. generalize dependent s0. induction rc0 as [| rc0 IHrc0]; intros s2 Heq H0 ?.
    (* No turns remaining *)
    + injection Heq as H1. rewrite <- H1. assumption.
    (* At least one turn remaining *)
    + destruct H0 as [Hv Hle].
      unshelve epose (Hbtv := attemptBotTurn_valid _ _ _ _); cycle -1.
      * eapply IHrc0. -- apply Heq.
        -- destruct Hbtv as [Hbtsv Hbtne].
           split. ++ eassumption.
           ++ eapply statesCompatible_transitive. ** eassumption. ** assumption.
        -- destruct Hbtv as [Hbtsv Hbtne].
           destruct Hbtne as [Hbtw [Hbth Hbtne]].
           rewrite <- Hbtne. lia.
      * assumption. * lia.
Qed.
